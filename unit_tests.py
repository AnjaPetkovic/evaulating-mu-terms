import unittest
from muTerms import *
from evaluating_algorithm import *

class TestMuTermsMethods(unittest.TestCase):

    def test_arguments(self):
        t0 = muTerm('0', None)
        self.assertEqual(t0.subterm1, None)
        self.assertEqual(t0.subterm2, None)
        self.assertEqual(t0.name, None)
        self.assertEqual(t0.value, 0)
        self.assertEqual(t0.var_set, set())

        t1 = muTerm('1', None)
        self.assertEqual(t1.subterm1, None)
        self.assertEqual(t1.subterm2, None)
        self.assertEqual(t1.name, None)
        self.assertEqual(t1.value, 1)
        self.assertEqual(t1.var_set, set())

        t2 = muTerm('r', muTerm('1', None),None, 0.25)
        self.assertEqual(t2.subterm1, muTerm('1', None))
        self.assertEqual(t2.subterm2, None)
        self.assertEqual(t2.name, None)
        self.assertEqual(t2.value, 0.25)
        self.assertEqual(t2.var_set, set())

        t = muTerm('+',t1,t2)
        self.assertEqual(t.subterm1, t1)
        self.assertEqual(t.subterm2, t2)
        self.assertEqual(t.name, None)
        self.assertEqual(t.value, None)
        self.assertEqual(t.var_set, set())

        t = muTerm('.',t1,t2)
        self.assertEqual(t.subterm1, t1)
        self.assertEqual(t.subterm2, t2)
        self.assertEqual(t.name, None)
        self.assertEqual(t.value, None)
        self.assertEqual(t.var_set, set())

        t = muTerm('M',t1,t2)
        self.assertEqual(t.subterm1, t1)
        self.assertEqual(t.subterm2, t2)
        self.assertEqual(t.name, None)
        self.assertEqual(t.value, None)
        self.assertEqual(t.var_set, set())

        t = muTerm('m',t1,t2)
        self.assertEqual(t.subterm1, t1)
        self.assertEqual(t.subterm2, t2)
        self.assertEqual(t.name, None)
        self.assertEqual(t.value, None)
        self.assertEqual(t.var_set, set())

        t = muTerm('mu',t2,None,None,'x')
        self.assertEqual(t.subterm1, t2)
        self.assertEqual(t.subterm2, None)
        self.assertEqual(t.name, 'x')
        self.assertEqual(t.value, None)
        self.assertEqual(t.var_set, set())

        t = muTerm('nu',t2,None,None,'x')
        self.assertEqual(t.subterm1, t2)
        self.assertEqual(t.subterm2, None)
        self.assertEqual(t.name, 'x')
        self.assertEqual(t.value, None)
        self.assertEqual(t.var_set, set())

        t = muTerm('var',None,None,None,'x')
        self.assertEqual(t.subterm1, None)
        self.assertEqual(t.subterm2, None)
        self.assertEqual(t.name, 'x')
        self.assertEqual(t.value, None)
        self.assertEqual(t.var_set, set('x'))

        with self.assertRaises(ValueError):
            muTerm('blabla',None)
            muTerm('var',None,None,None,None)
            muTerm('var',None,None,None,0.25)
            muTerm('r',None,None,0.3)
            muTerm('r',muTerm('1',None),None,None)
            muTerm('r',muTerm('1',None),None,'123')
            muTerm('+',None,t)
            muTerm('+',t,None)
            muTerm('.',None,t)
            muTerm('.',t,None)
            muTerm('M',None,t)
            muTerm('M',t,None)
            muTerm('m',None,t)
            muTerm('m',t,None)
            muTerm('mu',None,None,None,'x')
            muTerm('mu',t,None,None,None)
            muTerm('nu',None,None,None,'x')
            muTerm('nu',t,None,None,None)



    def test_renaming_variables(self):
        t0 = muTerm('0', None)
        t0.rename_variables()
        self.assertEqual(t0, muTerm('0', None))

        t1 = muTerm('1', None)
        t1.rename_variables()
        self.assertEqual(t1, muTerm('1', None))

        t2 = muTerm('r', muTerm('1', None),None, 0.25)
        t2.rename_variables()
        self.assertEqual(t2, muTerm('r', muTerm('1', None),None, 0.25))

        t = muTerm('+',t1,t2)
        t.rename_variables()
        self.assertEqual(t, muTerm('+',t1,t2))

        t = muTerm('.',t1,t2)
        t.rename_variables()
        self.assertEqual(t, muTerm('.',t1,t2))

        t = muTerm('M',t1,t2)
        t.rename_variables()
        self.assertEqual(t, muTerm('M',t1,t2))

        t = muTerm('m',t1,t2)
        t.rename_variables()
        self.assertEqual(t, muTerm('m',t1,t2))

        t = muTerm('mu',t2,None,None,'x')
        t.rename_variables()
        self.assertEqual(t,muTerm('mu',t2,None,None,1))

        t = muTerm('nu',t2,None,None,'x')
        t.rename_variables()
        self.assertEqual(t,muTerm('nu',t2,None,None,1))

        t = muTerm('nu',muTerm('mu',t2,None,None,'y'),None,None,'x')
        t.rename_variables()
        t_eq = muTerm('nu',muTerm('mu',t2,None,None,2),None,None,1)
        t_eq.rename_variables()
        self.assertEqual(t,t_eq)

        tvar = muTerm('var',None,None,None,'x')
        with self.assertRaises(ValueError):
            tvar.rename_variables()



if __name__ == '__main__':
    unittest.main()