counter = 8



from evaluating_algorithm import *
from fractions import Fraction

t1 = muTerm('nu',muTerm('mu',muTerm('mu',muTerm('+',muTerm('var',None,None,None,2),muTerm('+',muTerm('m',muTerm('M',muTerm('r',muTerm('var',None,None,None,1),None,0.3348859723441976,None),muTerm('r',muTerm('+',muTerm('r',muTerm('r',muTerm('1',None,None,1,None),None,0.24302186452427144,None),None,0.01003592261925701,None),muTerm('0',None,None,0,None),None,None),None,0.6992778315719036,None),None,None),muTerm('r',muTerm('var',None,None,None,3),None,0.9220922700272144,None),None,None),muTerm('r',muTerm('+',muTerm('r',muTerm('r',muTerm('1',None,None,1,None),None,0.24302186452427144,None),None,0.01003592261925701,None),muTerm('0',None,None,0,None),None,None),None,0.6992778315719036,None),None,None),None,None),None,None,3),None,None,2),None,None,1)
t1.rename_variables()

t2 = muTerm('mu',muTerm('mu',muTerm('nu',muTerm('.',muTerm('var',None,None,None,3),muTerm('M',muTerm('r',muTerm('var',None,None,None,3),None,0.8415185252902345,None),muTerm('m',muTerm('.',muTerm('r',muTerm('1',None,None,1,None),None,0.8554083848561956,None),muTerm('var',None,None,None,2),None,None),muTerm('+',muTerm('r',muTerm('var',None,None,None,1),None,0.46829345255634114,None),muTerm('M',muTerm('.',muTerm('r',muTerm('1',None,None,1,None),None,0.8554083848561956,None),muTerm('var',None,None,None,2),None,None),muTerm('0',None,None,0,None),None,None),None,None),None,None),None,None),None,None),None,None,3),None,None,2),None,None,1)
t2.rename_variables()

t3 = muTerm('nu',muTerm('mu',muTerm('mu',muTerm('+',muTerm('+',muTerm('.',muTerm('1',None,None,1,None),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(0.17893308448778023),None),None,None),muTerm('var',None,None,None,3),None,None),muTerm('m',muTerm('r',muTerm('var',None,None,None,1),None,Fraction(0.7242590130523039),None),muTerm('r',muTerm('+',muTerm('r',muTerm('+',muTerm('r',muTerm('var',None,None,None,2),None,Fraction(0.9103135125398851),None),muTerm('+',muTerm('.',muTerm('1',None,None,1,None),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(0.17893308448778023),None),None,None),muTerm('var',None,None,None,3),None,None),None,None),None,Fraction(0.8589375828484366),None),muTerm('r',muTerm('+',muTerm('r',muTerm('var',None,None,None,2),None,Fraction(0.9103135125398851),None),muTerm('+',muTerm('.',muTerm('1',None,None,1,None),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(0.17893308448778023),None),None,None),muTerm('var',None,None,None,3),None,None),None,None),None,Fraction(0.8589375828484366),None),None,None),None,Fraction(0.24941144536952975),None),None,None),None,None),None,None,3),None,None,2),None,None,1)
t3.rename_variables()

t4 = muTerm('mu',muTerm('nu',muTerm('mu',muTerm('.',muTerm('M',muTerm('+',muTerm('r',muTerm('1',None,None,1,None),None,0.6635939554336246,None),muTerm('var',None,None,None,1),None,None),muTerm('var',None,None,None,3),None,None),muTerm('r',muTerm('var',None,None,None,2),None,0.6649012934300045,None),None,None),None,None,3),None,None,2),None,None,1)
t4.rename_variables()

t5 = muTerm('mu',muTerm('nu',muTerm('mu',muTerm('.',muTerm('M',muTerm('+',muTerm('r',muTerm('1',None,None,1,None),None,Fraction(0.6635939554336246),None),muTerm('var',None,None,None,1),None,None),muTerm('var',None,None,None,3),None,None),muTerm('r',muTerm('var',None,None,None,2),None,Fraction(0.6649012934300045),None),None,None),None,None,3),None,None,2),None,None,1)
t5.rename_variables()

t6 = muTerm('nu',muTerm('mu',muTerm('mu',muTerm('+',muTerm('M',muTerm('0',None,None,0,None),muTerm('m',muTerm('.',muTerm('+',muTerm('m',muTerm('m',muTerm('m',muTerm('1',None,None,1,None),muTerm('r',muTerm('1',None,None,1,None),None,0.3089885877453342,None),None,None),muTerm('1',None,None,1,None),None,None),muTerm('r',muTerm('var',None,None,None,2),None,Fraction(7861847607833971, 9007199254740992),None),None,None),muTerm('m',muTerm('1',None,None,1,None),muTerm('r',muTerm('1',None,None,1,None),None,0.3089885877453342,None),None,None),None,None),muTerm('r',muTerm('var',None,None,None,1),None,Fraction(1862790397950701, 2251799813685248),None),None,None),muTerm('1',None,None,1,None),None,None),None,None),muTerm('r',muTerm('var',None,None,None,3),None,Fraction(3377434910665179, 4503599627370496),None),None,None),None,None,3),None,None,2),None,None,1)
t6.rename_variables()

t7 = muTerm('mu',muTerm('mu',muTerm('nu',muTerm('r',muTerm('.',muTerm('r',muTerm('var',None,None,None,3),None,Fraction(1219781353577131, 2251799813685248),None),muTerm('+',muTerm('var',None,None,None,2),muTerm('M',muTerm('var',None,None,None,1),muTerm('+',muTerm('r',muTerm('1',None,None,1,None),None,0.4696385103715178,None),muTerm('var',None,None,None,2),None,None),None,None),None,None),None,None),None,Fraction(3541721276663595, 9007199254740992),None),None,None,3),None,None,2),None,None,1)
t7.rename_variables()

t8 = muTerm('mu',muTerm('mu',muTerm('nu',muTerm('M',muTerm('r',muTerm('var',None,None,None,1),None,Fraction(7996439381087229, 9007199254740992),None),muTerm('+',muTerm('var',None,None,None,2),muTerm('+',muTerm('M',muTerm('M',muTerm('var',None,None,None,1),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(3248894305368971, 9007199254740992),None),None,None),muTerm('M',muTerm('var',None,None,None,1),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(3248894305368971, 9007199254740992),None),None,None),None,None),muTerm('r',muTerm('var',None,None,None,2),None,Fraction(1119845467165005, 1125899906842624),None),None,None),None,None),None,None),None,None,3),None,None,2),None,None,1)
t8.rename_variables()