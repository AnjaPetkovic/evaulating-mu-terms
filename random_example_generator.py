from muTerms import *
from fractions import Fraction
import random

def generate_random_muTerm(muNumber = 2, nuNumber = 2):
	"""
	Generates a random closed muTerm with nuNumber of nu operators and muNumber of mu operators. All mu's and nu's are at the front.
	"""
	term  = muTerm('r',muTerm('1',None),None,Fraction(random.random()))
	subterms = []
	varNumber = nuNumber + muNumber
	for i in range(1,varNumber + 1):
		subterms.append(muTerm('var',None, None, None, i))
	subterms.append(muTerm('0', None))
	subterms.append(muTerm('1',None))
	muCounter = 0
	nuCounter = 0
	varCounter = 0
	iteration = 0
	while len(term.var_set) != varNumber:
		iteration +=1
		if iteration % 5 == 0:
			op = random.choice(['m','M','+','.'])
			rational = Fraction(random.random())
			x = random.choice(list(set(list(range(1,varNumber+1)))-term.var_set))
			s1 = muTerm('r',muTerm('var',None,None,None,x),None,rational)
			possition = random.choice([1,2])
			if possition == 1:
				term = muTerm(op,term,s1)
			else:
				term = muTerm(op,s1,term)
		op = random.choice(term.possible_operators)
		if op == 'r':
			rational = Fraction(random.random())
			subterms.append(muTerm('r',muTerm('1',None),None,rational))
			term = muTerm('r',term,None,rational)
			subterms.append(term)
		if op in ['m','M','+','.']:
			s1 = random.choice(subterms)
			possition = random.choice([1,2])
			if possition == 1:
				term = muTerm(op,term,s1)
			else:
				term = muTerm(op,s1,term)
			subterms.append(term)
	while muCounter != muNumber or nuCounter != nuNumber:
		op = random.choice(['mu','nu'])
		if op == 'mu':
			if muCounter == muNumber:
				continue
			else:
				muCounter +=1
		else:
			if nuCounter == nuNumber:
				continue
			else:
				nuCounter +=1
		varCounter += 1
		# create a subterm
		term = muTerm(op,term, None, None, varCounter)
		if varCounter == varNumber:
			break	
	term.rename_variables()
	#print(term)
	return term





