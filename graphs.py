import numpy as np
import matplotlib.pyplot as plt

def read_performance_data_graphs(file):
	#read the file
	with open(file,'r') as f:
		data_str = f.readlines()
	data = [[] for x in range(len(data_str)//6 + 1)]
	# get the averages, medians and maximums
	for i in range(len(data_str)):
		if i%6 == 0:
			l = [int(x) for x in data_str[i].strip().split()]
			print(l)
			m = l[0]
			n = l[1]
			N = m+n # number of fixed points is important
			data[N] = []
			medians = []
			maximums = []
		if i%6 == 1:
			averages = eval(data_str[i])
			data[N].append(averages)
		if i%6 == 2:
			term_sizes = eval(data_str[i])
			medians.append(np.median(term_sizes))
			maximums.append(max(term_sizes))
		if i%6 == 3:
			iteration_numbers = eval(data_str[i])
			medians.append(np.median(iteration_numbers))
			maximums.append(max(iteration_numbers))
		if i%6 == 4:
			times = eval(data_str[i])
			medians.append(np.median(times))
			maximums.append(max(times))
		if i%6 == 5:
			set_sizes = eval(data_str[i])
			medians.append(np.median(set_sizes))
			maximums.append(max(set_sizes))
			data[N].append(medians)
			data[N].append(maximums)
	data = list(filter(lambda x: x != [],data))
	return data


def draw_performance_graphs(average = True, median = True, maxi = True, file_evaluate = 'performance_interleaving_evaluate.txt', file_evaluate_optimized = 'performance_interleaving_evaluate_optimized.txt',
	file_evaluate_optimized_direct = 'performance_interleaving_evaluate_optimized_direct.txt',file_evaluate_optimized_direct_qe ='performance_interleaving_evaluate_optimized_direct_qe.txt'):
	# read the data from file
	data_evaluate = read_performance_data_graphs(file_evaluate)
	data_evaluate_optimized = read_performance_data_graphs(file_evaluate_optimized)
	data_evaluate_optimized_direct = read_performance_data_graphs(file_evaluate_optimized_direct)
	data_evaluate_optimized_direct_qe = read_performance_data_graphs(file_evaluate_optimized_direct_qe)

	N = min([len(data_evaluate),len(data_evaluate_optimized),len(data_evaluate_optimized_direct),len(data_evaluate_optimized_direct_qe)])
	x = list(range(N))

	# # plot term sizes

	# fig1, ax1 = plt.subplots()
	# ax1.plot(x,[data_evaluate[j][0][0] for j in range(N)], 'ko', linewidth=1.5, label='Average evaluate')
	# ax1.plot(x,[data_evaluate[j][1][0] for j in range(N)], 'bo', linewidth=1.5, label='Median evaluate')
	# ax1.plot(x,[data_evaluate[j][2][0] for j in range(N)], 'ro', linewidth=1.5, label='Maximum evaluate')

	# ax1.plot(x,[data_evaluate_optimized[j][0][0] for j in range(N)], 'k+', linewidth=1.5, label='Average evaluate optimized')
	# ax1.plot(x,[data_evaluate_optimized[j][1][0] for j in range(N)], 'b+', linewidth=1.5, label='Median evaluate optimized')
	# ax1.plot(x,[data_evaluate_optimized[j][2][0] for j in range(N)], 'r+', linewidth=1.5, label='Maximum evaluate optimized')

	# ax1.plot(x,[data_evaluate_optimized_direct[j][0][0] for j in range(N)], 'kv', linewidth=1.5, label='Average evaluate optimized direct')
	# ax1.plot(x,[data_evaluate_optimized_direct[j][1][0] for j in range(N)], 'bv', linewidth=1.5, label='Median evaluate optimized direct')
	# ax1.plot(x,[data_evaluate_optimized_direct[j][2][0] for j in range(N)], 'rv', linewidth=1.5, label='Maximum evaluate optimized direct')

	# ax1.plot(x,[data_evaluate_optimized_direct_qe[j][0][0] for j in range(N)], 'ks', linewidth=1.5, label='Average evaluate optimized direct qe')
	# ax1.plot(x,[data_evaluate_optimized_direct_qe[j][1][0] for j in range(N)], 'bs', linewidth=1.5, label='Median evaluate optimized direct qe')
	# ax1.plot(x,[data_evaluate_optimized_direct_qe[j][2][0] for j in range(N)], 'rs', linewidth=1.5, label='Maximum evaluate optimized direct qe')


	# ax1.grid(True)
	# ax1.legend(loc='upper left')
	# ax1.set_title('Term sizes')
	# ax1.set_xlabel('Number of \u03bc-s and \u03bd-s')
	# ax1.set_ylabel('Number of operators')

	# plot number of iterations

	fig2, ax2 = plt.subplots()

	if average:
		ax2.plot(x,[np.log10(data_evaluate[j][0][1]) for j in range(N)], '--ko', linewidth=1.5, label='Average evaluate')
		ax2.plot(x,[np.log10(data_evaluate_optimized[j][0][1]) for j in range(N)], '--bo', linewidth=1.5, label='Average evaluate qe')
		ax2.plot(x,[np.log10(data_evaluate_optimized_direct[j][0][1]) for j in range(N)], '--ro', linewidth=1.5, label='Average modified evaluate')
		ax2.plot(x,[np.log10(data_evaluate_optimized_direct_qe[j][0][1]) for j in range(N)], '--mo', linewidth=1.5, label='Average modified evaluate qe')
	if median:
		ax2.plot(x,[np.log10(data_evaluate[j][1][1]) for j in range(N)], '-kv', linewidth=1.5, label='Median evaluate')
		ax2.plot(x,[np.log10(data_evaluate_optimized[j][1][1]) for j in range(N)], '-bv', linewidth=1.5, label='Median evaluate qe')
		ax2.plot(x,[np.log10(data_evaluate_optimized_direct[j][1][1]) for j in range(N)], '-rv', linewidth=1.5, label='Median modified evaluate')
		ax2.plot(x,[np.log10(data_evaluate_optimized_direct_qe[j][1][1]) for j in range(N)], '-mv', linewidth=1.5, label='Median modified evaluate qe')
	if maxi:
		ax2.plot(x,[np.log10(data_evaluate[j][2][1]) for j in range(N)], ':ks', linewidth=1.5, label='Maximum evaluate')
		ax2.plot(x,[np.log10(data_evaluate_optimized[j][2][1]) for j in range(N)], ':bs', linewidth=1.5, label='Maximum evaluate qe')
		ax2.plot(x,[np.log10(data_evaluate_optimized_direct[j][2][1]) for j in range(N)], ':rs', linewidth=1.5, label='Maximum modified evaluate')
		ax2.plot(x,[np.log10(data_evaluate_optimized_direct_qe[j][2][1]) for j in range(N)], ':ms', linewidth=1.5, label='Maximum modified evaluate qe')

	ax2.grid(True)
	ax2.legend(loc='upper left')
	ax2.set_title('Number of iterations of while loop')
	ax2.set_xlabel('Number of \u03bc-s and \u03bd-s')
	ax2.set_ylabel('Log_10 number of iterations')

	# plot times spent od evaluations

	fig3, ax3 = plt.subplots()
	if average:
		ax3.plot(x,[np.log10(data_evaluate[j][0][2]) for j in range(N)], '--ko', linewidth=1.5, label='Average evaluate')
		ax3.plot(x,[np.log10(data_evaluate_optimized[j][0][2]) for j in range(N)], '--bo', linewidth=1.5, label='Average evaluate qe')
		ax3.plot(x,[np.log10(data_evaluate_optimized_direct[j][0][2]) for j in range(N)], '--ro', linewidth=1.5, label='Average modified evaluate')
		ax3.plot(x,[np.log10(data_evaluate_optimized_direct_qe[j][0][2]) for j in range(N)], '--mo', linewidth=1.5, label='Average modified evaluate qe')
	if median:
		ax3.plot(x,[np.log10(data_evaluate[j][1][2]) for j in range(N)], '-kv', linewidth=1.5, label='Median evaluate')
		ax3.plot(x,[np.log10(data_evaluate_optimized[j][1][2]) for j in range(N)], '-bv', linewidth=1.5, label='Median evaluate qe')
		ax3.plot(x,[np.log10(data_evaluate_optimized_direct[j][1][2]) for j in range(N)], '-rv', linewidth=1.5, label='Median modified evaluate')
		ax3.plot(x,[np.log10(data_evaluate_optimized_direct_qe[j][1][2]) for j in range(N)], '-mv', linewidth=1.5, label='Median modified evaluate qe')
	if maxi:
		ax3.plot(x,[np.log10(data_evaluate[j][2][2]) for j in range(N)], ':ks', linewidth=1.5, label='Maximum evaluate')
		ax3.plot(x,[np.log10(data_evaluate_optimized[j][2][2]) for j in range(N)], ':bs', linewidth=1.5, label='Maximum evaluate qe')
		ax3.plot(x,[np.log10(data_evaluate_optimized_direct[j][2][2]) for j in range(N)], ':rs', linewidth=1.5, label='Maximum modified evaluate')
		ax3.plot(x,[np.log10(data_evaluate_optimized_direct_qe[j][2][2]) for j in range(N)], ':ms', linewidth=1.5, label='Maximum modified evaluate qe')
	
	ax3.grid(True)
	ax3.legend(loc='upper left')
	ax3.set_title('Time spent on evaluations')
	ax3.set_xlabel('Number of \u03bc-s and \u03bd-s')
	ax3.set_ylabel('log_10 time [sec]')

	# plot constraint set sizes

	fig4, ax4 = plt.subplots()
	if average:
		ax4.plot(x,[np.log10(data_evaluate[j][0][3]) for j in range(N)], '--ko', linewidth=1.5, label='Average evaluate')
		ax4.plot(x,[np.log10(data_evaluate_optimized[j][0][3]) for j in range(N)], '--bo', linewidth=1.5, label='Average evaluate qe')
		ax4.plot(x,[np.log10(data_evaluate_optimized_direct[j][0][3]) for j in range(N)], '--ro', linewidth=1.5, label='Average modified evaluate')
		ax4.plot(x,[np.log10(data_evaluate_optimized_direct_qe[j][0][3]) for j in range(N)], '--mo', linewidth=1.5, label='Average modified evaluate qe')
	if median:
		ax4.plot(x,[np.log10(data_evaluate[j][1][3]) for j in range(N)], '-kv', linewidth=1.5, label='Median evaluate')
		ax4.plot(x,[np.log10(data_evaluate_optimized[j][1][3]) for j in range(N)], '-bv', linewidth=1.5, label='Median evaluate qe')
		ax4.plot(x,[np.log10(data_evaluate_optimized_direct[j][1][3]) for j in range(N)], '-rv', linewidth=1.5, label='Median modified evaluate')
		ax4.plot(x,[np.log10(data_evaluate_optimized_direct_qe[j][1][3]) for j in range(N)], '-mv', linewidth=1.5, label='Median modified evaluate qe')
	if maxi:
		ax4.plot(x,[np.log10(data_evaluate[j][2][3]) for j in range(N)], ':ks', linewidth=1.5, label='Maximum evaluate')
		ax4.plot(x,[np.log10(data_evaluate_optimized[j][2][3]) for j in range(N)], ':bs', linewidth=1.5, label='Maximum evaluate qe')
		ax4.plot(x,[np.log10(data_evaluate_optimized_direct[j][2][3]) for j in range(N)], ':rs', linewidth=1.5, label='Maximum modified evaluate')
		ax4.plot(x,[np.log10(data_evaluate_optimized_direct_qe[j][2][3]) for j in range(N)], ':ms', linewidth=1.5, label='Maximum modified evaluate qe')

	ax4.grid(True)
	ax4.legend(loc='upper left')
	ax4.set_title('Constraint set sizes')
	ax4.set_xlabel('Number of \u03bc-s and \u03bd-s')
	ax4.set_ylabel('log_10 number of constraints (max)')

	plt.show()

draw_performance_graphs(True, True, True)





