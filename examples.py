from evaluating_algorithm import *
from fractions import Fraction
import sharedModule as shared


testTerm.rename_variables()
t = testTerm.subterm1.subterm2
print("testTerm = ", t)
result = evaluate(t,(1,))
print('testTerm evaluated : ', conditioned_linear_expression_to_string(result[0],result[1]))

complexTerm = muTerm('nu',
	muTerm('mu',
		muTerm('.', 
			muTerm('+', 
				muTerm('r',muTerm('1',None),None,Fraction(5,8)), muTerm('r',muTerm('var',None,None,None,'x0'),None,Fraction(3,8))),
				muTerm('M',muTerm('r',muTerm('1',None),None,Fraction(1,2)),
			muTerm('+',muTerm('r',muTerm('1',None),None,Fraction(3,8)),muTerm('r',muTerm('var',None,None,None,'x1'),None,Fraction(1,2))))),
	None, None, 'x1'),
None, None, 'x0')
print("complexTerm = ", complexTerm)
complexTerm.rename_variables()
tc = complexTerm.subterm1.subterm1
resultC = evaluate(tc,[1,0])
print('complexTermsubterm1.subterm1 evaluated at [1,0] : ', conditioned_linear_expression_to_string(resultC[0],resultC[1]))
tm = complexTerm.subterm1
resultM = evaluate(tm,[1])
print("complexTerm.subterm1 evaluated at [1] : ", conditioned_linear_expression_to_string(resultM[0],resultM[1]))
resultM_opt = evaluate_optimized(tm,[1])
print("complexTerm.subterm1 evaluated at [1] with optimized alg : ", conditioned_linear_expression_to_string(resultM_opt[0],resultM_opt[1]))
resultM1 = evaluate(tc,[1,Fraction(1,2)])
print("complexTerm.subterm1.subterm1 evaluated at [1,1/2] : ", conditioned_linear_expression_to_string(resultM1[0],resultM1[1]))
resultAll = evaluate(complexTerm,[])
print("complexTerm evaluated :  ", conditioned_linear_expression_to_string(resultAll[0],resultAll[1]))

resultAll_opt = evaluate(complexTerm,[])
print("complexTerm evaluated with optimized alg :  ", conditioned_linear_expression_to_string(resultAll_opt[0],resultAll_opt[1]))

resultAll_opt_direct = evaluate(complexTerm,[])
print("complexTerm evaluated with direct optimized alg :  ", conditioned_linear_expression_to_string(resultAll_opt_direct[0],resultAll_opt_direct[1]))


simpleTerm = muTerm('mu', 
	muTerm('nu', 
		muTerm('M', 
			muTerm('.',
				muTerm('var',None,None,None,'y'),
				muTerm('+',
					muTerm('var',None,None,None,'x'),
					muTerm('r',muTerm('1',None),None,Fraction(1,2)))),
			muTerm('r',muTerm('1',None),None,Fraction(1,2))
		),
		 None, None, 'y'), 
	None, None, 'x')
print("simpleTerm = ", simpleTerm)
simpleTerm.rename_variables()
resultSimpleTerm = evaluate(simpleTerm,[])
print("simpleTerm evaluated : ", conditioned_linear_expression_to_string(resultSimpleTerm[0],resultSimpleTerm[1]))

resultSimpleTerm_opt = evaluate_optimized(simpleTerm,[])
print("simpleTerm evaluated with optimized algortihm : ", conditioned_linear_expression_to_string(resultSimpleTerm_opt[0],resultSimpleTerm_opt[1]))

resultSimpleTerm_opt_direct = evaluate_optimized_direct(simpleTerm,[])
print("simpleTerm evaluated with direct optimized algortihm : ", conditioned_linear_expression_to_string(resultSimpleTerm_opt_direct[0],resultSimpleTerm_opt_direct[1]))


resultSubSimpleTerm_opt = evaluate_optimized(simpleTerm.subterm1,[0])
print("subsimpleTerm evaluated with optimized algortihm : ", conditioned_linear_expression_to_string(resultSubSimpleTerm_opt[0],resultSubSimpleTerm_opt[1]))

resultSubSimpleTerm_opt_direct = evaluate_optimized_direct(simpleTerm.subterm1,[0])
print("subsimpleTerm evaluated with direct optimized algortihm : ", conditioned_linear_expression_to_string(resultSubSimpleTerm_opt_direct[0],resultSubSimpleTerm_opt_direct[1]))

smallTerm = muTerm('mu', muTerm('+',muTerm('var',None,None,None,'x'),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(3,8),None),None, None),None,None,'x')
print('smallTerm = ', smallTerm)
smallTerm.rename_variables()
resultSmallTerm = evaluate_optimized_direct(smallTerm,[])
print("smallTerm evaluated : ", conditioned_linear_expression_to_string(resultSmallTerm[0],resultSmallTerm[1]))
