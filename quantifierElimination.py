import sharedModule as shared
from fractions import Fraction

def eliminate_exists_quantifiers_linear_inequalities(C,n):
	""" Eliminates exists quantifiers from a set of linear constraints C, n is the number of bound variables """
	#transform inequalities so they have a form e1 <= e2 or e1 < e2.
	eliminated = []
	for inequality in C:
		ineq_sign, e1, e2 = inequality
		if ineq_sign == ">=":
			eliminated.append(('<=',e2,e1))
		elif ineq_sign == '>':
			eliminated.append(('<',e2,e1))
		else:
			eliminated.append(inequality)
	# eliminate variables one-by-one
	for i in range(n,0,-1): #start eliminating x_n and then go down to x_1
		#rearrange the inequalities so they are of the form x_i <= t_j or s_j <= x_i
		left_terms = []
		right_terms = []
		inequalities_without_x = []
		for inequality in eliminated:
			ineq_sign, e1, e2 = inequality
			# make sure types are ok
			s = sum([e1[i]*e2[i] for i in range(len(e1))])
			if isinstance(s,int) or isinstance(s,Fraction):
				typ = Fraction
			else:
				typ = float
			# rearrange
			if e1[-1] == e2[-1]:
				inequalities_without_x.append((ineq_sign,e1[:-1],e2[:-1]))
			elif e1[-1] - e2[-1] > 0:
				right_terms.append((ineq_sign,[ typ(e2[j] - e1[j])/typ(e1[-1] - e2[-1]) for j in range(len(e1) - 1)]))
			else:
				left_terms.append((ineq_sign,[ typ(e2[j] - e1[j])/typ(e1[-1] - e2[-1]) for j in range(len(e1) - 1)]))
		# add inequalities of the form s_j < t_k
		eliminated = inequalities_without_x
		for j in range(len(left_terms)):
			for k in range(len(right_terms)):
				if left_terms[j][0] == '<' or right_terms[k][0] == '<':
					eliminated.append(('<',left_terms[j][1],right_terms[k][1]))
				else:
					eliminated.append(('<=',left_terms[j][1],right_terms[k][1]))
	satisfied = True
	for inequality in eliminated:
		ineq_sign, e1, e2 = inequality
		if ineq_sign == '<=':
			if not e1[0] <= e2[0]:
				satisfied = False
				break
		elif ineq_sign == '<':
			if not e1[0] < e2[0]:
				satisfied = False
				break
	return satisfied








