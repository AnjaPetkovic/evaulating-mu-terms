\select@language {slovene}
\contentsline {section}{\tocsection {}{1}{Introduction}}{4}{section.1}
\contentsline {section}{\tocsection {}{2}{Complete lattices, fixed points and Knaster-Tarski theorem}}{4}{section.2}
\contentsline {subsection}{\tocsubsection {}{2.1}{Complete lattices}}{4}{subsection.2.1}
\contentsline {subsection}{\tocsubsection {}{2.2}{Monotonic functions and fixed points}}{5}{subsection.2.2}
\contentsline {subsection}{\tocsubsection {}{2.3}{Knaster - Tarski theorem}}{7}{subsection.2.3}
\contentsline {subsection}{\tocsubsection {}{2.4}{Properties of fixed points and nested fixed points}}{8}{subsection.2.4}
\contentsline {section}{\tocsection {}{3}{Piecewise linear functions}}{9}{section.3}
\contentsline {subsection}{\tocsubsection {}{3.1}{Conditioned linear expressions}}{9}{subsection.3.1}
\contentsline {subsection}{\tocsubsection {}{3.2}{First-order theory of linear arithmetic}}{11}{subsection.3.2}
\contentsline {subsection}{\tocsubsection {}{3.3}{Local algorithm}}{13}{subsection.3.3}
\contentsline {section}{\tocsection {}{4}{Iterative algorithm for finding fixed points}}{13}{section.4}
\contentsline {subsection}{\tocsubsection {}{4.1}{Motivating first version on example}}{13}{subsection.4.1}
\contentsline {subsection}{\tocsubsection {}{4.2}{Modified version of the iterative algorithm}}{14}{subsection.4.2}
\contentsline {subsubsection}{\tocsubsubsection {}{4.2.1}{Modified algorithm}}{14}{subsubsection.4.2.1}
\contentsline {subsubsection}{\tocsubsubsection {}{4.2.2}{Correctness and termination of modified algorithm}}{17}{subsubsection.4.2.2}
\contentsline {subsubsection}{\tocsubsubsection {}{4.2.3}{Comparison to the initial version of the algorithm}}{20}{subsubsection.4.2.3}
\contentsline {subsection}{\tocsubsection {}{4.3}{Quantifier elimination optimisation}}{21}{subsection.4.3}
\contentsline {section}{\tocsection {}{5}{$\mu $-terms}}{25}{section.5}
\contentsline {subsubsection}{\tocsubsubsection {}{5.0.1}{Algorithm for evaluating $\mu $-terms}}{28}{subsubsection.5.0.1}
\contentsline {section}{\tocsection {}{6}{Implementation of the algorithm}}{29}{section.6}
\contentsline {subsection}{\tocsubsection {}{6.1}{The class of $\mu $-terms}}{29}{subsection.6.1}
\contentsline {subsection}{\tocsubsection {}{6.2}{Generating examples}}{30}{subsection.6.2}
\contentsline {subsection}{\tocsubsection {}{6.3}{Algorithm evaluate}}{30}{subsection.6.3}
\contentsline {subsection}{\tocsubsection {}{6.4}{Testing the performance}}{31}{subsection.6.4}
\contentsline {section}{\tocsection {}{7}{Results}}{32}{section.7}
\contentsline {section}{\tocsection {}{8}{Conclusion + further work}}{33}{section.8}
\contentsline {section}{\tocsection {}{}{Literatura}}{33}{section*.2}
