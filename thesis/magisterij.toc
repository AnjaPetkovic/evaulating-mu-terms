\select@language {british}
\select@language {british}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Complete lattices, fixed points and the Knaster-Tarski theorem}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Complete lattices}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Monotonic functions and fixed points}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Knaster-Tarski theorem}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Properties of fixed points and nested fixed points}{6}{subsection.2.4}
\contentsline {section}{\numberline {3}Piecewise linear functions}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Conditioned linear expressions}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}First-order theory of linear arithmetic}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Local algorithm}{12}{subsection.3.3}
\contentsline {section}{\numberline {4}Iterative algorithm for finding fixed points}{12}{section.4}
\contentsline {subsection}{\numberline {4.1}Motivating the algorithm on example}{13}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}The iterative algorithm}{14}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Modified algorithm}{15}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Correctness and termination of modified algorithm}{18}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Comparison to the original version of the algorithm from \cite {Mio-Simpson}.}{21}{subsubsection.4.2.3}
\contentsline {subsection}{\numberline {4.3}Quantifier elimination optimisation}{22}{subsection.4.3}
\contentsline {section}{\numberline {5}$\mu $-terms}{28}{section.5}
\contentsline {subsection}{\numberline {5.1}Algorithm for evaluating $\mu $-terms}{31}{subsection.5.1}
\contentsline {section}{\numberline {6}Implementation of the algorithm}{32}{section.6}
\contentsline {subsection}{\numberline {6.1}The class of $\mu $-terms}{33}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Generating examples}{33}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Algorithm evaluate}{35}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Testing the performance}{36}{subsection.6.4}
\contentsline {section}{\numberline {7}Results}{37}{section.7}
\contentsline {section}{\numberline {8}Conclusion and further work}{43}{section.8}
\select@language {slovene}
\contentsline {section}{\numberline {9}Raz\IeC {\v s}irjeni povzetek v sloven\IeC {\v s}\IeC {\v c}ini}{45}{section.9}
\contentsline {subsection}{\numberline {9.1}Uvod}{45}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Mre\IeC {\v z}e, negibne to\IeC {\v c}ke in izrek Knaster-Tarski}{45}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}Odsekovno linearne funkcije}{47}{subsection.9.3}
\contentsline {subsection}{\numberline {9.4}Iterativni algoritem na primeru}{49}{subsection.9.4}
\contentsline {subsection}{\numberline {9.5}Primerjava razli\IeC {\v c}ic algoritma}{51}{subsection.9.5}
\contentsline {subsection}{\numberline {9.6}Implementacija in glavni rezultati}{52}{subsection.9.6}
\select@language {british}
\contentsline {section}{References}{57}{section*.7}
