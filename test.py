from evaluating_algorithm import *
from fractions import Fraction
import time
import sharedModule
import sys

from random_example_generator import *

#generate timeout class to terminate a function if it takes too long to process
import signal

class timeout:
    def __init__(self, seconds=1, error_message='Timeout'):
        self.seconds = seconds
        self.error_message = error_message
    def handle_timeout(self, signum, frame):
        raise TimeoutError(self.error_message)
    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)
    def __exit__(self, type, value, traceback):
        signal.alarm(0)

def find_problematic_examples(file = 'problematic_examples.py'):
	for i in range(10000):
		t = generate_random_muTerm(5,5)
		try:
			with timeout(10):
				resultT = evaluate(t,[])
				if resultT[1][0] != 0 and resultT[1][0] != 1:
					print(linear_expression_to_string(resultT[1]))
		except TimeoutError:
			print('found problematic example')
			with open(file,'r') as f:
				data = f.readlines()
				counter = eval(data[0])
				counter += 1
				data[0] = str(counter) + "\n"
				data.append('t' + str(counter) + ' = ' + repr(t) + '\n')
				data.append('t' + str(counter) + '.rename_variables()\n')
				data.append('\n')
			with open(file,'w') as f:
				for l in data:
					f.write(l)
		#if resultT[1][0] != 0 and resultT[1][0] != 1:
		#	#print(t)
		#	print(linear_expression_to_string(resultT[1]))
		#print('resultT = ', conditioned_linear_expression_to_string(resultT[0], resultT[1]))

def consequent_mu_tests(algorithm = evaluate, in_file = 'testing_consequent_examples.txt',out_file = 'results_testing_consequent.txt'):
	with open(in_file,'r') as f:
		data = f.readlines()
	print(algorithm)
	with open(out_file, 'a') as f:
		for i in range(len(data)):
			if i%3 == 1:
				mus,nus,k = [int(j) for j in data[i].strip().split()]
			if i%3 == 2:
				term = eval(data[i].strip())
				term.rename_variables()
				reduced_term = eliminate_consequent_mu(term,{})
				reduced_term.rename_variables()
				result_term = algorithm(term,[])
				result_reduced_term = algorithm(reduced_term,[])
				if result_term[1] != result_reduced_term[1]:
					print(i,False, file = f)
					print(result_term[1][0], file = f)
					print(result_reduced_term[1][0], file = f)
					print('break')
					break
				print(result_term[1][0], end = ' ', file = f)
				print(result_term[1][0], end = ' ')
				sys.stdout.flush()
		


def performance_test(algorithm = evaluate, in_file = 'testing_interleaving_examples.txt',out_performance_file = 'performance_interleaving_evaluate.txt', out_results_file = 'resluts_interleaving_evaluate.txt' ):
	""" Function for testing evaluating algorithm performance including time for computation, number of while loop iterations and 
	biggest constraint set size

	algorithm : function
		Which algorithm is being tested: evaluate / evaluate_optimized
	in_file: str
		name of the file with examples of terms for testing
	out_performance_file: str
		name of the file to wirte preformance in
	out_results_file: str
		name of the file to write results of the algorithm in 
	"""
	performance_results = []
	iterations_list = [] # how many times we enter the while loop
	times_list = [] # how long the computation takes
	constraint_set_sizes_list = [] # sizes of biggest constraint sets
	term_sizes_list = [] # how many operators do the terms have
	algorithm_results = []


	with open(in_file,'r') as f:
		data = f.readlines()
	kus = int(data[0])
	for i in range(len(data)):
		if i != 0 and i%(3*kus) == 0:
			# calculate the averages and document the performance
			averages = [sum(term_sizes_list)/kus,sum(iterations_list)/kus, sum(times_list)/kus,sum(constraint_set_sizes_list)/kus]
			performance_results = [mus,nus,kus,averages,term_sizes_list,iterations_list,times_list,constraint_set_sizes_list]
			print(mus,nus)
			print("average term size:", averages[0])
			print("average number of iterations:", averages[1])
			print("average time:", averages[2])
			print("average constraint set size:", averages[3])
			# write down the peformance in a file
			with open(out_performance_file,'a') as f:
				print(mus,nus,kus,file = f)
				print(averages,file =f)
				print(term_sizes_list, file = f)
				print(iterations_list, file = f)
				print(times_list,file = f)
				print(constraint_set_sizes_list, file = f)
			with open(out_results_file, 'a') as f:
				print(' '.join(algorithm_results),file = f, end = ' ')
			# reset lists
			performance_results = []
			iterations_list = [] # how many times we enter the while loop
			times_list = [] # how long the computation takes
			constraint_set_sizes_list = [] # sizes of biggest constraint sets
			term_sizes_list = [] # how many operators do the terms have
			algorithm_results = []
		if i%3 == 1:
			mus,nus,k = [int(j) for j in data[i].strip().split()]
		if i%3 == 2:
			t = eval(data[i].strip())
			t.rename_variables()
			term_sizes_list.append(t.size)
			# reset the global counters 
			sharedModule.speed_counter = 0
			sharedModule.constraint_set_size = 0
			# set the start time
			start_time = time.process_time()
			# run the algorithm
			resultT = algorithm(t,[],True, True)
			# get the end time
			end_time = time.process_time()
			# get the results
			times_list.append(end_time - start_time)
			iterations_list.append(sharedModule.speed_counter)
			constraint_set_sizes_list.append(sharedModule.constraint_set_size)
			algorithm_results.append(str(resultT[1]))
	return performance_results

def run_performance_tests(k = 100, N = 10, file = 'performance.txt', algorithm = evaluate):
	""" Function for testing evaluating algorithm performance including time for computation, number of while loop iterations and 
	biggest constraint set size"""
	performance_results = []
	for m in range(N+1):
		mus = m//2
		nus = m-mus
		result = performance_test(k,mus,nus,file,algorithm)
		performance_results.append(result)
	return performance_results

def read_performance(file = 'performance.txt'):
	with open(file,'r') as f:
		data = f.readlines()
	for i in range(len(data)):
		if i%6 == 0:
			m,n,k = data[i].strip().split()
			print('m = ' + m + " n = " + n + " k = " + k)
		if i%6 == 1:
			averages = eval(data[i])
			print("average term size:", averages[0])
			print("average number of iterations:", averages[1])
			print("average time:", averages[2])
			print("average constraint set size:", averages[3])


#result1 = run_performance_tests(10,8,'performance_small.txt',evaluate)
#result2 = run_performance_tests(10,8,'performance_optimized_small.txt',evaluate_optimized)
#result3 = run_performance_tests(10,8,'performance_direct_optimized_small.txt',evaluate_optimized_direct)
#consequent_mu_tests(evaluate,'testing_consequent_examples_small_terms.txt','results_testing_consequent_evaluate.txt')
#consequent_mu_tests(evaluate_optimized,'testing_consequent_examples_small_terms.txt','results_testing_consequent_evaluate_optimized.txt')
#consequent_mu_tests(evaluate_optimized_direct,'testing_consequent_examples_small_terms.txt','results_testing_consequent_evaluate_optimized_direct.txt')
#performance_test(evaluate,'testing_interleaving_examples.txt','performance_interleaving_evaluate.txt', 'resluts_interleaving_evaluate.txt')
#performance_test(evaluate_optimized,'testing_interleaving_examples.txt','performance_interleaving_evaluate_optimized.txt', 'resluts_interleaving_evaluate_optimized.txt')
#performance_test(evaluate_optimized_direct,'testing_interleaving_examples.txt','performance_interleaving_evaluate_optimized_direct.txt', 'resluts_interleaving_evaluate_optimized_direct.txt')
performance_test(evaluate_optimized_direct,'testing_interleaving_examples.txt','performance_interleaving_evaluate_optimized_direct_qe.txt', 'resluts_interleaving_evaluate_optimized_direct_qe.txt')
