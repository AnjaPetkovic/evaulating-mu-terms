from muTerms import *
from fractions import Fraction
import sharedModule as shared


def scalar_prod(linear_expression, vector):
	""" Implementation of evaluating linear expression on a vector via scalar product. """
	vec = [(1 if i == 0 else vector[i-1]) for i in range(len(vector)+1)] # vector has no null component
	if len(linear_expression) != len(vec):
		raise ValueError("Linear expression and vector are not the same size.")
	s = 0
	for i in range(len(vec)):
		s += linear_expression[i]*vec[i]
	return s

def construct_Cd(C,d):
	""" From a linear expression d(x_1,...,x_n) and set of linear constraints C(x_1,...,x_n,x_n+1) constructs
	 a set of linear constraints C(x_1,...,x_n,d(x_1,...,x_n)) """
	Cd = []
	for inequality in C:
		ineq_sign, e1, e2 = inequality
		if ineq_sign == "<":
			Cd.append( ('<',[e1[i]+e1[-1]*d[i] for i in range(len(d))],[e2[i]+e2[-1]*d[i] for i in range(len(d))]) )
		if ineq_sign == "<=":
			Cd.append( ('<=',[e1[i]+e1[-1]*d[i] for i in range(len(d))],[e2[i]+e2[-1]*d[i] for i in range(len(d))]) )
		if ineq_sign == ">":
			Cd.append( ('>',[e1[i]+e1[-1]*d[i] for i in range(len(d))],[e2[i]+e2[-1]*d[i] for i in range(len(d))]) )
		if ineq_sign == ">=":
			Cd.append( ('>=',[e1[i]+e1[-1]*d[i] for i in range(len(d))],[e2[i]+e2[-1]*d[i] for i in range(len(d))]) )
	return Cd

def check_inequalities(C,r_vec):
	""" Checks if set of linear inequalities holds at vector r_vec """
	for inequality in C:
		ineq_sign, e1, e2 = inequality
		if ineq_sign == '<':
			if not scalar_prod(e1,r_vec) < scalar_prod(e2,r_vec):
				return False
		if ineq_sign == '>':
			if not scalar_prod(e1,r_vec) > scalar_prod(e2,r_vec):
				return False
		if ineq_sign == '<=':
			if not scalar_prod(e1,r_vec) <= scalar_prod(e2,r_vec):
				return False
		if ineq_sign == '>=':
			if not scalar_prod(e1,r_vec) >= scalar_prod(e2,r_vec):
				return False
	return True


def find_next_approximation(C, r_vec, D, d, N, e, operator):
	""" Procedure to find next approximation for the fixed point """
	#arrange inequalities
	C_p = []
	B = []
	for inequality in C:
		ineq_sign, e1, e2 = inequality
		s = sum([e1[i]*e2[i] for i in range(len(e1))])
		if isinstance(s,int) or isinstance(s,Fraction):
			typ = Fraction
		else:
			typ = float
		if e1[-1] == 0 and e2[-1] == 0:
			C_p.append(inequality)
		elif operator == 'mu':
			if ineq_sign in ["<", "<="]:
				if e1[-1] - e2[-1] >0:
					b = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)]
					B.append(b)
			else:
				if e2[-1] - e1[-1] >0:
					b = [ typ(e1[i] - e2[i])/ typ(e2[-1] - e1[-1]) for i in range(len(e1) - 1)]
					B.append(b)
		elif operator == 'nu':
			if ineq_sign in [">", ">="]:
				if e1[-1] - e2[-1] >0:
					b = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)]
					B.append(b)
			else:
				if e2[-1] - e1[-1] >0:
					b = [ typ(e1[i] - e2[i])/ typ(e2[-1] - e1[-1]) for i in range(len(e1) - 1)]
					B.append(b)

	# find the infimum/supremum term bj
	B_vals = [scalar_prod(b,r_vec) for b in B]
	if operator == 'mu':
		bjr = min(B_vals)
	elif operator == 'nu':
		bjr = max(B_vals)
	bj = B[ B_vals.index(bjr) ]
	# construct the term e(x,bj(x))
	eb = [e[i]+e[-1]*bj[i] for i in range(len(bj))]
	# construct C(x1,...xn,d(x1,...,xn))
	Cd = construct_Cd(C,d) 
	# return updated D and d
	if operator == 'mu':
		return D + Cd + [N] + [('<=', bj, b) for b in B], eb
	elif operator == 'nu':
		return D + Cd + [N] + [('>=', bj, b) for b in B], eb

def linear_expression_to_string(e):
	outstr = ""
	for i,q in enumerate(e):
		if i == 0:
			outstr += str(q) + " + "
		elif q == 0:
			continue
		else:
			outstr += str(q) + "x_" + str(i) + " + "
	return outstr[:-3]


def conditioned_linear_expression_to_string(C,e):
	outstr = "{  "
	for inequality in C:
		ineq_sign, e1, e2 = inequality
		outstr += linear_expression_to_string(e1) + " " + ineq_sign + " " + linear_expression_to_string(e2) + ", "
	outstr = outstr[:-2] + " } \u22a2  " + linear_expression_to_string(e)
	return outstr

def evaluate(term, r_vec = [], speed_testing = False):
	""" This is a naive algorithm for evaluating mu-terms.

	Linear expressions q0 + q1 x1 + q2 x2 + ... + qn xn are represented as lists [q0, q1, ..., qn].

	Inequalities are represented as tuples ('<',expr1,expr2), ('>',expr1,expr2), ('<=',expr1,expr2), ('>=',expr1,expr2),
	where expr1, expr2 are linear expressions (lists).
	
	"""
	if term.operator == "1":
		e = [0 for i in range(term.var_counter + 1)]
		e[0] = 1
		return ([],e)
	if term.operator == "0":
		e = [0 for i in range(term.var_counter + 1)]
		return ([],e)
	if term.operator == 'var':
		#add constraints that variables are in domain [0,1]
		zero = [0 for i in range(term.var_counter + 1)]
		one = zero[:]
		one[0] = 1
		e = [0 for i in range(term.var_counter + 1)]
		e[term.name] = 1
		return ([('<=',e,one),('>=',e,zero)],e)
	if term.operator == 'r':
		c1, e1 = evaluate(term.subterm1,r_vec, speed_testing)
		e = [term.value*x for x in e1]
		return c1, e
	if term.operator == 'M':
		c1, e1 = evaluate(term.subterm1,r_vec, speed_testing)
		c2, e2 = evaluate(term.subterm2,r_vec, speed_testing)
		if scalar_prod(e1,r_vec) <= scalar_prod(e2,r_vec):
			return (c1 + c2 + [('<=',e1,e2)], e2)
		else:
			return (c1 + c2 + [('>=',e1,e2)], e1)
	if term.operator == 'm':
		c1, e1 = evaluate(term.subterm1,r_vec, speed_testing)
		c2, e2 = evaluate(term.subterm2,r_vec, speed_testing)
		if scalar_prod(e1,r_vec) <= scalar_prod(e2,r_vec):
			return (c1 + c2 + [('<=',e1,e2)], e1)
		else:
			return (c1 + c2 + [('>=',e1,e2)], e2)
	if term.operator == '+':
		c1, e1 = evaluate(term.subterm1,r_vec, speed_testing)
		c2, e2 = evaluate(term.subterm2,r_vec, speed_testing)
		e = [e1[i]+ e2[i]  for i in range(len(e1))]
		one = [0 for i in range(len(e1))]
		one[0] = 1
		if scalar_prod(e1,r_vec) + scalar_prod(e2,r_vec) <= 1:
			return (c1 + c2 + [('<=',e,one)], e)
		else:
			return (c1 + c2 + [('>=',e,one)], one)
	if term.operator == '.':
		c1, e1 = evaluate(term.subterm1,r_vec, speed_testing)
		c2, e2 = evaluate(term.subterm2,r_vec, speed_testing)
		zero = [0 for i in range(len(e1))]
		one = zero[:]
		one[0] = 1
		e = [e1[i]+ e2[i] - one[i] for i in range(len(e1))]
		if scalar_prod(e,r_vec) >= 0:
			return (c1 + c2 + [('>=',e,zero)], e)
		else:
			return (c1 + c2 + [('<=',e,zero)], zero)
	if term.operator == 'mu':
		#raise ValueError("Cannot evaluate terms with operator mu")
		#we make initial values for the loop
		D = []
		d = [0 for i in range(term.var_counter + 1)]
		while True:
			#print('mu')
			#print('d = ', d)
			C , e = evaluate(term.subterm1,r_vec + [scalar_prod(d,r_vec)], speed_testing) # we resursively calculate at (r_vec, d(r_vec))
			# if we are testing the speed, we modify some global variables:
			if speed_testing:
				shared.speed_counter +=1
				if len(C) > shared.constraint_set_size:
					shared.constraint_set_size = len(C)
			#print('mu, e = ',e)
			if e[-1] != 1: # case if q_n+1 != 1
				# calcualte f
				s = sum(e)
				if isinstance(s,int) or isinstance(s,Fraction):
					typ = Fraction
				else:
					typ = float
				q = 1/(1 - typ(e[-1]))
				f = [q * x for x in e[:-1] ]
				# test if C(r_vec,f(f_vec)) is true, we need to check every inequality inside
				rf_vec = r_vec + [scalar_prod(f,r_vec)]
				condition = True
				N = None
				for inequality in C:
					ineq_sign, e1, e2 = inequality
					if ineq_sign == '<':
						if not scalar_prod(e1,rf_vec) < scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) < e2(x1,..,xn,f(x1,..., xn))
							N = ( '>=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>':
						if not scalar_prod(e1,rf_vec) > scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) > e2(x1,..,xn,f(x1,..., xn))
							N = ( '<=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '<=':
						if not scalar_prod(e1,rf_vec) <= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) <= e2(x1,..,xn,f(x1,..., xn))
							N = ( '>', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>=':
						if not scalar_prod(e1,rf_vec) >= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) >= e2(x1,..,xn,f(x1,..., xn))
							N = ( '<', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
				if condition:
					# build C(x1,...,xn,f(x1,...,xn))
					Cf = construct_Cd(C,f) 
					# build C(x1,...,xn,d(x1,...,xn))
					Cd  = construct_Cd(C,d)
					if speed_testing:
						if len(D) + len(Cd) + len(Cf) > shared.constraint_set_size:
							shared.constraint_set_size = len(D) + len(Cd) + len(Cf)
					return D + Cd + Cf , f
				else:
					#find next approximation
					D, d = find_next_approximation(C, r_vec, D, d, N, e, 'mu')		
			else: # case if q_n+1 == 1
				qr = scalar_prod(e[:-1],r_vec)
				#print('qr = ',qr)
				zero = [0 for i in range(len(r_vec)+1)]
				if qr == 0:
					# build C(x1,...,xn,d(x1,...,xn))
					Cd = construct_Cd(C,d)
					Q = [("<=",e[:-1],zero),(">=",e[:-1],zero)]
					if speed_testing:
						if len(D) + len(Cd) + len(Q) > shared.constraint_set_size:
							shared.constraint_set_size = len(D) + len(Cd) + len(Q)
					return D + Cd + Q, d
				# if the equality does not hold, we choose N
				if qr < 0:
					N = ("<",e[:-1],zero)
				else:
					N = (">",e[:-1],zero)
				D,d = find_next_approximation(C, r_vec, D, d, N, e, 'mu')

	if term.operator == 'nu':
		#we make initial values for the loop
		D = []
		d = [0 for i in range(term.var_counter + 1)] 
		d[0] = 1
		while True:
			#print('nu')
			#print('d = ',d)
			C , e = evaluate(term.subterm1,r_vec + [scalar_prod(d,r_vec)], speed_testing) # we resursively calculate at (r_vec, d(r_vec))
			if speed_testing:
				shared.speed_counter +=1
				if len(C) > shared.constraint_set_size:
					shared.constraint_set_size = len(C)
			#print('nu, e = ',e)
			#print('r_vec = ',r_vec) 
			if e[-1] != 1: # case if q_n+1 != 1
				# calcualte f
				s = sum(e)
				if isinstance(s,int) or isinstance(s,Fraction):
					typ = Fraction
				else:
					typ = float
				q = 1/(1 - typ(e[-1]))
				f = [q * x for x in e[:-1] ]
				# test if C(r_vec,f(f_vec)) is true, we need to check every inequality inside
				rf_vec = r_vec + [scalar_prod(f,r_vec)]
				condition = True
				N = None
				for inequality in C:
					ineq_sign, e1, e2 = inequality
					if ineq_sign == '<':
						if not scalar_prod(e1,rf_vec) < scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) < e2(x1,..,xn,f(x1,..., xn))
							N = ( '>=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>':
						if not scalar_prod(e1,rf_vec) > scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) > e2(x1,..,xn,f(x1,..., xn))
							N = ( '<=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '<=':
						if not scalar_prod(e1,rf_vec) <= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) <= e2(x1,..,xn,f(x1,..., xn))
							N = ( '>', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>=':
						if not scalar_prod(e1,rf_vec) >= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) >= e2(x1,..,xn,f(x1,..., xn))
							N = ( '<', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
				if condition:
					# build C(x1,...,xn,f(x1,...,xn))
					Cf = construct_Cd(C,f) 
					# build C(x1,...,xn,d(x1,...,xn))
					Cd  = construct_Cd(C,d)
					if speed_testing:
						if len(D) + len(Cd) + len(Cf) > shared.constraint_set_size:
							shared.constraint_set_size = len(D) + len(Cd) + len(Cf)
					return D + Cd + Cf , f
				else:
					#find next approximation
					D, d = find_next_approximation(C, r_vec, D, d, N, e, 'nu')
			else: # case if q_n+1 == 1
				qr = scalar_prod(e[:-1],r_vec)
				zero = [0 for i in range(len(r_vec)+1)]
				if qr == 0:
					# build C(x1,...,xn,d(x1,...,xn))
					Cd = construct_Cd(C,d)
					Q = [("<=",e[:-1],zero),(">=",e[:-1],zero)]
					if speed_testing:
						if len(D) + len(Cd) + len(Q) > shared.constraint_set_size:
							shared.constraint_set_size = len(D) + len(Cd) + len(Q)
					return D + Cd + Q, d
				# if the equality does not hold, we choose N
				if qr < 0:
					N = ("<",e[:-1],zero)
				else:
					N = (">",e[:-1],zero)
				D,d  = find_next_approximation(C, r_vec, D, d, N, e, 'nu')





from quantifierElimination import *

def add_constraint(C,constraint,n):
	""" Function that adds a constraint only if it is necessary. It simplifies the constraint set when new
	constraint is added. """

	#find negation of the constraint:
	negations_inequalities_sign_dict = {'<=':'>','<':'>=','>':'<=','>=':'<'}
	ineq_sign, e1, e2 = constraint
	negated_constraint = (negations_inequalities_sign_dict[ineq_sign],e1,e2)
	# find out if the new constraint is necessary. Test if the constraint set C with negated constraint is feasible
	necessary = eliminate_exists_quantifiers_linear_inequalities(C +[negated_constraint],n)
	if not necessary:
		return C
	C1 = [constraint] # only the truly necessary constraints
	for i, inequality in enumerate(C):
		ineq_sign, e1, e2 = inequality
		negated_inequality = (negations_inequalities_sign_dict[ineq_sign],e1,e2)
		C_tmp = C1 + C[i+1:]
		necessary_tmp = eliminate_exists_quantifiers_linear_inequalities(C_tmp + [negated_inequality],n)
		if necessary_tmp:
			C1.append(inequality)
	return C1


def find_next_approximation_optimized(C, r_vec, D, d, N, e, operator):
	""" Procedure to find next approximation for the fixed point """
	#arrange inequalities
	C_p = []
	B = []
	for inequality in C:
		ineq_sign, e1, e2 = inequality
		s = sum([e1[i]*e2[i] for i in range(len(e1))])
		if isinstance(s,int) or isinstance(s,Fraction):
			typ = Fraction
		else:
			typ = float
		if e1[-1] == 0 and e2[-1] == 0:
			C_p.append(inequality)
		elif operator == 'mu':
			if ineq_sign in ["<", "<="]:
				if e1[-1] - e2[-1] >0:
					b = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)]
					B.append(b)
			else:
				if e2[-1] - e1[-1] >0:
					b = [ typ(e1[i] - e2[i])/ typ(e2[-1] - e1[-1]) for i in range(len(e1) - 1)]
					B.append(b)
		elif operator == 'nu':
			if ineq_sign in [">", ">="]:
				if e1[-1] - e2[-1] >0:
					b = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)]
					B.append(b)
			else:
				if e2[-1] - e1[-1] >0:
					b = [ typ(e1[i] - e2[i])/ typ(e2[-1] - e1[-1]) for i in range(len(e1) - 1)]
					B.append(b)

	# find the infimum/supremum term bj
	B_vals = [scalar_prod(b,r_vec) for b in B]
	if operator == 'mu':
		bjr = min(B_vals)
	elif operator == 'nu':
		bjr = max(B_vals)
	bj = B[ B_vals.index(bjr) ]
	# construct the term e(x,bj(x))
	eb = [e[i]+e[-1]*bj[i] for i in range(len(bj))]
	# construct C(x1,...xn,d(x1,...,xn))
	Cd = construct_Cd(C,d) 
	# return updated D and d
	if operator == 'mu':
		# check if added constraints are necessary by add_constraint function
		D_new = D[:]
		candidate_constraints =  Cd + [N] + [('<=', bj, b) for b in B]
		for constraint in candidate_constraints:
			D_new = add_constraint(D_new,constraint,len(e)-2)
		return D_new, eb
	elif operator == 'nu':
		# check if added constraints are necessary by add_constraint function
		D_new = D[:]
		candidate_constraints = Cd + [N] + [('>=', bj, b) for b in B]
		for constraint in candidate_constraints:
			D_new = add_constraint(D_new,constraint,len(e)-2)
		return D_new, eb






def evaluate_optimized(term, r_vec = [], speed_testing = False):
	""" This is an optimized algorithm for evaluating mu-terms.

	Linear expressions q0 + q1 x1 + q2 x2 + ... + qn xn are represented as lists [q0, q1, ..., qn].

	Inequalities are represented as tuples ('<',expr1,expr2), ('>',expr1,expr2), ('<=',expr1,expr2), ('>=',expr1,expr2),
	where expr1, expr2 are linear expressions (lists).
	
	Optimization is for the constraint set using quantifier elimination
	"""
	if term.operator == "1":
		e = [0 for i in range(term.var_counter + 1)]
		e[0] = 1
		return ([],e)
	if term.operator == "0":
		e = [0 for i in range(term.var_counter + 1)]
		return ([],e)
	if term.operator == 'var':
		#add constraints that variables are in domain [0,1]
		zero = [0 for i in range(term.var_counter + 1)]
		one = zero[:]
		one[0] = 1
		e = [0 for i in range(term.var_counter + 1)]
		e[term.name] = 1
		return ([('<=',e,one),('>=',e,zero)],e)
	if term.operator == 'r':
		c1, e1 = evaluate_optimized(term.subterm1,r_vec,speed_testing)
		e = [term.value*x for x in e1]
		return c1, e
	if term.operator == 'M':
		c1, e1 = evaluate_optimized(term.subterm1,r_vec, speed_testing)
		c2, e2 = evaluate_optimized(term.subterm2,r_vec,speed_testing)
		if scalar_prod(e1,r_vec) <= scalar_prod(e2,r_vec):
			# check if added constraint is necessary by add_constraint function
			C = add_constraint(c1 + c2,('<=',e1,e2),len(e1)-1)
			return (C, e2)
		else:
			# check if added constraint is necessary by add_constraint function
			C = add_constraint(c1 + c2,('>=',e1,e2),len(e1)-1)
			return (C, e1)
	if term.operator == 'm':
		c1, e1 = evaluate_optimized(term.subterm1,r_vec,speed_testing)
		c2, e2 = evaluate_optimized(term.subterm2,r_vec,speed_testing)
		if scalar_prod(e1,r_vec) <= scalar_prod(e2,r_vec):
			# check if added constraint is necessary by add_constraint function
			C = add_constraint(c1 + c2,('<=',e1,e2),len(e1)-1)
			return (C, e1)
		else:
			# check if added constraint is necessary by add_constraint function
			C = add_constraint(c1 + c2,('>=',e1,e2),len(e1)-1)
			return (C, e2)
	if term.operator == '+':
		c1, e1 = evaluate_optimized(term.subterm1,r_vec,speed_testing)
		c2, e2 = evaluate_optimized(term.subterm2,r_vec,speed_testing)
		e = [e1[i]+ e2[i]  for i in range(len(e1))]
		one = [0 for i in range(len(e1))]
		one[0] = 1
		if scalar_prod(e1,r_vec) + scalar_prod(e2,r_vec) <= 1:
			# check if added constraint is necessary by add_constraint function
			C = add_constraint(c1 + c2,('<=',e,one),len(e1)-1)
			return (C, e)
		else:
			# check if added constraint is necessary by add_constraint function
			C = add_constraint(c1 + c2,('>=',e,one),len(e1)-1)
			return (C, one)
	if term.operator == '.':
		c1, e1 = evaluate_optimized(term.subterm1,r_vec,speed_testing)
		c2, e2 = evaluate_optimized(term.subterm2,r_vec,speed_testing)
		zero = [0 for i in range(len(e1))]
		one = zero[:]
		one[0] = 1
		e = [e1[i]+ e2[i] - one[i] for i in range(len(e1))]
		if scalar_prod(e,r_vec) >= 0:
			# check if added constraint is necessary by add_constraint function
			C = add_constraint(c1 + c2,('>=',e,zero),len(e1)-1)
			return (C, e)
		else:
			# check if added constraint is necessary by add_constraint function
			C = add_constraint(c1 + c2,('<=',e,zero),len(e1)-1)
			return (C, zero)
	if term.operator == 'mu':
		#we make initial values for the loop
		D = []
		d = [0 for i in range(term.var_counter + 1)]
		while True:
			#print('mu')
			#print('d = ', d)
			C , e = evaluate_optimized(term.subterm1,r_vec + [scalar_prod(d,r_vec)],speed_testing) # we resursively calculate at (r_vec, d(r_vec))
			#print(conditioned_linear_expression_to_string(C,e))
			# if we are testing the speed, we modify some global variables:
			if speed_testing:
				shared.speed_counter +=1
				if len(C) > shared.constraint_set_size:
					shared.constraint_set_size = len(C)
			#print('mu, e = ',e)
			if e[-1] != 1: # case if q_n+1 != 1
				# calcualte f
				s = sum(e)
				if isinstance(s,int) or isinstance(s,Fraction):
					typ = Fraction
				else:
					typ = float
				q = 1/(1 - typ(e[-1]))
				f = [q * x for x in e[:-1] ]
				# test if C(r_vec,f(f_vec)) is true, we need to check every inequality inside
				rf_vec = r_vec + [scalar_prod(f,r_vec)]
				condition = True
				N = None
				for inequality in C:
					ineq_sign, e1, e2 = inequality
					if ineq_sign == '<':
						if not scalar_prod(e1,rf_vec) < scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) < e2(x1,..,xn,f(x1,..., xn))
							N = ( '>=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>':
						if not scalar_prod(e1,rf_vec) > scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) > e2(x1,..,xn,f(x1,..., xn))
							N = ( '<=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '<=':
						if not scalar_prod(e1,rf_vec) <= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) <= e2(x1,..,xn,f(x1,..., xn))
							N = ( '>', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>=':
						if not scalar_prod(e1,rf_vec) >= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) >= e2(x1,..,xn,f(x1,..., xn))
							N = ( '<', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
				if condition:
					# build C(x1,...,xn,f(x1,...,xn))
					Cf = construct_Cd(C,f) 
					# build C(x1,...,xn,d(x1,...,xn))
					Cd  = construct_Cd(C,d)
					# check if added constraints are necessary by add_constraint function
					C_new = D[:]
					for constraint in Cf + Cd:
						C_new = add_constraint(C_new,constraint,len(e)-2)
					if speed_testing:
						if len(C_new) > shared.constraint_set_size:
							shared.constraint_set_size = len(C_new)
					return C_new, f
				else:
					#find next approximation
					D, d = find_next_approximation_optimized(C, r_vec, D, d, N, e, 'mu')		
			else: # case if q_n+1 == 1
				qr = scalar_prod(e[:-1],r_vec)
				#print('qr = ',qr)
				zero = [0 for i in range(len(r_vec)+1)]
				if qr == 0:
					# build C(x1,...,xn,d(x1,...,xn))
					Cd = construct_Cd(C,d)
					Q = [("<=",e[:-1],zero),(">=",e[:-1],zero)]
					# check if added constraints are necessary by add_constraint function
					C_new = D[:]
					for constraint in Cd + Q:
						C_new = add_constraint(C_new,constraint,len(e)-2)
					if speed_testing:
						if len(C_new) > shared.constraint_set_size:
							shared.constraint_set_size = len(C_new)
					return C_new, d
				# if the equality does not hold, we choose N
				if qr < 0:
					N = ("<",e[:-1],zero)
				else:
					N = (">",e[:-1],zero)
				D,d = find_next_approximation_optimized(C, r_vec, D, d, N, e, 'mu')

	if term.operator == 'nu':
		#we make initial values for the loop
		D = []
		d = [0 for i in range(term.var_counter + 1)] 
		d[0] = 1
		while True:
			#shared.speed_counter += 1 
			#print('nu')
			#print('d = ',d)
			C , e = evaluate_optimized(term.subterm1,r_vec + [scalar_prod(d,r_vec)],speed_testing) # we resursively calculate at (r_vec, d(r_vec))
			#print(conditioned_linear_expression_to_string(C,e))
			if speed_testing:
				shared.speed_counter +=1
				if len(C) > shared.constraint_set_size:
					shared.constraint_set_size = len(C)
			#print('nu, e = ',e)
			#print('r_vec = ',r_vec) 
			if e[-1] != 1: # case if q_n+1 != 1
				# calcualte f
				s = sum(e)
				if isinstance(s,int) or isinstance(s,Fraction):
					typ = Fraction
				else:
					typ = float
				q = 1/(1 - typ(e[-1]))
				f = [q * x for x in e[:-1] ]
				# test if C(r_vec,f(f_vec)) is true, we need to check every inequality inside
				rf_vec = r_vec + [scalar_prod(f,r_vec)]
				condition = True
				N = None
				for inequality in C:
					ineq_sign, e1, e2 = inequality
					if ineq_sign == '<':
						if not scalar_prod(e1,rf_vec) < scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) < e2(x1,..,xn,f(x1,..., xn))
							N = ( '>=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>':
						if not scalar_prod(e1,rf_vec) > scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) > e2(x1,..,xn,f(x1,..., xn))
							N = ( '<=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '<=':
						if not scalar_prod(e1,rf_vec) <= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) <= e2(x1,..,xn,f(x1,..., xn))
							N = ( '>', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>=':
						if not scalar_prod(e1,rf_vec) >= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) >= e2(x1,..,xn,f(x1,..., xn))
							N = ( '<', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
				if condition:
					# build C(x1,...,xn,f(x1,...,xn))
					Cf = construct_Cd(C,f) 
					# build C(x1,...,xn,d(x1,...,xn))
					Cd  = construct_Cd(C,d)
					# check if added constraints are necessary by add_constraint function
					C_new = D[:]
					for constraint in Cf + Cd:
						C_new = add_constraint(C_new,constraint,len(e)-2)
					if speed_testing:
						if len(C_new) > shared.constraint_set_size:
							shared.constraint_set_size = len(C_new)
					return C_new, f
				else:
					#find next approximation
					D, d = find_next_approximation_optimized(C, r_vec, D, d, N, e, 'nu')
			else: # case if q_n+1 == 1
				qr = scalar_prod(e[:-1],r_vec)
				zero = [0 for i in range(len(r_vec)+1)]
				if qr == 0:
					# build C(x1,...,xn,d(x1,...,xn))
					Cd = construct_Cd(C,d)
					Q = [("<=",e[:-1],zero),(">=",e[:-1],zero)]
					# check if added constraints are necessary by add_constraint function
					C_new = D[:]
					for constraint in Cd + Q:
						C_new = add_constraint(C_new,constraint,len(e)-2)
					if speed_testing:
						if len(C_new) > shared.constraint_set_size:
							shared.constraint_set_size = len(C_new)
					return C_new, d
				# if the equality does not hold, we choose N
				if qr < 0:
					N = ("<",e[:-1],zero)
				else:
					N = (">",e[:-1],zero)
				D,d  = find_next_approximation_optimized(C, r_vec, D, d, N, e, 'nu')
				
########################################################################################################################
################### direct optimization

def find_next_approximation_optimized_direct(C, r_vec, D, d, N, e, operator, quantifier_elim):
	""" Procedure to find next approximation for the fixed point """
	#arrange inequalities
	C_p = []
	B = []
	A = []
	negations_inequalities_sign_dict = {'<=':'>=','<':'>','>':'<','>=':'<='}
	for inequality in C:
		ineq_sign, e1, e2 = inequality
		s = sum([e1[i]*e2[i] for i in range(len(e1))])
		if isinstance(s,int) or isinstance(s,Fraction):
			typ = Fraction
		else:
			typ = float
		if (e1[-1] == 0 and e2[-1] == 0) or (e1[-1]==e2[-1]):
			C_p.append((ineq_sign,e1[:-1],e2[:-1]))
		elif operator == 'mu':
			if ineq_sign in ["<", "<="]:
				if e1[-1] - e2[-1] >0:
					b = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)]
					B.append((b,ineq_sign))
				else:
					a = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)]
					A.append((a,negations_inequalities_sign_dict[ineq_sign]))
			else:
				if e2[-1] - e1[-1] >0:
					b = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) - 1)]
					B.append((b,negations_inequalities_sign_dict[ineq_sign]))
				else:
					a = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)]
					A.append((a,ineq_sign))
		elif operator == 'nu':
			if ineq_sign in [">", ">="]:
				if e1[-1] - e2[-1] >0:
					b = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)]
					B.append((b,ineq_sign))
				else:
					a = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)]
					A.append((a,negations_inequalities_sign_dict[ineq_sign]))
			else:
				if e2[-1] - e1[-1] >0:
					b = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) - 1)]
					B.append((b,negations_inequalities_sign_dict[ineq_sign]))
				else:
					a = [ typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)]
					A.append((a,ineq_sign))

	# find the infimum/supremum term bj
	B_vals = [scalar_prod(b,r_vec) for (b,ineq_sign) in B]
	bjr = min(B_vals) if operator == 'mu' else max(B_vals)
	infimum_term_candidates = [B[i] for i, j in enumerate(B_vals) if j == bjr]
	infimum_term_candidates_ineq = [b[1] for b in infimum_term_candidates]
	# check if there is a strict inequality in infimum terms
	strict_ineq = False
	for i, ineq_sign in enumerate(infimum_term_candidates_ineq):
		if ineq_sign == '<' or ineq_sign == '>':
			strict_ineq = True
			bj, ineq_sign_bj = infimum_term_candidates[i]
			break
	if not strict_ineq:
		bj,ineq_sign_bj = infimum_term_candidates[0]
	# construct the term e(x,bj(x))
	eb = [e[i]+e[-1]*bj[i] for i in range(len(bj))]
	# return updated D and d with picked constraints from C(x1,...xn,d(x1,...,xn))
	if operator == 'mu':
		# check if we have a strict inequaltiy or not and do each case separately
		D_new = D[:]
		if strict_ineq:
			candidate_constraints = [('<', d, bj)] + [N] + C_p + [('<=', bj, b) for (b,ineq_sign) in B] + [(ineq_sign,d,a) for (a, ineq_sign) in A]
		else:
			candidate_constraints = [('<=', d, bj)] + [N] + C_p + [(ineq_sign, bj, b) for (b,ineq_sign) in B] + [(ineq_sign,d,a) for (a, ineq_sign) in A]
		if quantifier_elim:
			for constraint in candidate_constraints:
				D_new = add_constraint(D_new,constraint,len(e)-2)
			return D_new, eb
		else:
			return D_new + candidate_constraints, eb
	elif operator == 'nu':
		D_new = D[:]
		if strict_ineq:
			candidate_constraints = [('>', d, bj)] + [N] + C_p + [('>=', bj, b) for (b,ineq_sign) in B] + [(ineq_sign,d,a) for (a, ineq_sign) in A]
		else:
			candidate_constraints = [('>=', d, bj)] + [N] + C_p + [(ineq_sign, bj, b) for (b,ineq_sign) in B] + [(ineq_sign,d,a) for (a, ineq_sign) in A]
		if quantifier_elim:
			for constraint in candidate_constraints:
				D_new = add_constraint(D_new,constraint,len(e)-2)
			return D_new, eb
		else:
			return D_new + candidate_constraints, eb





def evaluate_optimized_direct(term, r_vec = [], speed_testing = False, quantifier_elim = False):
	""" This is a directly optimized algorithm for evaluating mu-terms.

	Linear expressions q0 + q1 x1 + q2 x2 + ... + qn xn are represented as lists [q0, q1, ..., qn].

	Inequalities are represented as tuples ('<',expr1,expr2), ('>',expr1,expr2), ('<=',expr1,expr2), ('>=',expr1,expr2),
	where expr1, expr2 are linear expressions (lists).
	
	Optimization of the constraint set using quantifier elimination is optional in the argument quantifier_elim
	"""
	if term.operator == "1":
		e = [0 for i in range(term.var_counter + 1)]
		e[0] = 1
		return ([],e)
	if term.operator == "0":
		e = [0 for i in range(term.var_counter + 1)]
		return ([],e)
	if term.operator == 'var':
		#add constraints that variables are in domain [0,1]
		zero = [0 for i in range(term.var_counter + 1)]
		one = zero[:]
		one[0] = 1
		e = [0 for i in range(term.var_counter + 1)]
		e[term.name] = 1
		return ([('<=',e,one),('>=',e,zero)],e)
	if term.operator == 'r':
		c1, e1 = evaluate_optimized_direct(term.subterm1,r_vec,speed_testing, quantifier_elim)
		e = [term.value*x for x in e1]
		return c1, e
	if term.operator == 'M':
		c1, e1 = evaluate_optimized_direct(term.subterm1,r_vec, speed_testing, quantifier_elim)
		c2, e2 = evaluate_optimized_direct(term.subterm2,r_vec,speed_testing,quantifier_elim)
		if scalar_prod(e1,r_vec) <= scalar_prod(e2,r_vec):
			if quantifier_elim:
				# check if added constraint is necessary by add_constraint function
				C = add_constraint(c1 + c2,('<=',e1,e2),len(e1)-1)
				return (C, e2)
			return (c1 + c2 + [('<=',e1,e2)], e2)
		else:
			if quantifier_elim:
				# check if added constraint is necessary by add_constraint function
				C = add_constraint(c1 + c2,('>=',e1,e2),len(e1)-1)
				return (C, e1)
			return (c1 + c2 + [('>=',e1,e2)], e1)
	if term.operator == 'm':
		c1, e1 = evaluate_optimized_direct(term.subterm1,r_vec,speed_testing,quantifier_elim)
		c2, e2 = evaluate_optimized_direct(term.subterm2,r_vec,speed_testing,quantifier_elim)
		if scalar_prod(e1,r_vec) <= scalar_prod(e2,r_vec):
			if quantifier_elim:
				# check if added constraint is necessary by add_constraint function
				C = add_constraint(c1 + c2,('<=',e1,e2),len(e1)-1)
				return (C, e1)
			return (c1 + c2 + [('<=',e1,e2)], e1)
		else:
			if quantifier_elim:
				# check if added constraint is necessary by add_constraint function
				C = add_constraint(c1 + c2,('>=',e1,e2),len(e1)-1)
				return (C, e2)
			return (c1 + c2 + [('>=',e1,e2)], e2)
	if term.operator == '+':
		c1, e1 = evaluate_optimized_direct(term.subterm1,r_vec,speed_testing,quantifier_elim)
		c2, e2 = evaluate_optimized_direct(term.subterm2,r_vec,speed_testing,quantifier_elim)
		e = [e1[i]+ e2[i]  for i in range(len(e1))]
		one = [0 for i in range(len(e1))]
		one[0] = 1
		if scalar_prod(e1,r_vec) + scalar_prod(e2,r_vec) <= 1:
			if quantifier_elim:
				# check if added constraint is necessary by add_constraint function
				C = add_constraint(c1 + c2,('<=',e,one),len(e1)-1)
				return (C, e)
			return (c1 + c2 + [('<=',e,one)], e)
		else:
			if quantifier_elim:
				# check if added constraint is necessary by add_constraint function
				C = add_constraint(c1 + c2,('>=',e,one),len(e1)-1)
				return (C, one)
			return (c1 + c2 + [('>=',e,one)], one)
	if term.operator == '.':
		c1, e1 = evaluate_optimized_direct(term.subterm1,r_vec,speed_testing,quantifier_elim)
		c2, e2 = evaluate_optimized_direct(term.subterm2,r_vec,speed_testing,quantifier_elim)
		zero = [0 for i in range(len(e1))]
		one = zero[:]
		one[0] = 1
		e = [e1[i]+ e2[i] - one[i] for i in range(len(e1))]
		if scalar_prod(e,r_vec) >= 0:
			if quantifier_elim:
				# check if added constraint is necessary by add_constraint function
				C = add_constraint(c1 + c2,('>=',e,zero),len(e1)-1)
				return (C, e)
			return (c1 + c2 + [('>=',e,zero)], e)
		else:
			if quantifier_elim:
				# check if added constraint is necessary by add_constraint function
				C = add_constraint(c1 + c2,('<=',e,zero),len(e1)-1)
				return (C, zero)
			return (c1 + c2 + [('<=',e,zero)], zero)
	if term.operator == 'mu':
		#we make initial values for the loop
		D = []
		d = [0 for i in range(term.var_counter + 1)]
		while True:
			#print('mu')
			#print('d = ', d)
			C , e = evaluate_optimized_direct(term.subterm1,r_vec + [scalar_prod(d,r_vec)],speed_testing,quantifier_elim) # we resursively calculate at (r_vec, d(r_vec))
			#print(conditioned_linear_expression_to_string(C,e))
			# if we are testing the speed, we modify some global variables:
			if speed_testing:
				shared.speed_counter +=1
				if len(C) > shared.constraint_set_size:
					shared.constraint_set_size = len(C)
			#print('mu, e = ',e)
			if e[-1] != 1: # case if q_n+1 != 1
				# calcualte f
				s = sum(e)
				if isinstance(s,int) or isinstance(s,Fraction):
					typ = Fraction
				else:
					typ = float
				q = 1/(1 - typ(e[-1]))
				f = [q * x for x in e[:-1] ]
				#print("f = ", f)
				# test if C(r_vec,f(f_vec)) is true, we need to check every inequality inside
				rf_vec = r_vec + [scalar_prod(f,r_vec)]
				condition = True
				N = None
				for inequality in C:
					ineq_sign, e1, e2 = inequality
					#print(inequality)
					if ineq_sign == '<':
						if not scalar_prod(e1,rf_vec) < scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) < e2(x1,..,xn,f(x1,..., xn))
							N = ( '>=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>':
						if not scalar_prod(e1,rf_vec) > scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) > e2(x1,..,xn,f(x1,..., xn))
							N = ( '<=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '<=':
						if not scalar_prod(e1,rf_vec) <= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) <= e2(x1,..,xn,f(x1,..., xn))
							N = ( '>', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>=':
						if not scalar_prod(e1,rf_vec) >= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) >= e2(x1,..,xn,f(x1,..., xn))
							N = ( '<', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
				if condition:
					# construct the new constraint set C_new deciding whether to add d or f at each constraint individually
					if quantifier_elim:
						C_new = add_constraint(D[:],('<=',d,f),len(d)-1) #this may be buggy!
					else:
						C_new = D[:] + [('<=',d,f)]
					for inequality in C:
						ineq_sign, e1, e2 = inequality
						if ineq_sign in ["<", "<="]:
							if e1[-1] - e2[-1] >0:
								chosen_constraint = (ineq_sign,f, [typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)])
							elif e1[-1] - e2[-1] ==0:
								chosen_constraint = (ineq_sign, e1[:-1], e2[:-1])
							else:
								chosen_constraint = (ineq_sign, [typ(e1[i] - e2[i])/ typ(e2[-1] - e1[-1]) for i in range(len(e1)-1)],d)
						else:
							if e1[-1] - e2[-1] >0:
								chosen_constraint = (ineq_sign,d, [typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)])
							elif e1[-1] - e2[-1] == 0:
								chosen_constraint = (ineq_sign, e1[:-1], e2[:-1])
							else:
								chosen_constraint = (ineq_sign, [typ(e1[i] - e2[i])/ typ(e2[-1] - e1[-1]) for i in range(len(e1) -1)],f)
						if quantifier_elim:
							C_new = add_constraint(C_new,chosen_constraint,len(d)-1)
						else:
							C_new.append(chosen_constraint)
					if speed_testing:
						if len(C_new) > shared.constraint_set_size:
							shared.constraint_set_size = len(C_new)
					return C_new, f
				else:
					#find next approximation
					D, d = find_next_approximation_optimized_direct(C, r_vec, D, d, N, e, 'mu', quantifier_elim)		
			else: # case if q_n+1 == 1
				qr = scalar_prod(e[:-1],r_vec)
				#print('qr = ',qr)
				zero = [0 for i in range(len(r_vec)+1)]
				if qr == 0:
					# build C(x1,...,xn,d(x1,...,xn))
					Cd = construct_Cd(C,d)
					Q = [("<=",e[:-1],zero),(">=",e[:-1],zero)]
					# check if added constraints are necessary by add_constraint function
					C_new = D[:]
					if quantifier_elim:
						for constraint in Cd + Q:
							C_new = add_constraint(C_new,constraint,len(e)-2)
					else:
						C_new = C_new + Cd + Q
					if speed_testing:
						if len(C_new) > shared.constraint_set_size:
							shared.constraint_set_size = len(C_new)
					return C_new, d
				# if the equality does not hold, we choose N
				if qr < 0:
					N = ("<",e[:-1],zero)
				else:
					N = (">",e[:-1],zero)
				D,d = find_next_approximation_optimized_direct(C, r_vec, D, d, N, e, 'mu',quantifier_elim)

	if term.operator == 'nu':
		#we make initial values for the loop
		D = []
		d = [0 for i in range(term.var_counter + 1)] 
		d[0] = 1
		while True:
			#shared.speed_counter += 1 
			#print('nu')
			#print('d = ',d)
			C , e = evaluate_optimized_direct(term.subterm1,r_vec + [scalar_prod(d,r_vec)],speed_testing,quantifier_elim) # we resursively calculate at (r_vec, d(r_vec))
			#print(conditioned_linear_expression_to_string(C,e))			
			if speed_testing:
				shared.speed_counter +=1
				if len(C) > shared.constraint_set_size:
					shared.constraint_set_size = len(C)
			#print('nu, e = ',e)
			#print('r_vec = ',r_vec) 
			if e[-1] != 1: # case if q_n+1 != 1
				# calcualte f
				s = sum(e)
				if isinstance(s,int) or isinstance(s,Fraction):
					typ = Fraction
				else:
					typ = float
				q = 1/(1 - typ(e[-1]))
				f = [q * x for x in e[:-1] ]
				# test if C(r_vec,f(f_vec)) is true, we need to check every inequality inside
				rf_vec = r_vec + [scalar_prod(f,r_vec)]
				condition = True
				N = None
				for inequality in C:
					ineq_sign, e1, e2 = inequality
					#print(inequality)
					if ineq_sign == '<':
						if not scalar_prod(e1,rf_vec) < scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) < e2(x1,..,xn,f(x1,..., xn))
							N = ( '>=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>':
						if not scalar_prod(e1,rf_vec) > scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) > e2(x1,..,xn,f(x1,..., xn))
							N = ( '<=', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '<=':
						if not scalar_prod(e1,rf_vec) <= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) <= e2(x1,..,xn,f(x1,..., xn))
							N = ( '>', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
					if ineq_sign == '>=':
						if not scalar_prod(e1,rf_vec) >= scalar_prod(e2,rf_vec):
							# we found the inequality that is unsatisfied. 
							# N is the negation of e1(x1,..,xn,f(x1,..., xn)) >= e2(x1,..,xn,f(x1,..., xn))
							N = ( '<', [e1[i] + e1[-1]*f[i] for i in range(len(f))], [e2[i] + e2[-1]*f[i] for i in range(len(f))])
							condition = False
							break
				if condition:
					# construct the new constraint set C_new deciding whether to add d or f at each constraint individually
					if quantifier_elim:
						C_new = add_constraint(D[:],('>=',d,f),len(d)-1) #this may be buggy!
					else:
						C_new = D[:] + [('>=',d,f)]
					for inequality in C:
						ineq_sign, e1, e2 = inequality
						if ineq_sign in ["<", "<="]:
							if e1[-1] - e2[-1] >0:
								chosen_constraint = (ineq_sign,d, [typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)])
							elif e1[-1] - e2[-1] ==0:
								chosen_constraint =  (ineq_sign, e1[:-1], e2[:-1])
							else:
								chosen_constraint = (ineq_sign, [typ(e1[i] - e2[i])/ typ(e2[-1] - e1[-1]) for i in range(len(e1) -1)],f)
						else:
							if e1[-1] - e2[-1] >0:
								chosen_constraint = (ineq_sign,f, [typ(e2[i] - e1[i])/ typ(e1[-1] - e2[-1]) for i in range(len(e1) -1)])
							elif e1[-1] - e2[-1] ==0:
								chosen_constraint =  (ineq_sign, e1[:-1], e2[:-1])
							else:
								chosen_constraint = (ineq_sign, [typ(e1[i] - e2[i])/ typ(e2[-1] - e1[-1]) for i in range(len(e1) -1)],d)
						if quantifier_elim:
							C_new = add_constraint(C_new,chosen_constraint,len(d)-1)
						else:
							C_new.append(chosen_constraint)
					if speed_testing:
						if len(C_new) > shared.constraint_set_size:
							shared.constraint_set_size = len(C_new)
					return C_new, f
				else:
					#find next approximation
					D, d = find_next_approximation_optimized_direct(C, r_vec, D, d, N, e, 'nu',quantifier_elim)
			else: # case if q_n+1 == 1
				qr = scalar_prod(e[:-1],r_vec)
				zero = [0 for i in range(len(r_vec)+1)]
				if qr == 0:
					# build C(x1,...,xn,d(x1,...,xn))
					Cd = construct_Cd(C,d)
					Q = [("<=",e[:-1],zero),(">=",e[:-1],zero)]
					# check if added constraints are necessary by add_constraint function
					C_new = D[:]
					for constraint in Cd + Q:
						C_new = add_constraint(C_new,constraint,len(e)-2)
					if speed_testing:
						if len(C_new) > shared.constraint_set_size:
							shared.constraint_set_size = len(C_new)
					return C_new, d
				# if the equality does not hold, we choose N
				if qr < 0:
					N = ("<",e[:-1],zero)
				else:
					N = (">",e[:-1],zero)
				D,d  = find_next_approximation_optimized_direct(C, r_vec, D, d, N, e, 'nu',quantifier_elim)
				
		
		





