\documentclass[12pt]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{url}
\usepackage[reqno]{amsmath}    % basic ams math environments and symbols
\usepackage{amssymb,amsthm}    % ams symbols and theorems
\usepackage{mathtools}         % extends ams with arrows and stuff
\usepackage{url}               % \url and \href for links
\usepackage{graphicx}          % images
\usepackage{enumerate}

\usepackage{algpseudocode}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{corollary}{Corollary}

\newcommand{\R}{\mathbb R}
\newcommand{\N}{\mathbb N}
\newcommand{\Z}{\mathbb Z}
%\renewcommand{\C}{\mathbb C}
\newcommand{\Q}{\mathbb Q}

\newcommand{\Fix}{\operatorname{Fix}}
\newtheorem{example}[definition]{Example}


\title{Seminar: On computing simultaneous fixed points of monotone piecewise linear functions}
\author{Anja Petković }


\begin{document}
	
	\maketitle
	
	\begin{abstract}
		In this talk I will present an iterative (and somewhat optimised) algorithm for computing simultaneous fixed points of monotone piecewise linear functions. We will start with necessary backgrund knowledge about complete lattices and fixed-point calculus and build up to demonstrate the use of that in the algorithm and its optimisations. 
	\end{abstract}
	
\section{Introduction}
Motivation?
	
\section{Complete lattices - definitions and needed properties}

\subsection{Lattices, complete lattices, product lattices, finite partial order}

We will use notation $(E,\leq)$ for an ordered set, where $\leq$ is a partial order (reflexive, antisymmetric and transitive). We will leave out the notation for the ordering and just call $E$ an ordered set, where the relation will be known from the context. A \emph{finite partial order} is a finite ordered set $(E, \leq)$.  Recall that an element $e \in E$ is an \emph{upper bound} of $X$, where $X \subseteq E$, if for all $x \in X$, it holds that $x \leq e$. Similarly $e$ is a \emph{lower bound} if $e \leq x$ for all $x \in X$. An element $e \in E$ is a \emph{least upper bound} of the set $X \subseteq E$, if it is the least element  in the set of all upper bounds, i.~e. the conditions 
\begin{itemize}
	\item $\forall x \in X$, $x \leq e$,
	\item if $\forall x \in X$, $x \leq f$ for some $f\in E$, then $e \leq f$
\end{itemize}
hold. Similarly for \emph{greatest lower bound}. The least upper bound is unique and we will denote it by $\bigvee X$ (and $\bigwedge X$ for the greatest lower bound). Since lower and upper bounds are dual to each other and statements and proofs are very similar for both cases, we will refer to that as \emph{the principle of symmetry}.

\begin{definition}
	A \emph{lattice} is an ordered set $(E, \leq)$ such that for any two elements $x,y \in E$, the set $\{x,y\}$ has a least upper bound $x \vee y$ and a greatest lower bound $x \wedge y$. A \emph{complete lattice} is an ordered set $(E, \leq)$ such that every subset $X \subseteq E$ has a least upper bound $\bigvee X$ and a greatest lower bound $\bigwedge X$.
\end{definition}

In particular a complete lattice has a least element $\bigvee \emptyset$ and a greatest element $\bigwedge \emptyset$.

Another way to construct complete lattices is via product. If $E$ and $F$ are ordered sets, we consider the Cartesian product $E \times F$ with the product ordering
\[
(e,f) \leq (e',f') \iff  (e \leq e' \text{ and } f \leq f').
\]
This is trivially generalised for a product of $n$ ordered sets. Recall the projections $\pi_1$ and $\pi_2$ on $E$ and $F$ respectively.
If $E$ and $F$ are complete lattices, their product is also a complete lattice, since for $X \subseteq E \times F$, we have $\bigwedge X = (\bigwedge \pi_1(X), \bigwedge \pi_2(X))$ and $\bigvee X = (\bigvee \pi_1(X), \bigvee \pi_2(X))$.

We note that for an ordered set to be a complete lattice it is sufficient that $\bigvee X$ exists for every subset $X \subseteq E$.



\subsection{Monotone functions and Knaster-Tarski theorem}

We would like to generalize the notion of monotonic functions on real numbers to be used in ordered sets. 

\begin{definition}
	\label{def:monotonic}
	Let $(E, \leq_E)$ and $(F, \leq_F)$ be two ordered sets. A function $f \colon E \to F$ is said to be \emph{monotonic}, if 
	\[
	\forall x, y \in E, x \leq_E y \implies f(x) \leq_F f(y).
	\]
\end{definition}

For the proof of Knaster-Tarski theorem  later we will also need the following property of monotonic functions.
\begin{proposition}
	\label{prop_mon}
	Let $E$ and $F$ be complete lattices and let $f \colon E \to F$ be \emph{monotonic}. For any subset $X \subseteq E$, 
	\[
	\bigvee_F f(X) \leq_F f(\bigvee_E X)
	\]
	and 
	\[
	f(\bigwedge_E X) \leq_F \bigwedge f(X).
	\]
\end{proposition}

\begin{definition}
	Let $E$ be any set and $f\colon E \to E$ a function. A \emph{fixed point} of $f$ is an element $x \in E$, such that $f(x) = x$. The set of all fixed points of $f$ is denoted by $\Fix(f)$.
\end{definition}

When $E$ is an ordered set, $\Fix(f)$ is an ordered subset of $E$ and it can be empty. 

The most interesting fixed point for us are the least and the greatest fixed points of a monotonic function. But so far we have not even proven the existence of a fixed point for such functions. This is taken care of by the Knaster-Tarski theorem, which ensures the existence of fixed points of monotonic function as well as the existence of the least and greatest one. Note, that $E$ must be a complete lattice rather than just an ordered set. 

\begin{theorem}[Knaster-Tarski]
	\label{Knaster-Tarski}
	Let $(E,\leq)$ be a complete lattice and $f \colon E \to E$ be a monotonic mapping. 
	Then $\bigwedge  \Fix (f)$ and $\bigvee \Fix (f)$ belong to $\Fix(f)$.
\end{theorem}
Moreover, it holds, that 
$$\bigwedge \Fix(f) = \bigwedge \{x \in E| f(x) \leq x\} \in \Fix(f)$$ (we call $x \in E$, such that $f(x) \leq x$ a \emph{pre-fixed point}) and 
$$\bigvee \Fix(f) = \bigvee \{x \in E| x \leq  f(x)\} \in \Fix(f).$$ (we call $x \in E$, such that $x \leq f(x) $ a \emph{post-fixed point}

The Knaster-Tarski theorem gives us a method, by which we can prove that a certain element is a least (or a greatest) fixed point of a monotonic function.

\begin{corollary}
	Let $E$ be a complete lattice and $f \colon E \to E$ a monotonic function. Then $e \in E$ is the least fixed point of $f$ if and only if it satisfies:
	\begin{enumerate}
		\item for each $x \in E$ it holds $f(x) \leq x \implies e \leq x $ ($e$ is smaller or equal to all pre-fixed points),
		\item $f(e) \leq e$ ($e$ is a pre-fixed point).
	\end{enumerate}
	
	Similarly, $e \in E$ is the greatest fixed point of $f$ if and only if it satisfies:
	\begin{enumerate}
		\item for each $x \in E$ it holds $x \leq f(x) \implies x \leq e $,
		\item $e \leq f(e)$.
	\end{enumerate}
\end{corollary}

\subsection{Some fixedpoint calculus}

In the case of fixed points of function $f$ we use the following notation:
\begin{itemize}
	\item for least fixed point, we write $\mu x. f(x)$,
	\item for greatest fixed point, we write $\nu x. f(x)$.
\end{itemize}
The variable $x$ is bound by an extremal fixed point and  we may use renaming of the variables in the following sense $\mu x. f(x) = \mu y. f(y)$, $\nu x. f(x) = \nu y. f(y)$.
The two notions are dual by the principle of symmetry.

We can also have functions in multiple arguments. Suppose $E$ is a complete lattice, $F$ is an ordered set and $f \colon E\times F \to E$ is a monotonic function in its two arguments. For any $y  \in F$ we define $f_y \colon E \to E$ by $f_y(x) = f(x,y)$. We denote $\mu x. f(x,y)$ to be the function from $F$ to $E$ defined by $\mu x. f(x,y) = \mu x. f_y(x)$ (similarly $\nu x. f(x,y)$). The $\mu$ and $\nu$ notation seems to be the standard way to represent the greatest and the least fixed points.

\begin{proposition}
	\label{prop:monotonic_fixed_points}
	Let $E$ be a complete lattice and $F$ an ordered set. If $f \colon E \times F \to E$ is monotonic in its two arguments, then $\mu x. f(x,y)$ and $\nu x. f(x,y)$ are monotonic functions from $F$ to $E$.
\end{proposition} 

No proof?


\begin{lemma}[Golden lemma of $\mu$-calculus]
	\label{Golden_lemma}
	Let $E$ be a complete lattice and $f \colon E \times E \to E$ a monotonic function in both arguments. Then
	\[
	\mu x. \mu y. f(x,y) = \mu x. f(x,x) = \mu y. \mu x. f(x,y)
	\]
	and 
	\[
	\nu x. \nu y. f(x,y) = \nu x. f(x,x) = \nu y. \nu x. f(x,y).
	\]
\end{lemma}

No proof?

\subsection{Simultaneous fixed point and Bekić principle}

When dealing with (least) fixed points of product lattices, it is useful to know, that we can compute the fixed point iteratively, which is demonstrated in the Bekić theorem. 

\begin{theorem}[Bekić principle]
	For two dimensions, a simulatneous fixed point can be calculated as:
	$$
	\mu 
	\begin{bmatrix}
	x_1 \\
	x_2
	\end{bmatrix}. 
	\begin{bmatrix}
	f_1(x_1, x_2, \vec{y})\\
	f_2(x_1,x_2,\vec{y})
	\end{bmatrix} = 
	\begin{bmatrix}
	\mu x_1. f_1(x_1, \mu x_2. f_2(x_1, x_2, \vec{y}), \vec{y}) \\
	\mu x_2. f_2(\mu x_1. f_1(x_1, \mu x_2. f_2(x_1, x_2, \vec{y}), \vec{y}),x_2, \vec{y})
	\end{bmatrix}.
	$$
\end{theorem}

This generalises to for $n$ variables using the Gaussian principle (see \cite[pg. 31: Gaussian elimination principle]{rudiments}) in the following way:

\begin{theorem}[Generalised Bekić principle]
	The simultaneous fixed point 
	$$
	\begin{bmatrix}
	a_1(\vec{y}) \\
	a_2(\vec{y}) \\
	\vdots \\
	a_n(\vec{y})
	\end{bmatrix} = 
	\mu 
	\begin{bmatrix}
	x_1 \\
	x_2 \\
	\vdots \\
	x_n
	\end{bmatrix}. 
	\begin{bmatrix}
	t_1(x_1, x_2, \ldots, x_n, \vec{y}) \\
	t_2(x_1, x_2, \ldots, x_n, \vec{y}) \\
	\vdots \\
	t_n(x_1, x_2, \ldots, x_n, \vec{y})
	\end{bmatrix} 
	$$ can be calculated in the following way:
	\begin{enumerate}
		\item Calculate $g_n(x_1, x_2, \ldots, x_{n-1}, \vec{y}) = \mu x_n. t_n(x_1, x_2, \ldots, x_n, \vec{y})$.
		\item Recursively calculate 
		$$
		\begin{bmatrix}
		a_1(\vec{y}) \\
		\vdots \\
		a_{n-1}(\vec{y})
		\end{bmatrix} = 
		\mu 
		\begin{bmatrix}
		x_1 \\
		\vdots \\
		x_{n-1}
		\end{bmatrix}. 
		\begin{bmatrix}
		t_1(x_1, x_2, \ldots, x_{n-1},g_n(x_1, \ldots, x_{n-1}, \vec{y}), \vec{y}) \\
		t_2(x_1, x_2, \ldots, x_{n-1},g_n(x_1, \ldots, x_{n-1}, \vec{y}), \vec{y}) \\
		\vdots \\
		t_{n-1}(x_1, x_2, \ldots, x_{n-1},g_n(x_1, \ldots, x_{n-1}, \vec{y}), \vec{y})
		\end{bmatrix} 
		$$
		\item $a_n(\vec{y}) = g_n(a_1(\vec{y}), \ldots, a_{n-1}(\vec{y}), \vec{y}) $
	\end{enumerate}
	\label{thm:generalised_bekic_principle}
\end{theorem}

\section{Piecewise linear functions}

\subsection{Conditioned linear expressions}

Since we are dealing with piecewise linear functions, we take a closer look at their definition. The term itself suggests that we slice the domain of a function into pieces and on every piece we have a linear expression. The function need not be continuous as we can see in the one-dimensional example in Figure \ref{fig:piecewise_linear_function_example}.
\begin{figure}[!h]
	\centering
	\includegraphics{piecewise_linear_function_example.pdf}
	\caption{A discontinuous monotone piecewise linear function on $[0,1]$. An open interval is indicated by an arrow for us to see, that the function takes a unique value at every point in the domain.}
	\label{fig:piecewise_linear_function_example}
\end{figure}


\begin{definition}
	A \emph{linear expression} in variables $x_1, x_2, \ldots, x_n$ is an expression
	\[
	q_0 + q_1 x_1 + q_2 x_2 +  \ldots + q_n x_n,
	\]
	where $q_0, \ldots, q_n$ are real numbers. We say that a linear expression is \emph{rational} if all $q_0, \ldots, q_n$ are rational numbers. 
\end{definition}

Since we will mostly be dealing with rational linear expressions, we will omit the word ``rational" and only use the term linear expression.

We write $e(x_1, \ldots, x_n)$ to mean a linear expression in variables $x_1, \ldots, x_n$ and if we want to take real numbers $r_1, \ldots, r_n$ to replace the variables, we write $e(\vec{r})$. Linear expressions are closed under substitution.

\begin{definition}
	A \emph{conditioned linear expression} is a pair $C \vdash e$, where $e$ is a linear expression and $C$ is a finite set of strict and non-strict inequalities between linear expressions, i.e. each element in $C$ has one of the forms
	\begin{eqnarray}
	\label{linear_inequalities}
	e_1 \leq e_2, && e_1 < e_2.
	\end{eqnarray}
\end{definition}
We write $C(\vec{r})$ to mean the conjunction of inequalities, in which variables in $C$ are replaced by real numbers $r_1, \ldots, r_n$. We use a conditioned linear expression $C \vdash e$ to express one piece of a piecewise linear function. The domain of the piece is the set of vectors $\vec{r}$ that satisfy $C(\vec{r})$ and the expression $e$ sprecifies the linear function over that domain.

\begin{proposition}
	\label{convex_domain}
	The domain $\{ \vec{r} \mid C(\vec{r})\}$ is always convex, which means if $C(\vec{r})$ and $C(\vec{s})$, then for all $\lambda \in [0,1]$ it holds that $C(\lambda \vec{r} + (1-\lambda) \vec{s})$.
\end{proposition}

Note that the domain of a piece does not need to be open or closed and may be empty. Since it is given by a set of linear inequalities, we have a clear idea of the form of the domain. Every linear inequality gives us a half-space (a closed half-space from strict inequality and an open half-space from non-strict inequality). We therefore have a finite intersection of half-spaces, some closed and some open. 
If the inequalities were merely non-strict, we would get a closed convex polytope. Our domains are more general since we allow a mix of both types of inequalities, but we can be sure, that the closure of the domain is in fact a closed convex polytope.

\begin{definition}
	A function $f \colon [0,1]^n \to [0,1]$ is \emph{piecewise linear} if there exists a finite set $\mathcal{F}$ of conditioned linear expressions in variables $x_1, \ldots, x_n$, such that the following conditions hold:
	\begin{enumerate}
		\item For all $\vec{r} \in [0,1]^n$, there exists a conditioned linear expression $(C \vdash e) \in \mathcal{F}$ such that $C(\vec{r})$ holds and
		\item for all $\vec{r} \in [0,1]^n$ and every conditioned linear expression $(C \vdash e) \in \mathcal{F}$, if $C(\vec{r})$ holds, then $f(\vec{r}) = e(\vec{r})$.
	\end{enumerate}
	We say, that such $\mathcal{F}$ \emph{represents} $f$.
\end{definition}
Note, that two conditional linear expressions $(C_1 \vdash e_1) \in \mathcal{F}$ and $(C_2 \vdash e_2) \in \mathcal{F}$ need not have disjoint domains, however $e_1$ and $e_2$ must agree on any overlap.

\begin{definition}
	A function $f \colon [0,1]^{n+k} \to [0,1]^n$ is \emph{piecewise linear} if there exists a finite set $\mathcal{F}$ of $n$-tuples of conditioned linear expressions in variables $x_1, \ldots, x_n,\vec{y}$, such that the following conditions hold:
	\begin{enumerate}
		\item For all $(\vec{s},\vec{r}) \in [0,1]^{n+k}$, there exists an $n$-tuple of conditioned linear expressions $(C_1 \vdash e_1, C_2 \vdash e_2, \ldots, C_n \vdash e_n) \in \mathcal{F}$ such that $C_i(\vec{s},\vec{r})$ holds for all $1 \leq i \leq n$ and
		\item for all $(\vec{s},\vec{r}) \in [0,1]^{n+k}$ and every $n$-tuple of conditioned linear expressions $(C_1 \vdash e_1, C_2 \vdash e_2, \ldots, C_n \vdash e_n) \in \mathcal{F}$, if $C_i(\vec{s},\vec{r})$ holds for all $1 \leq i \leq n$, then $f(\vec{s},\vec{r}) = (e_1(\vec{s},\vec{r}), e_2(\vec{s},\vec{r}), \ldots, e_n(\vec{s},\vec{r}))$.
	\end{enumerate}
	We say, that such $\mathcal{F}$ \emph{represents} $f$.
\end{definition}


\begin{example}
	\label{exam:piecewise_linear_function_example}
	We again look at the one-dimensional example in Figure \ref{fig:piecewise_linear_function_example}. The function is given by the following conditioned linear expression:
	\begin{eqnarray*}
		0 \leq x < \frac{1}{3} & \vdash & \frac{1}{2}x + \frac{1}{3}\\
		\frac{1}{3} \leq x < \frac{2}{3} & \vdash & \frac{1}{4}x + \frac{7}{12}\\
		\frac{2}{3} \leq x \leq 1 & \vdash& \frac{5}{6}.
	\end{eqnarray*}
	It is discontinuous and has its domain sliced into three pieces. 
\end{example}

\subsection{Local algorithm}

If one is just interested in computing a piecewise linear function $f$, then all one needs to do is have an algorithm that, given a vector $\vec{r}$ as input, outputs the value $f(\vec{r})$. 
But sometimes we need more information. One possibility would be to explicitly carry around the full representation of the function as a finite set of conditioned linear expressions. However, this can be extremely large. Instead we will work with a less explicit form of representation, namely a local algorithm, that given a point in the domain, outputs an appropriate $n$-tuple of conditioned linear expressions. 

\begin{definition}
	\label{def:local_alg}
	A \emph{local algorithm} for a piecewise linear function $f \colon [0,1]^{n+k} \to [0,1]^n$ is an algorithm, that for $(\vec{s},\vec{r})) \in [0,1]^{n+k}$ outputs an $n$-tuple of conditioned linear expressions $(C_1 \vdash e_1, C_2 \vdash e_2, \ldots, C_n \vdash e_n) $ such that
	\begin{enumerate}
		\item $C_i(\vec{s},\vec{r})$ holds for all $1 \leq i \leq n$ and
		\item if for any $(\vec{u}, \vec{v}) \in [0,1]^{n+k}$ $C_i(\vec{u}, \vec{v})$ holds for all $1 \leq i \leq n$, then $f(\vec{u}, \vec{v}) = (e_1(\vec{u},\vec{v}), e_2(\vec{u},\vec{v}), \ldots, e_n(\vec{u},\vec{v})$.
	\end{enumerate}
	Furthermore, only finitely many distinct $(C_1 \vdash e_1, C_2 \vdash e_2, \ldots, C_n \vdash e_n) $  can be returned.
\end{definition}
It is worth remarking, that an explicit representation is easily converted to a local algorithm, and a local algorithm trivially provides a way of mapping input vectors to output values.
However, there are cases, where we have a far more efficient representation via local algorithms. 

No further information about how the local algorithms carry out their computation is required. Thus local algorithms can be viewed quite abstractly as providing a sort of "black box”, whose internals are hidden, whose only requirement is to map input vectors to appropriate conditioned linear expressions.

\section{Iteration algorithm for computing fixed points}

\subsection{Statement of the problem}

\subsection{Statement of the generalised problem}

We are working in the following setting: we are given a function $t \colon [0,1]^n \times [0,1]^k \to [0,1]^n$, which is monotone (as a map between lattices) in the first $n$ variables and piecewise linear. We use notation $(\vec{x}, \vec{y})$ for elements of $[0,1]^n \times [0,1]^k$ and we think of the vector $\vec{y}$ as the vector of parameters. We name the components of $\vec{x}$ as $(x_1, x_2, \ldots, x_n)$. The function is given by a local algorithm, which given an input vector $(\vec{s}, \vec{r}) \in [0,1]^n \times [0,1]^k$ yields $n$ conditioned linear expressions 
\begin{align*}
C_1(x_1, x_2, \ldots, x_n, \vec{y}) & \vdash  e_1(x_1, x_2, \ldots, x_n, \vec{y}) \\
C_2(x_1, x_2, \ldots, x_n, \vec{y}) & \vdash  e_2(x_1, x_2, \ldots, x_n, \vec{y}) \\
\vdots & \\
C_n(x_1, x_2, \ldots, x_n, \vec{y}) & \vdash  e_n(x_1, x_2, \ldots, x_n, \vec{y}), \\
\end{align*}
such that for all $i \in \{1,2,\ldots.n\}$,  $C_i(\vec{s},\vec{r})$ holds and 
$$t(\vec{s},\vec{r}) = \left ( e_1(\vec{s},\vec{r}), e_2(\vec{s},\vec{r}), \ldots, e_n(\vec{s},\vec{r})\right).$$
We are calculating the simultaneous least fixed point of the first $n$ components, meaning we are looking for
\begin{equation}
\mu 
\begin{bmatrix}
x_1 \\
x_2 \\
\vdots \\
x_n
\end{bmatrix}. t(x_1, x_2, \ldots, x_n, \vec{y}).
\end{equation}

Keeping in mind that the procedure for calculating greatest fixed point is dual to the procedure for the least fixed point by the principle of symmetry, we will focus only on the least fixed points. 

Rather than computing the enitre set of conditioned linear expressions, the algorithm works locally and provides a single $n$-tuple of conditioned linear expressions that applies to the given input vector $(\vec{s},\vec{r})$. Since we are dealing with piecewise linear functions locally, we represent them by local algorithms as defined in \ref{def:local_alg}.

We are of course dealing with rational piecewise linear functions and we are inputing rational numbers, since the real world computers can only precisely calculate rationals and not arbitrary reals. Because we have a working implementation of the algorithm, this restriction in in fact necessary. However it is convenient to consider the running of the algorithm in the general case, where $(\vec{s},\vec{r})$ are arbitrary real vectors in $[0,1]^{n+k}$. This can be understood as an algorithm in the \emph{Real RAM} model of computation. When we input rational numbers, all real numbers generated in the algorithm are rational themselves and all linear expressions generated are rational.

\subsection{Algorithm on an example}
IN ONE DIMENSION?

Fixed point are computed by iterating through their approximations. We start with vecor $\vec{0}$ for the least fixed point and $\vec{1}$ for the greatest fixed point. To illustrate the main idea we take another look at the piecewise linear function from example \ref{exam:piecewise_linear_function_example}. The function $f \colon [0,1] \to [0,1]$ is given by the following conditioned linear expression:
\begin{eqnarray*}
	0 \leq x < \frac{1}{3} & \vdash & \frac{1}{2}x + \frac{1}{3}\\
	\frac{1}{3} \leq x < \frac{2}{3} & \vdash & \frac{1}{4}x + \frac{7}{12}\\
	\frac{2}{3} \leq x \leq 1 & \vdash& \frac{5}{6}.
\end{eqnarray*}
 Clearly $f$ is monotonic and it has a unique fixed point at $\frac{5}{6}$.

The algorithm calculates the fixed point by iteratively correcting an approximation $d$. We start with $d = 0$. Note, that simple iteration of $f$ from $0$ (like in Banach fixed point theorem) $0  \leq f(0) \leq f(f(0)) \leq \ldots$ generally does not work, since the sequence may never reach a fixed point. In the Banach fixed point theorem this sequence may not reach a fixed point in finite time, but it is guaranteed to converge to one. In our setting, the limit of the sequence need not be a fixed point because of discontinuity of $f$. Instead we iterate through pieces.

The initial approximation is $d = 0$ and we retrieve the linear piece for $x = 0$ given by the conditioned linear expression $0 \leq x < \frac{1}{3}  \vdash  \frac{1}{2}x + \frac{1}{3}$. The unique solution of $x = \frac{1}{2}x + \frac{1}{3}$ is $x = \frac{2}{3}$, which is outside the domain for this piece. So we replace $d$ by a new approximation, given by $ \frac{1}{2}x + \frac{1}{3}$ calculated at the upper bound $x = \frac{1}{3}$ of the domain. Therefore the next approximation for the fixed point is $x = \frac{1}{2}$.

We again retrieve the linear piece for $x = \frac{1}{2}$, which is given by $\frac{1}{3} \leq x < \frac{2}{3}  \vdash  \frac{1}{4}x + \frac{7}{12}$. The solution to the equation $x =  \frac{1}{4}x + \frac{7}{12}$ is $x = \frac{7}{9}$, which is again outside the domain of the linear piece. So we find the next approximation by calculating $ \frac{1}{4}x + \frac{7}{12}$ at $x = \frac{2}{3}$ and we have $d = \frac{3}{4}$.

Finally we retrieve the last piece for $x = \frac{3}{4}$, which is given by $\frac{2}{3} \leq x \leq 1  \vdash  \frac{5}{6}$. We get a candidate $x = \frac{5}{6}$, which in fact is in the domain of the current linear piece and therefore the least fixed point of $f$.
Although on this example, we examine all pieces of the domain, that is not necessarily the case.

The general algorithm for finding fixed points of monotone piecewise linear functions with $n$ arguments, where $n >1$ is substantially more complicated, because we are calculating fixed points of linear expressions rather than just numbers. 

The main difference is, that the parameters need to be taken into account, we need to keep track of current restrictions, we are building up the domain of the piece iteratively as well, and calculating the upper bound of the piece is done by isolating one variable and evaluating linear expressions at the chosen vector $(\vec{s}, \vec{r})$. 


%\subsection{Iterations on a finite lattice}

\section{Generalisation to simultaneous fixed point on product lattices}

\subsection{Failed attempts of generalisation?}
\begin{itemize}
	\item Fisrt attempt was to directly solve a system of lienar equations on each piece. The problem arises how to efficiently choose a minimal one, when we have a subspace of possible solutions, given the fact, that we are working on a non-closed domain
	\item iteration on diagonal: we found a counter example, where it does not converge
	\item iteration by Bekić starting from 0 each time: huge constraint sets??
\end{itemize}

%\subsection{Iterations on a finite lattice?}

\subsection{Different approaches to iteration}

Let us demonstrate different apporaches to iteration on the following example.
Let $h \colon [0,1]^2 \to [0,1]^2$ be the function
$$
h(x,y) = \left (\frac{y+1}{2}, \frac{x+y+1}{2} \right ) = (f(x,y),g(x,y)).
$$

We would like to solve the system of equations
$$
(x,y) = h(x,y)
$$

If we solve that via gaussian elimination, we get $x = \frac{4}{5}$ and $y = \frac{3}{5}$.
But if we try iterating the function we can get different approaches:
\begin{enumerate}
	\item Diagonal approach:
	\begin{align*}
		x = & f(0,0) = \frac{1}{2}, & f(\frac{1}{2},\frac{1}{4}) = \frac{5}{8}, & \frac{23}{32} & \ldots \\
		y = & g(0,0) = \frac{1}{4}, & g(\frac{1}{2},\frac{1}{4}) = \frac{7}{16}, & \frac{33}{64} & \ldots
	\end{align*}
	\item iterative approach starting from 0:
	$$x = 0$$
	\begin{align*}
		y = 0,& g(0,0) = \frac{1}{4}, &  \frac{5}{16}, & \frac{21}{64} & \ldots & \frac{1}{3}
	\end{align*}
	$$x = f(0, \frac{1}{3}) = \frac{2}{3} $$
	\begin{align*}
		y = 0,& \ldots \\
	\end{align*}
	\item iterative approach starting from previous result due to monotonicity
	$$x = 0$$
	\begin{align*}
		y = 0,& g(0,0) = \frac{1}{4}, &  \frac{5}{16}, & \frac{21}{64} & \ldots & \frac{1}{3}
	\end{align*}
	$$x = f(0, \frac{1}{3}) = \frac{2}{3} $$
	\begin{align*}
		y = \frac{1}{3},& \ldots \\
	\end{align*}
	

\end{enumerate}
The reason for parameters is, because when using Bekić principle, all the other variables become parameters and we need a way to handle them. 

\subsection{Optimised version of the generalised algorithm}	

\begin{algorithmic}
	\State \textbf{Input:} A vector $\vec{r} \in [0,1]^k$ of rational numbers  and a piecewise linear function $t \colon [0,1]^n \times [0,1]^k \to [0,1]^n$ with components $t_1(\vec{x},\vec{y})$, $t_2(\vec{x},\vec{y})$, \ldots,  $t_n(\vec{x},\vec{y})$, that is monotone in variables $x_1, \ldots, x_n$ and is given by a local algorithm. 
	
	\State \textbf{Output:} Conditioned linear expessions $D_1(\vec{y}) \vdash d_1(\vec{y})$, $D_2(\vec{y}) \vdash d_2(\vec{y})$, \ldots, $D_n(\vec{y}) \vdash d_n(\vec{y})$, that represent the fixed point 
	\begin{equation*}
	\mu 
	\begin{bmatrix}
	x_1 \\
	x_2 \\
	\vdots \\
	x_n
	\end{bmatrix}. t(x_1, x_2, \ldots, x_n, \vec{y}),
	\end{equation*}
	at vector $\vec{r}$ and therefore have the following properties:
	\begin{enumerate}[(P1)]
		\item $D_i(\vec{r})$ holds for $i = 1, 2, \ldots, n$,
		\item for all $\vec{s} \in \mathbb{R}^k$, if $D_i(\vec{s})$ holds for $i = 1, 2, \ldots, n$, then $\vec{s} \in [0,1]^k$ and 
		\begin{equation*}
		\begin{bmatrix}
		d_1(\vec{s}) \\
		d_2(\vec{s}) \\
		\vdots \\
		d_n(\vec{s})
		\end{bmatrix} 
		= 
		\mu 
		\begin{bmatrix}
		x_1 \\
		x_2 \\
		\vdots \\
		x_n
		\end{bmatrix}. t(x_1, x_2, \ldots, x_n, \vec{s}).
		\end{equation*}
	\end{enumerate}
	
	
	
	\State Let $d_1(\vec{y})$, $d_2(x_1,\vec{y}), \ldots, d_n(x_1, x_2, \ldots, x_{n-1}, \vec{y})$ be the current approximations of the fixpoint and $D_1(\vec{y})$, $D_2(x_1,\vec{y}), \ldots, D_n(x_1, x_2, \ldots, x_{n-1}, \vec{y})$ current approximations of constraint sets for variables $x_1, x_2, \ldots x_n$. For $m \in \{1, 2, \ldots n\}$ we use notation $d_m(\vec{r})$ for $d_m(d_1(\vec{r}), d_2(d_1(\vec{r}),\vec{r}), \ldots, \vec{r})$. 
	
	\State Initialize values with $D_1(\vec{y}) = D_2(x_1, \vec{y}) = \ldots = D_n(x_1, \ldots, x_{n-1},\vec{y} ) = \emptyset$ and
	$d_1(\vec{y}) = d_2(x_1, \vec{y}) = \ldots =  d_n(x_1, \ldots, x_{n-1},\vec{y} ) = 0$.
	\State Let $m$ be the current index of the variable we are considering. Start with $m := 1$.
	\State \textbf{Loop:}
	\State The loop invariants that apply are:
	\begin{enumerate}[($\mathcal{I}$1)]
		\item $D_i(d_1(\vec{r}), \ldots, d_{i-1}(\vec{r}), \vec{r})$ is true for $i = 1, \ldots, n$
		\item if $D_i(s_1, s_2, \ldots, s_{i-1}, \vec{s})$ holds for $(s_1,\ldots, s_{i-1}, \vec{s}) \in [0,1]^{k+i}$, then 
		$d_i(s_1, \ldots, s_{i-1}, \vec{s}) \leq \mu x_i. t_i(s_1, s_2, \ldots, s_{i-1}, x_i, d_{i+1}(s_1, \ldots, s_{i-1}, x_i, \vec{s}), \ldots, \vec{s})$.
	\end{enumerate}
	\State By using the local algorithm, calculate 
	$t_m(x_1,x_2,\ldots, x_m, d_{m+1}(x_1, \ldots, x_m, \vec{y}), \ldots, \vec{y})$ at 
	$(d_1(\vec{r}), d_2(\vec{r}), \ldots, d_m(\vec{r}),\vec{r})$ as 
	$C(x_1, x_2, \ldots, x_m, \vec{y}) \vdash e(x_1, x_2, \ldots, x_m, \vec{y})$, where $e(x_1, x_2, \ldots, x_m, \vec{y})$ has the form $$e(x_1, x_2, \ldots, x_m, \vec{y}) = q_1 x_1 + q_2 x_2 + \ldots q_m x_m + \vec{q_y} \cdot \vec{y}$$ and 
	$q_1, q_2, \ldots q_m$ are rational numbers and $\vec{q_y}$ is a $k$-dimensional vector of rational numbers.
	\State Let 
	\begin{align*}
	E(x_1, x_2, \ldots, x_m, \vec{y}) := C(x_1, x_2, \ldots, x_m, \vec{y})  \cup
	D_{m+1}(x_1, x_2, \ldots, x_m,\vec{y}) \cup \\
	D_{m+2}(x_1, x_2, \ldots, x_m, d_{m+1}(x_1,\ldots, x_m,  \vec{y}), \vec{y}) \cup \\
	\vdots \\
	D_{n}(x_1, x_2, \ldots, x_m, d_{m+1}(x_1, x_2, \ldots, x_m, \vec{y}),\ldots, \vec{y}) 
	\end{align*}
	\State Arrange inequalities in $E(x_1, x_2, \ldots, x_m, \vec{y})$  to have the following structure:
	\begin{align}
	\begin{split}
	E'(x_1, \ldots, x_{m-1},\vec{y}) \cup 
	\{x_m > a_i(x_1,\ldots, x_{m-1}, \vec{y})\}_{1 \leq i \leq u'}\cup \\ 
	\{x_m \geq a_i(x_1,\ldots, x_{m-1}, \vec{y})\}_{u' \leq i \leq u} \cup 
	\{x_m < b_i(x_1,\ldots, x_{m-1}, \vec{y})\}_{1 \leq i \leq v'} \cup \\
	\{x_m \leq b_i(x_1,\ldots, x_{m-1}, \vec{y})\}_{v' \leq i \leq v}
	\end{split}
	\label{eq:rearranged}
	\end{align}
	\If{$q_m \neq 1$ }
	\State Define the linear expression
	$$ f(x_1, x_2, \ldots, x_{m-1}, \vec{y}) := \frac{1}{1-q_m}(q_1 x_1 + \ldots q_{m-1} x_{m-1} + \vec{q_y} \cdot \vec{y})$$
	\If{$E(d_1(\vec{r}),d_2(\vec{r}),\ldots, d_{m-1}(\vec{r}), f(d_1(\vec{r}),\ldots, d_{m-1}(\vec{r}), \vec{r}), \vec{r})$ holds
	}
	\State \begin{align*}
	D_m(x_1, \ldots, x_{m-1}, \vec{y}) :=  D_m(x_1, \ldots, x_{m-1}, \vec{y}) \cup 
	E'(x_1, \ldots, x_{m-1}, \vec{y}) \cup \\
	\{d_m(x_1, \ldots, x_{m-1}, \vec{y}) \leq f(x_1, \ldots, x_{m-1}, \vec{y}) \} \cup \\
	\{d_m(x_1, \ldots, x_{m-1}, \vec{y}) > a_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{1 \leq i \leq u'} \cup\\
	\{d_m(x_1, \ldots, x_{m-1}, \vec{y}) \geq a_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{u' \leq i \leq u}  \cup\\
	\{f(x_1, \ldots, x_{m-1}, \vec{y}) < b_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{1 \leq i \leq v'}  \cup\\
	\{f(x_1, \ldots, x_{m-1}, \vec{y}) \leq b_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{v' \leq i \leq v} 
	\end{align*}
	\State $$d_m(x_1, \ldots, x_{m-1}, \vec{y}) :=  f(x_1, \ldots, x_{m-1}, \vec{y})$$
	\If{$m = 1$}
	exit the loop \textbf{else}  go back to the loop with $m := m-1$ 
	\EndIf
	
	\Else
	\State Define $$ N(x_1, \ldots, x_{m-1}, \vec{y}) $$ to be the negation of the inequality 
	$$ e_1((x_1, \ldots, x_{m-1}, \vec{y}) \triangleleft e_2((x_1, \ldots, x_{m-1}, \vec{y}),$$
	where $\triangleleft$ is used for either $<$ or $\leq$ and $e_1((x_1, \ldots, x_{m-1}, \vec{y}) \triangleleft e_2((x_1, \ldots, x_{m-1}, \vec{y})$ is the chosen inequality in $E(x_1, \ldots, x_{m-1}, \vec{y})$, that does not hold at $$(d_1(\vec{r}),d_2(\vec{r}),\ldots, d_{m-1}(\vec{r}), f(d_1(\vec{r}),\ldots, d_{m-1}(\vec{r}), \vec{r}), \vec{r})$$
	and go to \textbf{find next approximation} below.
	\EndIf
	\Else (case, that $q_m = 1$)
	\If{$q_1 d_1(\vec{r}) + q_2 d_2(\vec{r}) + \ldots + q_{m-1} d_{m-1}(\vec{r}) + \vec{q_y} \cdot \vec{r} = 0$}
	\State \begin{align*}
	D_m(x_1, \ldots, x_{m-1}, \vec{y}) : = D_m(x_1, \ldots, x_{m-1}, \vec{y})  \cup \\
	E(x_1, \ldots, x_{m-1},d_m(x_1, \ldots, x_{m-1}, \vec{y}) \vec{y}) \cup\\
	\{q_1 x_1 + \ldots q_{m-1} x_{m-1} + \vec{q_y} \cdot \vec{y}  = 0\} 
	\end{align*}
	\State leave $d_m(x_1, \ldots, x_{m-1}, \vec{y})$ unchanged 
	\If{$m = 1$}
	exit the loop \textbf{else}  go back to the loop with $m := m-1$ 
	\EndIf
	
	\Else
	\State Define $N(x_1, \ldots, x_{m-1}, \vec{y})$ to be the one of the inequalities 
	\begin{align*}
	q_1 x_1 + \ldots q_{m-1} x_{m-1} + \vec{q_y} \cdot \vec{y}  < 0, q_1 x_1 + \ldots q_{m-1} x_{m-1} + \vec{q_y} \cdot \vec{y}  > 0,
	\end{align*}
	that holds at $(d_1(\vec{r}), d_2(\vec{r},\ldots d_{m-1}(\vec{r}), \vec{r})$ and proceed to \textbf{find next approximation} below.
	\EndIf
	\EndIf
	\State \textbf{end loop}
	\State \textbf{return} $D_1(\vec{y}) \vdash d_1(\vec{y})$, $D_2(\vec{y}) \vdash d_2(\vec{y})$, \ldots, $D_n(\vec{y}) \vdash d_n(\vec{y})$
	
	\State \textbf{Find next approximation:}
	\State Consider inequalities in $E(x_1, \ldots, x_{m-1}, \vec{y})$ arranged as in (\ref{eq:rearranged}) and choose a $j$ such that $b_j(d_1(\vec{r}), \ldots, d_{m-1}(\vec{r}), \vec{r}) \leq b_i(d_1(\vec{r}), \ldots, d_{m-1}(\vec{r}), \vec{r})$ for all $i$. We shall refer to $b_j(x_1, \ldots, x_{m-1}, \vec{y})$ as a \emph{supremum term}. If there is a candidate for a supremum term, which arises from strict inequality, choose $b_j(x_1, \ldots, x_{m-1}, \vec{y})$ to be one such.
	\State Let $D_m'(x_1, \ldots, x_{m-1}, \vec{y})$ and $d_m'(x_1, \ldots, x_{m-1}, \vec{y})$ be the current values of $D_m(x_1, \ldots, x_{m-1}, \vec{y})$ and $d_m(x_1, \ldots, x_{m-1}, \vec{y})$ (we will now update them).
	\If {the supremum term $b_j(x_1, \ldots, x_{m-1}, \vec{y})$ arises from a strict inequality of the form $x_m < b_j(x_1, \ldots, x_{m-1}, \vec{y})$ }
	\State Update current values to be
	\begin{align*}
	D_m(x_1, \ldots, x_{m-1}, \vec{y}) :=D_m(x_1, \ldots, x_{m-1}, \vec{y}) \cup 
	E'(x_1, \ldots, x_{m-1}, \vec{y}))\cup 
	\\ \{N(x_1, \ldots, x_{m-1}, \vec{y})\}\cup 
	\{d_m(x_1, \ldots, x_{m-1}, \vec{y})<b_j(x_1, \ldots, x_{m-1}, \vec{y})\}\cup\\ 
	\{b_j(x_1, \ldots, x_{m-1}, \vec{y})\leq b_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{1 \leq i \leq v} \cup \\
	\{d_m(x_1, \ldots, x_{m-1}, \vec{y}))> a_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{1 \leq i \leq u'}  \cup \\
	\{d_m(x_1, \ldots, x_{m-1}, \vec{y}) \geq a_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{u' \leq i \leq u} 
	\end{align*}
	\State $$ d_m(x_1, \ldots, x_{m-1}, \vec{y}) := e(x_1, \ldots, x_{m-1},b_j(x_1, \ldots, x_{m-1}, \vec{y}), \vec{y})$$
	\Else
	\State Update current values to be
	\begin{align*}
	D_m(x_1, \ldots, x_{m-1}, \vec{y}) :=D_m(x_1, \ldots, x_{m-1}, \vec{y}) \cup 
	E'(x_1, \ldots, x_{m-1}, \vec{y}))\cup \\ 
	\{N(x_1, \ldots, x_{m-1}, \vec{y})\}\cup 
	\{d_m(x_1, \ldots, x_{m-1}, \vec{y})\leq b_j(x_1, \ldots, x_{m-1}, \vec{y})\}\cup\\ 
	\{b_j(x_1, \ldots, x_{m-1}, \vec{y})\leq b_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{v' \leq i \leq v} \cup \\
	\{b_j(x_1, \ldots, x_{m-1}, \vec{y})< b_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{1 \leq i \leq v'} \cup\\
	\{d_m(x_1, \ldots, x_{m-1}, \vec{y}))> a_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{1 \leq i \leq u'}  \cup \\
	\{d_m(x_1, \ldots, x_{m-1}, \vec{y}) \geq a_i(x_1, \ldots, x_{m-1}, \vec{y})\}_{u' \leq i \leq u} 
	\end{align*}
	\State $$ d_m(x_1, \ldots, x_{m-1}, \vec{y}) := e(x_1, \ldots, x_{m-1},b_j(x_1, \ldots, x_{m-1}, \vec{y}), \vec{y})$$
	\EndIf
	\State Let $\ell$ be the maximum index, such that $D_{\ell}(d_1(\vec{r}), d_2(\vec{r}), \ldots, d_m(\vec{r}), d_{m+1}(\vec{r}), \ldots, d_{\ell-1}(\vec{r}), \vec{r})$ is false (start with $D_n(x_1, \ldots, x_{n-1}, \vec{y})$ and go up to $D_m(x_1, \ldots, x_{m-1}, \vec{y})$, where we stop).
	\If{$\ell = m$}
	\State go back to the loop, again  with index $m$
	\Else
	\State let 
	\begin{align*}	
	d_i(\vec{y}) = d_i(d_1(\vec{y}), 
	d_2(d_1(\vec{y}), \vec{y}), \ldots,
	d_{m-1}(d_1(\vec{y}), \ldots, \vec{y}), \\
	d_m'(d_1(\vec{y}), \ldots, \vec{y}), 
	d_{m+1}(d_1(\vec{y}),\ldots, 
	d_m'(d_1(\vec{y}), \ldots, \vec{y}), \vec{y}), \ldots, 
	\vec{y})
	\end{align*}
	where $i = 1, \ldots, n$
	\State Update initial values of $D_\ell(x_1, \ldots, x_{\ell-1}, \vec{y})$ and $d_l(x_1, \ldots, x_{\ell-1}, \vec{y})$ to be 
	\begin{align*}
	D_\ell(x_1, \ldots, x_{\ell-1}, \vec{y}) := \{x_i \geq d_i(d_1(\vec{y}),\ldots, d_{m-1}(\vec{y}), d_m'(\vec{y}), d_{m+1}(\vec{y}), \ldots, d_{i-1}(\vec{y}), \vec{y}) )\}_{i = 1,2, \ldots, \ell-1} \\
	\cup D_\ell(d_1(\vec{y}),\ldots, d_{m-1}(\vec{y}), d_m'(\vec{y}), d_{m+1}(\vec{y}), \ldots, d_{\ell-1}(\vec{y}), \vec{y})
	\end{align*}
	\State $$ d_\ell(x_1, \ldots, x_{\ell-1}, \vec{y}):= d_\ell(d_1(\vec{y}),\ldots, d_{m-1}(\vec{y}), d_m'(\vec{y}), d_{m+1}(\vec{y}), \ldots, d_{\ell-1}(\vec{y}), \vec{y}) $$
	
	\EndIf
	
	
\end{algorithmic}


\begin{thebibliography}{99}
	\bibitem{Mio-Simpson} Mio, Simpson: \emph{Lukasiewicz $\mu$-calculus}
	\bibitem{rudiments} \emph{Rudiments of $\mu$-calclus}
\end{thebibliography}
\end{document}