              
\documentclass[12pt]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{url}
\usepackage[reqno]{amsmath}    % basic ams math environments and symbols
\usepackage{amssymb,amsthm}    % ams symbols and theorems
\usepackage{mathtools}         % extends ams with arrows and stuff
\usepackage{url}               % \url and \href for links
\usepackage{graphicx}          % images
\usepackage{enumerate}

\usepackage{algpseudocode}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\title{Alternative iteration algorithm for calculating simultaneous fixed point}
\author{Anja Petković }
%\date{April 2018}

\begin{document}
	
	\maketitle
	
	Here we give an algorithm for finidng a simulteneous $n$-dimensional fixed point of a monotone piecewise linear function. 
	
	\section{Statement of the problem and prerequisites}
	We are working in the following setting: we are given a function $t \colon [0,1]^n \times [0,1]^k \to [0,1]^n$, which is monotone (as a map between lattices) and piecewise linear. We use notation $(\vec{x}, \vec{y})$ for elements of $[0,1]^n \times [0,1]^k$ and we think of the vector $\vec{y}$ as the vector of parameters. We name the components of $\vec{x}$ as $(x_1, x_2, \ldots, x_n)$. The function is given by a local algorithm, which given an input vector $(\vec{s}, \vec{r}) \in$ yields $n$ conditioned linear expressions 
	\begin{align*}
		C_1(x_1, x_2, \ldots, x_n, \vec{y}) & \vdash  e_1(x_1, x_2, \ldots, x_n, \vec{y}) \\
		C_2(x_1, x_2, \ldots, x_n, \vec{y}) & \vdash  e_2(x_1, x_2, \ldots, x_n, \vec{y}) \\
		\vdots & \\
		C_n(x_1, x_2, \ldots, x_n, \vec{y}) & \vdash  e_n(x_1, x_2, \ldots, x_n, \vec{y}), \\
	\end{align*}
	such that for all $i \in \{1,2,\ldots.n\}$,  $C_i(\vec{s},\vec{r})$ holds and 
	$$t(\vec{s},\vec{r}) = \left ( e_1(\vec{s},\vec{r}), e_2(\vec{s},\vec{r}), \ldots, e_n(\vec{s},\vec{r})\right).$$
	We are calculating the simultaneous least fixed point of the first $n$ components, meaning we are looking for
	\begin{equation}
		\mu 
		\begin{bmatrix}
		x_1 \\
		x_2 \\
		\vdots \\
		x_n
		\end{bmatrix}. t(x_1, x_2, \ldots, x_n, \vec{y}).
	\end{equation}
	
	We use the prodecure of finding a fixed point in one variable for a monotone piecewise linear function $t' \colon [0,1]\times [0,1]^{k} \to [0,1]$, described in the article \cite{Mio-Simpson} and call it \emph{singleFP} (as in single fixed point). The procedure is used as one iteration of the loop, and we get either the fixed point and the conditioned linear expression at the input vector of parameters $\vec{r} \in [0,1]^k$, or the next approximation for it. For the purposes of this generalisaton we need the following properties of the procedure (that are described and proven in the article):
	\begin{enumerate}[(R1)]
		\item We input a vector $\vec{r} \in [0,1]^k$ (values for parameters), a set of linear inequalities $D_0'(\vec{y})$ and an approximation to the fixed point $d_0'(\vec{y})$ with the following two properties:
		\begin{enumerate}[($\mathcal{I}$1')]
			\item $D_0'(\vec{r})$ is true
			\item for all $\vec{s} \in [0,1]^k$, if $D_0'(\vec{s})$ then $d_0'(\vec{s}) \leq (\mu x. t'(x, \vec{y}))(\vec{s})$.
		\end{enumerate}
		\item The procedure either outputs the least fixed point in the form of a conditioned linear expression $C(\vec{y}) \vdash e( \vec{y})$, meaning 
		\begin{enumerate}[(P1')]
			\item $C(\vec{r})$ is true
			\item for all $\vec{s} \in [0,1]^k$, if $C(\vec{s})$ then $\vec{s} \in [0,1]^k$ and $e(\vec{s}) = (\mu x. t'(x, \vec{y}))(\vec{s})$,
		\end{enumerate}
		or it gives the next approximations $D_1'(\vec{y})$ and $d_1'(\vec{y})$, that satisfy properties $(\mathcal{I}1)$ and $(\mathcal{I}2)$. Moreover $D_0'(\vec{y})$ is a subset of $D_1'(\vec{y})$ and $C(\vec{y})$.
		\item Let $C(x,\vec{y}) \vdash e(x, \vec{y})$ be the conditioned linear expression given by the local algorithm for $t'$ at $(d_0'(\vec{r}), \vec{r})$ (this is the first step of the procedure \emph{singleFP}). If the procedure gives the next approximation, it holds that $d_1'(\vec{s}) > d_0'(\vec{s})$ for $\vec{s}$, that satisfies $D_1'(\vec{s})$ and $C(d_1'(\vec{r}), \vec{r})$ is false. 
	\end{enumerate}

	\section{Generalised algorithm for the simultaneous fixed point}

We have an input $\vec{r} \in [0,1]^k$ and the monotone piecewise linear function $t \colon [0,1]^n \times [0,1]^k \to [0,1]^n$ with components $t_1(\vec{x},\vec{y})$, $t_2(\vec{x},\vec{y})$, \ldots, $t_n(\vec{x},\vec{y})
$.
\begin{algorithmic}
	
	\State $\vec{D}^0(\vec{y}) := 
	\begin{bmatrix}
	\emptyset \\
	\emptyset \\
	\vdots\\
	\emptyset
	\end{bmatrix}$ ($n$-dimensional empty set)  
	\State $\vec{d^0}(\vec{y}) : = 
	\begin{bmatrix}
	0 \\
	0 \\
	\vdots\\
	0
	\end{bmatrix}$ ($n$-dimensional) \Comment notation for components: $d^0_1(\vec{y}), d^0_2(\vec{y}), \ldots$
	\State $\vec{D}^1(\vec{y}) := \vec{D}^0(\vec{y}) $ \Comment  make vectors for the next iteration
	\State $\vec{d}^1(\vec{y}) := \vec{d}^0(\vec{y}) $
	\While{True} \Comment main loop
		\For {$i = 1, 2, \ldots, n$ }
			\State Perform \emph{singleFP} with 
			\begin{align*}
				\vec{r} &= \vec{r} \\
				t'(x, \vec{y}) &= t_i(d^0_1(\vec{y}), d^0_2(\vec{y}), \ldots, d^0_{i-1}(\vec{y}), x, d^0_{i+1}(\vec{y}), \ldots, d^0_n(\vec{y}), \vec{y} ),\\
				D_0'(\vec{y}) &= D_i^0(\vec{y}) \\
				d_0'(\vec{y}) &= d^0_i(\vec{y}).				
			\end{align*}
			\State and save the result (next approximation or the fixed point) in $\vec{D}^1_i(\vec{y}) $ 
			\State and $\vec{d}^1_i(\vec{y})$.
		\EndFor
	\If{for all $i = 1, 2, \ldots, n$ the values $\vec{D}^1_i(\vec{y}) $ and $\vec{d}^1_i(\vec{y})$ were obtained from the procedure \emph{singleFP} yileding a result}
		\If {$\vec{D}^1(\vec{y}) = \vec{D}^0(\vec{y})$ and $\vec{d}^1(\vec{y}) := \vec{d}^0(\vec{y}) $}
			\Comment compare componentwise and by coefficients 
			\State  \Return $\vec{D}^1(\vec{y}) \vdash \vec{d}^1(\vec{y})$  \Comment exit the loop here
		\EndIf
	\EndIf
	\State $\vec{D}^0(\vec{y}) := \vec{D}^1(\vec{y})$
	\EndWhile
\end{algorithmic}

\section{Correctness proof}

\begin{theorem}
\label{thm:correctness}	
	For every input vector $\vec{r} \in [0,1]^k$, if the algorithm terminates with a conditioned linear expression $C(\vec{y}) \vdash e(\vec{y})$, the following two properties hold:
	\begin{enumerate}[(P1)]
		\item $C(\vec{r})$ is true,
		\item for all $\vec{s} \in \mathbb{R}^k$, if $C(\vec{s})$ then $\vec{s} \in [0,1]^k$ and 
		$$e(\vec{s}) = \mu 
		\begin{bmatrix}
		x_1 \\
		x_2 \\
		\vdots \\
		x_n
		\end{bmatrix}. t(x_1, x_2, \ldots, x_n, \vec{s}).$$
	\end{enumerate} 
\end{theorem}

In order to prove this correctnes theorem, we observe the following two loop invariants: 

\begin{lemma}
	\label{lemma: generalised_invariants}
	The above algorithm has the following loop invariants:
	\begin{enumerate}[($\mathcal{I}$1)]
		\item $\vec{D}^1(\vec{r})$ is true, meaning $D^1_1(\vec{r}) \cup D^1_2(\vec{r}) \cup \dots \cup D^1_n(\vec{r})$ is true,
		\item for all $\vec{s} \in [0,1]^k$, if $\vec{D}^1(\vec{s})$ then $\vec{d}^1(\vec{s}) \leq \mu \vec{x}. t(\vec{x}, \vec{s})$.
	\end{enumerate}
\end{lemma}


\begin{proof}[proof of lemma \ref{lemma: generalised_invariants}]
	First, we show, that the invariants $(\mathcal{I}1)$ and $(\mathcal{I}2)$ hold at the start of the loop. Since $\vec{D}^1(\vec{y})$ is empty at the beginning, $(\mathcal{I}1)$ trivially holds. For all $\vec{s} \in [0,1]^k$ we have $\vec{D}^1(\vec{s})$, but $\vec{d}^1(\vec{s})$ is the zero vector, which is less or equal to any other vector, specifically it is less or equal to $\mu \vec{x}. t(\vec{x}, \vec{s})$. Therefore the invariants hold before we enter the loop. 
	
	
	We proceed by induction and we look at some iteration $j\geq 1$. By induction hypothesis invariants $(\mathcal{I}1)$ and $(\mathcal{I}2)$ hold for $\vec{D}^0(\vec{y})$ and $\vec{d}^0(\vec{y})$ (from previous iteration $j-1$).
	
	Because for every $i = 1, 2, \ldots, n$ by $(\mathcal{I}1')$ for procedure \emph{singleFP} we have, that since $D^0_i(\vec{r})$ holds, so does $D^1_i(\vec{r})$, we can deduce, that $\vec{D}^1(\vec{r})$ holds and we have invariant $(\mathcal{I}1)$.

	To prove $(\mathcal{I}2)$ let $\vec{s}\in [0,1]^k$ satisfy $\vec{D}^1(\vec{s})$ and hence by the property $(R3)$ of the procedure \emph{singleFP} (we use it componentwise), $\vec{s}$ also satisfies $\vec{D}^0(\vec{s})$.  Let $\vec{b}(\vec{y})$ be the least fixed point $\mu \vec{x}. t(\vec{x}, \vec{y})$. Let $i \in \{1, 2, \ldots, n\}$. 
	Now by $(\mathcal{I}2')$ invariant of \emph{singleFP}, we know
	$$
	d^1_i(\vec{s}) \leq \mu x_i. t_i(d^0_1(\vec{s}), d^0_2(\vec{s}), \ldots, d^0_{i-1}(\vec{s}), x_i, d^0_{i+1}(\vec{s}),\ldots, d^0_n(\vec{s}), \vec{s} ).
	$$
	By the invariant $(\mathcal{I}2)$ for $\vec{d}^0(\vec{y})$, we have $\vec{d}^0(\vec{s}) \leq \vec{b}(\vec{s})$ and because $t$ is monotone, we have 
	\begin{equation}
		d^1_i(\vec{s}) \leq \mu x_i. t_i(b_1(\vec{s}), b_2(\vec{s}), \ldots, b_{i-1}(\vec{s}), x_i, b_{i+1}(\vec{s}),\ldots, b_n(\vec{s}), \vec{s} ) = b_i(\vec{s}). 
		\label{bekic}
	\end{equation}
	The last equality holds by the generalised Bekić principle \cite[proposition 1.4.7]{rudiments}. Because equation (\ref{bekic}) holds for every $i \in \{1, 2, \ldots, n\}$, we have 
	$$
	\vec{d}^1(\vec{s}) \leq \vec{b}(\vec{s}),
	$$
	which proves $(\mathcal{I}2)$.
\end{proof}

\begin{proof}[proof of theorem \ref{thm:correctness}]
	To prove $(P1)$ we see, that by $(\mathcal{I}1)$ at every iteration $\vec{D}^1(\vec{r})$ holds. Before we return the result, we only check two conditions, which do not change $\vec{D}^1(\vec{y})$, which we then return. Therefore $(P1)$ holds. 
	
	To prove $(P2)$, we know by $(\mathcal{I}2)$, that when we return the result it holds that for all $\vec{s} \in [0,1]^k$, if $\vec{D}^1(\vec{s})$ then $\vec{d}^1(\vec{s}) \leq \mu \vec{x}. t(\vec{x}, \vec{s})$. The first condition under which we return a result is, that for all $i = 1, 2, \ldots, n$,  $D^1_i(\vec{y}) \vdash d^1_i(\vec{y})$ is a result of the prodecure \emph{singleFP} and therefore by $(P2')$ we know, that if $D^1_i(\vec{s})$ holds, $s \in [0,1]^k$ and for $\vec{y} = \vec{s}$, $d^1_i(\vec{y})$ is the least fixed point 
	$$\mu x. t_i(d^0_1(\vec{y}), d^0_2(\vec{y}), \ldots, d^0_{i-1}(\vec{y}), x, d^0_{i+1}(\vec{y}),\ldots, d^0_n(\vec{y}), \vec{y} ),$$ therefore 
	$$
	d^1_i(\vec{y}) = t_i(d^0_1(\vec{y}), d^0_2(\vec{y}), \ldots, d^0_{i-1}(\vec{y}), d^1_i(\vec{y}), d^0_{i+1}(\vec{y}),\ldots, d^0_n(\vec{y}), \vec{y} ).
	$$
	Because of the second condition under which the result is returned, we know, that $d^1_i(\vec{y}) = d^0_i(\vec{y})$ and we have 
	$$
	d^1_i(\vec{y}) = t_i(d^1_1(\vec{y}), d^1_2(\vec{y}), \ldots, d^1_{i-1}(\vec{y}), d^1_i(\vec{y}), d^1_{i+1}(\vec{y})\ldots, d^1_n(\vec{y}), \vec{y} ).
	$$
	Since this equation holds for every $i = 1, 2, \ldots, n$, we get that for $\vec{s} \in \mathbb{R}^k$ if $\vec{D}^1(\vec{s})$ holds, then $\vec{s} \in [0,1]^k$ and $\vec{d}^1(\vec{s})$ really is a fixed point of $t(\vec{x},\vec{s})$. Because $\vec{d}^1(\vec{s}) \leq \mu \vec{x}. t(\vec{x}, \vec{s})$, it is the least fixed point, proving $(P2)$.
\end{proof}



\section{Termination proof}

\begin{theorem}
	For every input vector $\vec{r} \in [0,1]^k$, the above algorithm terminates with a conditioned linear expression $C_{\vec{r}}(\vec{y}) \vdash e_{\vec{r}}(\vec{y})$. Moreover, the set of all possible resulting conditioned linear expressions 
	$$\{ C_{\vec{r}} \vdash e_{\vec{r}} \mid \vec{r} \in [0,1]^k \} $$
	 is finite and provides a representing system for the function 
	 $$t \colon [0,1]^n \times [0,1]^k \to [0,1]^n.$$
\end{theorem}

HERE WE ARE STUCK 

\begin{proof}
	Because $t(\vec{x},\vec{y})$ is given by a local algorothm for a piecewise linear function, there are only finitiely many conditioned linear expressions 
	\begin{align*}
		\begin{bmatrix}
		C^m_0(\vec{x},\vec{y}) \\
		C^m_1(\vec{x},\vec{y})  \\
		\vdots \\
		C^m_n(\vec{x},\vec{y}) \\
		\end{bmatrix}
		\vdash 
		\begin{bmatrix}
		e^m_0(\vec{x},\vec{y})  \\
		e^m_1(\vec{x},\vec{y})  \\
		\vdots \\
		e^m_n(\vec{x},\vec{y})  \\
		\end{bmatrix}
	\end{align*}
	for $m \in \{1, 2, \ldots,\ell\}$, that the local algorithm can produce. 
	Let us look at iteration $j$, at the end of which we do not stop. Let $(\vec{d}^0(\vec{y})$ and $(\vec{d}^1(\vec{y})$ be the approximations at the end of the for loop at iteration $i$ of the (outer) while loop.
	Let 
	\begin{align*}
	\begin{bmatrix}
	C^j_0(\vec{x},\vec{y}) \\
	C^j_1(\vec{x},\vec{y})  \\
	\vdots \\
	C^j_n(\vec{x},\vec{y}) \\
	\end{bmatrix}
	\vdash 
	\begin{bmatrix}
	e^j_0(\vec{x},\vec{y})  \\
	e^j_1(\vec{x},\vec{y})  \\
	\vdots \\
	e^j_n(\vec{x},\vec{y})  \\
	\end{bmatrix}
	\end{align*}
	or in short $\vec{C}^j(\vec{x},\vec{y}) \vdash \vec{e}^j(\vec{x},\vec{y})$ be the conditioned linear expression, that the local algorithm for $t$ yields at $(\vec{d}^0(\vec{r}), \vec{r})$  and 
	\begin{align*}
	\begin{bmatrix}
	C^{j+1}_0(\vec{x},\vec{y}) \\
	C^{j+1}_1(\vec{x},\vec{y})  \\
	\vdots \\
	C^{j+1}_n(\vec{x},\vec{y}) \\
	\end{bmatrix}
	\vdash 
	\begin{bmatrix}
	e^{j+1}_0(\vec{x},\vec{y})  \\
	e^{j+1}_1(\vec{x},\vec{y})  \\
	\vdots \\
	e^{j+1}_n(\vec{x},\vec{y})  \\
	\end{bmatrix}
	\end{align*}
	or in short $\vec{C}^{j+1}(\vec{x},\vec{y}) \vdash \vec{e}^{j+1}(\vec{x},\vec{y})$ be the conditioned linear expression, that the local algorithm for $t$ yields at $(\vec{d}^1(\vec{r}), \vec{r})$
	
	At the end of the iteration, we have exactly one of the following three situations
	\begin{enumerate}[(N1)]
		\item for at least one $i \in \{1, 2, \ldots, n\}$, the prodecure \emph{singleFP} did not return a resulting fixed point, but rather gave a new approximation,
		\item for all $i \in \{1, 2, \ldots, n\}$, the prodecure \emph{singleFP} gave a result,  $\vec{C}^{j+1}(\vec{x},\vec{y}) \vdash \vec{e}^{j+1}(\vec{x},\vec{y})$ is not equal to $\vec{C}^{j}(\vec{x},\vec{y}) \vdash \vec{e}^{j}(\vec{x},\vec{y})$ and $\vec{d}^0(\vec{y}) \neq \vec{d}^1(\vec{y})$.
		\item for all $i \in \{1, 2, \ldots, n\}$, the prodecure \emph{singleFP} gave a result,  $\vec{C}^{j+1}(\vec{x},\vec{y}) \vdash \vec{e}^{j+1}(\vec{x},\vec{y})$ is equal to $\vec{C}^{j}(\vec{x},\vec{y}) \vdash \vec{e}^{j}(\vec{x},\vec{y})$, but $\vec{d}^0(\vec{y}) \neq \vec{d}^1(\vec{y})$.
	\end{enumerate}
\end{proof}




\begin{thebibliography}{99}
	\bibitem{Mio-Simpson} Mio, Simpson: \emph{Lukasiewicz $\mu$-calculus}
	\bibitem{rudiments} \emph{Rudiments of $\mu$-calclus}
\end{thebibliography}

\end{document}