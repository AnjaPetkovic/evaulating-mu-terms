\section{Correctness proof for the recursive form}

\begin{theorem}
    \label{thm:correctness-recursive}
    Let  $\vec{r} \in [0,1]^k$ be a vector of rational numbers and  $\vec{t} \colon [0,1]^k \times [0,1]^n \to [0,1]^n$ a piecewise linear function with components $t_1(\vec{y}, \vec{x})$, $t_2(\vec{y}, \vec{x})$, \ldots ,  $t_n(\vec{y}, \vec{x})$, that is monotone in variables $x_1, \ldots, x_n$ that are given by local algorithms and an $n$-dimensional vector of linear expressions $d_1(\vec{y})$, $d_2(\vec{y})$, \ldots,  $d_n(\vec{y})$. Let $D(\vec{y})$ be  set of linear constraints, such that
\begin{enumerate}[(\text{A}1)]
    \item $D(\vec{r})$ holds,
    \item if $D(\vec{p})$ holds then $\vec{p}\in [0,1]^k$ and for $i = 1, \ldots, n$ it holds that
    \begin{equation*}
        d_i(\vec{p}) \leq \proj{i}{\fixpoint{\xvec{i}{n}}{\slicevec{t}{i}{n}(\vec{p},\slicevec{d}{1}{i-1}(\vec{p}),\xvec{i}{n})}}.
    \end{equation*}
\end{enumerate}
Then the algorithm returns an $n$-dimensional vector $d_{1}'(\vec{y})$, $d_{2}'(\vec{y})$, \ldots, $d_n'(\vec{y})$ and a set of constraints $D'(\vec{y})$, such that
\begin{enumerate}[(P1)]
	\item $D'(\vec{r})$ holds,
	\item for all $\vec{p} \in \RR^{k}$, if $D'(\vec{p})$ holds then $D(\vec{p})$ holds and
	\begin{equation*}
		\vec{d'}(\vec{p})= \fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}
	\end{equation*}
\end{enumerate}
Moreover given $D(\vec{y})$ and $d_1(\vec{y})$, $d_2(\vec{y})$, \ldots,  $d_n(\vec{y})$ such that (A1) holds, the set of all possible resulting vectors of conditioned linear expressions
\begin{equation*}
    \{(D'(\vec{y}) \vdash d'_1(\vec{y}), \ldots, D'(\vec{y}) \vdash d'_n(\vec{y})) \mid \vec{r} \in [0,1]^k, D(\vec{r})\}
\end{equation*}
is finite.
\end{theorem}

\begin{proof}
    We prove correctness by induction on $n$.
    If $n = 0$, it's trivial.

    Suppose that $n > 0$. We observe that the loop invariants $\invariant{1}$ and $\invariant{2}$ hold at the start of the algorithm, since they are identical to (A1) and (A2).

    At the start of the loop, the algorithm makes a recursive call with vector $(\vec{r}, d_1(\vec{r}))$, monotone piecewise linear function $\slicevec{t}{2}{n}(\vec{y},\vec{x})$ approximations $\slicevec{d}{2}{n}(\vec{y})$ constraint set $D(\vec{y}) \cup \{d_1(\vec{y}) \leq x_1 \leq 1\}$. This recursive call is legitimate. Indeed (A1) is trivial because $D(\vec{r})$ holds and $d_1(\vec{r}) \leq d_1(\vec{r}) \leq 1$ where the latter inequality holds by $\invariant{2}$.
    To show (A2), suppose $D(\vec{p}) \cup \{d_1(\vec{p}) \leq p_{k+1} \leq 1\}$ holds. Indeed for $i = 2, \ldots, n$ we have by $\invariant{2}$ and monotonicity of $\mu$:
    \begin{align*}
         d_i(\vec{p})
        \leq \proj{i}{\fixpoint{\xvec{i}{n}}{\slicevec{t}{i}{n}(\vec{p},d_1(\vec{p}),\slicevec{d}{2}{i-1}(\vec{p}),\xvec{i}{n})}}
        \leq \proj{i}{\fixpoint{\xvec{i}{n}}{\slicevec{t}{i}{n}(\vec{p},p_{k+1},\slicevec{d}{2}{i-1}(\vec{p}),\xvec{i}{n})}}.
    \end{align*}
    (Note that $\slicevec{d}{2}{n}(\vec{y})$ depend only on the variables $\vec{y}$.)

    The recursive call returns $D^{+}(\vec{y}, x_1)$ and expressions $d_2^{+}(\vec{y}, x_1), \ldots, d_n^{+}(\vec{y}, x_1)$ which by induction hypothesis satisfy (P1) and (P2), specifically:
    \begin{equation}
        \label{eq:prop-D'}
        D^{+}(\vec{r}, d_1(\vec{r})) \text{ holds}
    \end{equation}
    and for all $(\vec{p}, p_{k+1} ) \in \RR^{k+1}$, if $D^{+}(\vec{p}, p_{k+1})$ holds then $D(\vec{p}) \cup \{d_1(\vec{p}) \leq p_{k+1} \leq 1\}$ holds and
    \begin{equation}
        \label{eq:prev-d-eq-fix}
		\slicevec{d^{+}}{2}{n}(\vec{p}, p_{k+1})= \fixpoint{\xvec{2}{n}}{{\slicevec{t}{2}{n}(\vec{p}, p_{k+1},\xvec{2}{n})}}.
    \end{equation}
    %
    The call to the local algorithm gives us
    $C'(\vec{y}, \xvec{1}{n}) \vdash e'(\vec{y}, \xvec{1}{n})$
    %
    and we define $e(\vec{y}, x_1)$ and $E(\vec{y},x_1)$ as in~\eqref{def:qs}, \eqref{eq:def_E} and \eqref{eq:rearranged}.

    We observe
    \begin{equation}
        \label{eq:prop-E-r-d1r}
        E(\vec{r}, d_1(\vec{r}))
    \end{equation}
    holds from~\eqref{eq:prop-D'} and the fact that $C'(\vec{y},\xvec{1}{n})$ are the constraints for the linear piece $e'(\vec{y}, \vec{x})$ of $t_1(\vec{y}, \vec{x})$ at $(\vec{r}, d_1(\vec{r}), \slicevec{d^{+}}{2}{n}(\vec{r}, d_1(\vec{r})))$.

    We first consider the case when $q_1 \neq 1$, see~\eqref{def:qs}. In this case define $f(\vec{y})$ as in~\eqref{eq:def_f}.

    In the case that $E(\vec{r}, f(\vec{r}))$ holds we need to prove properties (P1) and (P2) for $D'(\vec{y})$ defined by~\eqref{eq:update_qm_neq_1} and $\vec{d'}(\vec{y}) = \big(f(\vec{y})$, $d_2^{+}(\vec{y}, f(\vec{y}))$ \ldots, $d_n^{+}(\vec{y}, f(\vec{y}))\big )$.
    To prove (P1) we must show that $D'(\vec{r})$, so we consider the following sets of constraints:
    \begin{itemize}
        \item $D(\vec{r})$ holds by $\invariant{1}$.
        \item $E'(\vec{r})$, $\{f(\vec{r}) < b_i(\vec{r})\}_{1 \leq i \leq v'}$ and
        $\{f(\vec{r}) \leq b_i(\vec{r})\}_{v' \leq i \leq v}$ all hold since $E(\vec{r}, f(\vec{r}))$ holds, see~\eqref{eq:def_E}.
        \item $\{d_1(\vec{r}) > a_i(\vec{r})\}_{1 \leq i \leq u'}$ and
        $\{d_1(\vec{r}) \geq a_i(\vec{r})\}_{u' \leq i \leq u}$ hold by~\eqref{eq:prop-E-r-d1r}.
        \item To show that $d_1(\vec{r}) \leq f(\vec{r})$ holds, we only need to show, that
        \begin{equation*}
            f(\vec{r}) = t_1(\vec{r}, f(\vec{r}),\fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{r},f(\vec{r}),\xvec{2}{n})}).
        \end{equation*}
        \todo{This equality follows as below, improve exposition.}
    \end{itemize}

    To prove (P2) suppose $\vec{p} \in \RR^k$ and $D'(\vec{p})$ holds. $D(\vec{p})$ trivially holds since $D(\vec{y}) \subseteq D'(\vec{y})$.
    We need to show
    \begin{equation*}
		\vec{d'}(\vec{p})= \fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}
    \end{equation*}

    We first show that, since $D'(\vec{p})$ holds, then so do $E(\vec{p}, d_1(\vec{p}))$ and $E(\vec{p}, f(\vec{p}))$.
    Trivially $E'(\vec{p})$ holds. \todo{the rest is straightforward as well.}

    Since $E(\vec{p}, f(\vec{p}))$ we have $D^{+}(\vec{p}, f(\vec{p}))$ and we can instantiate~\eqref{eq:prev-d-eq-fix} with $f(\vec{p})$ to get
    \begin{equation*}
        \slicevec{d^{+}}{2}{n}(\vec{p}, f(\vec{p}))= \fixpoint{\xvec{2}{n}}{{\slicevec{t}{2}{n}(\vec{p}, f(\vec{p}),\xvec{2}{n})}}
    \end{equation*}
    We show below that
    \begin{equation}
        \label{eq:f-is-fixpoint}
        f(\vec{p}) = \fixpoint{x_1}{t_1(\vec{p},x_1,\slicevec{t}{2}{n}(\vec{p}, x_1, \fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{p},\vec{x})}))}
    \end{equation}
    Using this we have
    \begin{align*}
        d'_1(\vec{p}) =\\
        f(\vec{p}) =\\
        \fixpoint{x_1}{t_1(\vec{p}, x_1,\slicevec{t}{2}{n}(\vec{p}, x_1, \fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{p},\vec{x})}))} =\\
        \fixpoint{x_1}{t_1(\vec{p},
                             x_1,
                             \fixpoint{\xvec{2}{n}}{{\slicevec{t}{2}{n}(\vec{p}, x_1,\xvec{2}{n})}})} =\\
        \proj{1}{\fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}}
    \end{align*}
    \todo{Easy reasoning about Bekić principle.}
    And for $i = 2, \ldots, n$ we have
    \begin{align*}
        d'_i(\vec{p}) =\\
        d^{+}_i(\vec{p}, f(p)) =\\
        \proj{i}{\fixpoint{\xvec{2}{n}}{{\slicevec{t}{2}{n}(\vec{p}, f(\vec{p}),\xvec{2}{n})}}} =\\
        \proj{i}{\fixpoint{\xvec{2}{n}}{{\slicevec{t}{2}{n}(\vec{p}, \proj{1}{\fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}},\xvec{2}{n})}}} =\\
        \proj{i}{\fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}}
    \end{align*}

    To show~\eqref{eq:f-is-fixpoint} we want to show, that
    $f(\vec{p})$ is the least fixed point of
    \begin{equation*}
        p_{k+1} \mapsto t_1(\vec{p}, p_{k+1},\fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{p},p_{k+1},\xvec{2}{n})}).
    \end{equation*}
    Since both $E(\vec{p},d_1(\vec{p}))$ and $E(\vec{p},f(\vec{p}))$ hold and the constraints in $E$ are convex, $E(\vec{p}, p_{k+1})$ holds for $p_{k+1} \in [d_1(\vec{p}), f(\vec{p})]$. Specifically $D^+(\vec{p},p_{k+1})$ holds, so this function is equal to
    \begin{equation*}
        p_{k+1} \mapsto t_1(\vec{p}, p_{k+1},\slicevec{d^{+}}{2}{n}(\vec{p}, p_{k+1})),
    \end{equation*}
    and since $C'(\vec{p}, p_{k+1}, \slicevec{d^{+}}{2}{n}(\vec{p}, p_{k+1}))$ also holds, this function is equal to
    \begin{equation*}
        p_{k+1} \mapsto e(\vec{p}, p_{k+1}).
    \end{equation*}
    Since $f(\vec{p})$ is the unique fixed point of $p_{k+1} \mapsto e(\vec{p}, p_{k+1})$, it is a fixed point of $p_{k+1} \mapsto t_1(\vec{p}, p_{k+1},\slicevec{d^{+}}{2}{n}(\vec{p}, p_{k+1}))$ and because
    $d_1(\vec{p}) \leq \proj{1}{\fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}}$
    we know that $f(\vec{p})$ is the least fixed point.

    In the case that $E(\vec{r}, f(\vec{r}))$ we proceed to \textbf{find next approximation} with $N(\vec{y})$. We need to prove $\invariant{1}$ and $\invariant{2}$.
    In the case that the supremum term $b_j(\vec{y})$ arises from a strict inequality the new $D(\vec{r})$ defined by~\eqref{eq:fnaDm1} holds because:
    \begin{itemize}
        \item the old $D(\vec{r})$ holds at the start of the iteration,
        \item $E'(\vec{r})$ holds because $E(\vec{r},d_1(\vec{r}))$ holds,
        \item $N(\vec{r})$ holds by definition of $N(\vec{y})$,
        \item $b_j(\vec{r}) \leq b_i(\vec{r})$ by definition of the supremum term,
        \item $d_1(\vec{r}) < b_j(\vec{r})$ because $E(\vec{r}, d_1(\vec{r}))$ holds and $b_j(\vec{y})$ comes from a strict inequality.
        \item $d_1(\vec{r}) > a_i(\vec{r})$ and $d_1(\vec{r}) \geq a_i(\vec{r})$ hold because $E(\vec{r}, d_1(\vec{r}))$.
     \end{itemize}
     Similarly in the case that the supremum term $b_j(\vec{y})$ arises from a non-strict inequality.

     We now prove $\invariant{2}$. Suppose $D(\vec{p})$ holds.
     \todo{Trivially $\vec{p} \in [0,1]^k$}.
    %  Suppose $i > 1$.
    %  We need to show
    %  \begin{equation*}
    %      d^+_i(\vec{p},d_1^{old}(\vec{p})) \leq \proj{i}{\fixpoint{\xvec{i}{n}}{\slicevec{t}{i}{n}(\vec{p}, \slicevec{d}{1}{i-1}(\vec{p}), \xvec{i}{n})}}
    %  \end{equation*}
    For $i = 1$, we want to show
     \begin{equation*}
         e(\vec{p},b_j(\vec{p})) \leq \proj{1}{\fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}}
     \end{equation*}
     %
     Let $u = \proj{1}{\fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}}$. By the invariant $\invariant{2}$ we know that $d_1(\vec{p}) \leq u$ holds and by the choice of $N(\vec{y})$, we know that $N(\vec{p})$ implies, that $E(\vec{p}, u)$ does not hold.
     If we show, that $E(\vec{p}, d_1(\vec{p}))$ holds then by the choice of supremum term for $p_{k+1} \in \{d_1(p)\}\cup  (d_1(\vec{p}),b_j(\vec{p})) \cap [0,1]$ also $E(\vec{p}, p_{k+1})$ holds. To be sure of this implication let us take a look at one type of inequalities in $E$, say $x_1 \geq a_{\ell}(\vec{y})$. Then we have $p_{k+1} \geq d_1(\vec{p}) \geq a_{\ell}(\vec{p})$, so indeed $p_{k+1} \geq a_{\ell}(\vec{p})$. A similar argument proves the implication for the other types of inequalities.

     \todo{We now show that $E(\vec{p}, d_1(\vec{p}))$ holds (TO DO).}

     Since $E(\vec{p}, u)$ is false and the fact that $d_1(\vec{p}) \leq u$ we have $p_{k+1} < u$ for all $p_{k+1} \in [0,1]$ with $p_{k+1} = d_1(\vec{p})$ or $d_1(\vec{p}) < p_{k+1} < b_j(\vec{p})$. Since $u$ is the least fixed point of $x_1 \mapsto t_1(\vec{p},x_1,\slicevec{t}{2}{n}(\vec{p}, x_1, \fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{p},\vec{x})}))$, it is the least such $x_1$ that
     \begin{equation*}
        t_1(\vec{p},x_1,\slicevec{t}{2}{n}(\vec{p}, x_1, \fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{p},\vec{x})})) \leq x_1
     \end{equation*}
     and because $t_1$ is a monotone function we have
     \begin{align*}
         p_{k+1} &< t_1(\vec{p},p_{k+1},\slicevec{t}{2}{n}(\vec{p}, p_{k+1}, \fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{p}, p_{k+1}, \xvec{2}{n})})) \\
         &\leq t_1(\vec{p},u,\slicevec{t}{2}{n}(\vec{p}, u, \fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{p},u, \xvec{2}{n})}))\\
        &= u
     \end{align*}
     Since $E(\vec{p},p_{k+1})$ implies $D^+(\vec{p},p_{k+1})$, we have by~\eqref{eq:prev-d-eq-fix} that
     \begin{equation*}
        t_1(\vec{p},p_{k+1},\slicevec{t}{2}{n}(\vec{p}, p_{k+1}, \fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{p}, p_{k+1}, \xvec{2}{n})}))
        =
        e(\vec{p},p_{k+1})
     \end{equation*}
     so we can conclude
     \begin{equation}
        \label{eq:fixpoint-sandwitch}
        p_{k+1} < e(\vec{p}, p_{k+1}) \leq u
     \end{equation}
     Using the continuity of $e$ we have
     \begin{equation*}
        e(\vec{p}, b_j(p)) = \mathrm{sup}\{e(\vec{p}, x_1) \mid x_1 \in \{d_1(p)\}\cup  (d_1(\vec{p}),b_j(\vec{p}))\} \leq u
     \end{equation*}
     This is indeed a good approximation for the fixed point, as the old $d_1(\vec{p}) < e(\vec{p}, b_j(\vec{p}))$ and $E(\vec{p}, e(\vec{p}, b_j(\vec{p})))$ does not hold. The first holds by~\eqref{eq:fixpoint-sandwitch} for $p_{k+1} = d_1(\vec{p})$ and by the fact that $e$ is monotone in $x_1$ and $d_1(\vec{p}) \leq b_j(\vec{p})$. The second is because if $E(\vec{p}, e(\vec{p}, b_j(\vec{p})))$ then $e(\vec{p}, b_j(\vec{p})) \leq b_j(\vec{p})$, but we know $e(\vec{p}, b_j(\vec{p})) \geq b_j(\vec{p})$ (if we substitute $b_j(\vec{p})$ in~\eqref{eq:fixpoint-sandwitch} using the definition of $e(\vec{p}, b_j(\vec{p})$ via supremum), so we get
     $e(\vec{p}, b_j(\vec{p})) = u$ (because $u$ is a fixed point), which contradicts not $E(\vec{p},u)$.

     For $i = 2, \ldots, n$ we want to show
     \begin{equation*}
         d_i(\vec{p}) = d^+_i(\vec{p}, d_1(\vec{p})) \leq \proj{i}{\fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}}
     \end{equation*}

     Since $E(\vec{p}, d_1(\vec{p}))$ holds, also $D^+(\vec{p}, d_1(\vec{p}))$ holds and by~\eqref{eq:prev-d-eq-fix} we have that
     \begin{equation*}
        d^+_i(\vec{p}, d_1(\vec{p}))
        =
        \proj{i}{\fixpoint{\xvec{2}{n}}{{\slicevec{t}{2}{n}(\vec{p}, d_1(\vec{p}),\xvec{2}{n})}}}
        \leq
        \proj{i}{\fixpoint{\xvec{2}{n}}{{\slicevec{t}{2}{n}(\vec{p}, u,\xvec{2}{n})}}} = \proj{i}{\fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}}
     \end{equation*}
     which is true by monotonicity of $t_i$ and by $\invariant{2}$ for $i = 1$.
\end{proof}


\begin{lemma}
    Let  $\vec{r} \in [0,1]^k$ be a vector of rational numbers and  $\vec{t} \colon [0,1]^k \times [0,1]^n \to [0,1]^n$ a piecewise linear function with components $t_1(\vec{y}, \vec{x})$, $t_2(\vec{y}, \vec{x})$, \ldots ,  $t_n(\vec{y}, \vec{x})$, that is monotone in variables $x_1, \ldots, x_n$ that are given by local algorithms.

    Then the zero algorithm terminates and returns an $n$-dimensional vector $d_{1}'(\vec{y})$, $d_{2}'(\vec{y})$, \ldots, $d_n'(\vec{y})$ and a set of constraints $D'(\vec{y})$, such that
    \begin{enumerate}[(P1)]
        \item $D'(\vec{r})$ holds,
        \item for all $\vec{p} \in \RR^{k}$, if $D'(\vec{p})$ holds then
        \begin{equation*}
            \vec{d'}(\vec{p})= \fixpoint{\vec{x}}{\vec{t}(\vec{p},\vec{x})}
        \end{equation*}
    \end{enumerate}
    Moreover the set of all possible resulting vectors of conditioned linear expressions
    \begin{equation*}
        \{(D'(\vec{y}) \vdash d'_1(\vec{y}), \ldots, D'(\vec{y}) \vdash d'_n(\vec{y}))_{\vec{r}} \mid \vec{r} \in [0,1]^k\}
    \end{equation*}
    is finite.
\end{lemma}

\begin{proof}
    By induction on $n$. If $n = 0$ trivial.
    Induction hypothesis: termination and finiteness for $n-1$ variables.
    \todo{Idea: every time we enter the loop, we look at a different piece of the monotone piecewise linear function
    \begin{equation}
        \label{eq:one-dim-lin-fun}
        \vec{y}, x_1 \mapsto t_1(\vec{y}, x_1, \fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{y},\vec{x})})
    \end{equation}
    along the line $\vec{y} = \vec{r}$.
    }
    These piecewise can be described as
    \begin{align*}
        \mathcal{P} = \{
            C'(\vec{y}, x_1, \slicevec{d^+}{2}{n}(\vec{y}, x_1))
            \cup
            D^+(\vec{y}, x_1)
            \vdash e'(\vec{y}, x_1, \slicevec{d^+}{2}{n}(\vec{y}, x_1))
            \mid \\
            D^+(\vec{y}, x_1) \vdash \slicevec{d^+}{2}{n}(\vec{y}, x_1) \text{ piece of $\fixpoint{\xvec{2}{n}}{\slicevec{t}{2}{n}(\vec{y}, \vec{x})}$ at $\vec{r},p_1$}\\
            C(\vec{y}, \vec{x}) \vdash e'(\vec{y}, \vec{x}) \text{ piece of $t_1(\vec{y}, \vec{x})$ at $\vec{r},p_1, \slicevec{d^+}{2}{n}(\vec{r}, p_1) $}
        \}_{p_1 \in [0,1]}
    \end{align*}
    The iterations of the loop of the zero-algorithm generate a sequence of approximating terms $d_1(\vec{y})$, $ d'_1(\vec{y})$, \ldots,  $d^{(m)}_1(\vec{y})$, \ldots
    On each iteration the constrained expression $E \vdash e$ defined in~\eqref{eq:def_E} and~\eqref{eq:def_e} manifestly belongs to the set~$\mathcal{P}$. On the $m$-th iteration we have that $d^{(m)}_1(\vec{r}) < d^{(m+1)}_1(\vec{r})$ and $\neg E(\vec{r}, d^{(m+1)}_1(\vec{r}))$ as established in the proof of~\cref{thm:correctness-recursive}. By the convexity of the constraint sets, this means that each $d_1(\vec{y})$, $ d'_1(\vec{y})$, \ldots,  $d^{(m)}_1(\vec{y})$, \ldots is in a different constraint set from $\mathcal{P}$. The loop can therefore have only finitely many iterations and the algorithm terminates.

    For each input vector $\vec{r}$ to the algorithm, the execution of the algorithm proceeds, with the control flow depending on various choices, some dependent on $\vec{r}$ and some not.
    Because each choice point in the algorithm has only finitely many possibilities, the  possible execution sequences of the algorithm form a finitely branching tree in which, by termination, every branch is finite. Since the result returned by the algorithm depends solely on the execution branch followed, the algorithm only has finitely many possible results.
\end{proof}

\begin{lemma}
    Let  $\vec{r} \in [0,1]^k$ be a vector of rational numbers and  $\vec{t} \colon [0,1]^k \times [0,1]^n \to [0,1]^n$ a piecewise linear function with components $t_1(\vec{y}, \vec{x})$, $t_2(\vec{y}, \vec{x})$, \ldots ,  $t_n(\vec{y}, \vec{x})$, that is monotone in variables $x_1, \ldots, x_n$ that are given by local algorithms. Suppose \todo{both} algorithms compute for at least $\ell$ iterations of the main loop with approximating expressions $d_1^{0,1}(\vec{y}), \ldots, d_1^{0,\ell}(\vec{y})$ and $d_1^{1,1}(\vec{y}), \ldots, d_1^{1,\ell}(\vec{y})$. Then for $i = 1, \ldots, \ell$ it holds that
    \begin{equation*}
        d_1^{0,i}(\vec{r}) \leq d_1^{1,i}(\vec{r}).
    \end{equation*}
\end{lemma}

\begin{lemma}
    Let  $\vec{r} \in [0,1]^k$ be a vector of rational numbers and  $\vec{t} \colon [0,1]^k \times [0,1]^n \to [0,1]^n$ a piecewise linear function with components $t_1(\vec{y}, \vec{x})$, $t_2(\vec{y}, \vec{x})$, \ldots ,  $t_n(\vec{y}, \vec{x})$, that is monotone in variables $x_1, \ldots, x_n$ that are given by local algorithms.

    If $t_1$, \ldots, $t_n$ are given disjoint local algorithms, then the local algorithm given by the fixed point is also disjoint.
\end{lemma}
