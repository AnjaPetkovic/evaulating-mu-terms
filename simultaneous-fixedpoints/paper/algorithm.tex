\section{Generalized algorithm for $n$-dimensional fixed point}

\begin{algorithmic}
\State \textbf{Input:} A vector $\vec{r} \in [0,1]^k$ of rational numbers  and a piecewise linear function $\vec{t} \colon [0,1]^k \times [0,1]^n \to [0,1]^n$ with components $t_1(\vec{y}, \vec{x})$, $t_2(\vec{y}, \vec{x})$, \ldots,  $t_n(\vec{y}, \vec{x})$, that is monotone in variables $x_1, \ldots, x_n$ and is given by a local algorithm.

\State \textbf{Output:} Conditioned linear expressions $C_1(\vec{y}) \vdash e_1(\vec{y})$, $C_2(\vec{y}) \vdash e_2(\vec{y})$, \ldots, $C_n(\vec{y}) \vdash e_n(\vec{y})$, that represent the fixed point
\begin{equation*}
\fixpoint{\xvec{1}{n}}{\vec{t}(\vec{y}, \xvec{1}{n})},
\end{equation*}
at vector $\vec{r}$ and therefore have the following properties:
\begin{enumerate}[(P1)]
	\item $C_i(\vec{r})$ holds for $i = 1, 2, \ldots, n$,
	\item for all $\vec{s} \in \mathbb{R}^k$, if $C_i(\vec{s})$ holds for every $i = 1, 2, \ldots, n$, then $\vec{s} \in [0,1]^k$ and
	\begin{equation*}
	\slicevec{e}{1}{n}(\vec{s}) = \fixpoint{\xvec{1}{n}}{t(\vec{s}, \xvec{1}{n})}.
	\end{equation*}
\end{enumerate}

The algorithm will manipulate the following data:
\State For $i \in {1,2, \ldots, n}$ let $d_i(\vec{y}, \xvec{1}{i-1})$ be the current approximation for variable $x_i$ and $D_i(\vec{y}, \xvec{1}{i-1})$ current approximation of its corresponding constraint set. We use notation $$\slicevec{\cyrde}{i}{j}(\vec{y}, \xvec{1}{i-1}) = \todo{(d_i(\vec{y}, \xvec{1}{i-1}), d_{i+1}(\vec{y}, \xvec{1}{i-1}, d_{i}(\ldots))\ldots}$$
	\State For $i \in \{1, 2, \ldots, n\}$ initialize values $D_i(\vec{y}, \xvec{1}{i-1}) = \todo{\emptyset'}$ and
	$d_i(\vec{y}, \xvec{1}{i-1}) = 0$.
	\State Let $m$ be the current index of the variable we are considering. Start with $m := n$.
	\State \textbf{Loop:}
	\State The loop invariants that apply are:
	\begin{enumerate}[($\mathcal{I}$1)]
		\item $D_i(\vec{r}, \slicevec{\cyrde}{1}{i-1}(\vec{r}))$ is true for $i = 1, \ldots, n$
		\item for $i = 1, 2, \ldots, m$ if $(\vec{p},\svec{1}{i-1}) \in \RR^{k+i-1}$ and for all $j = 1, 2, \ldots, i$, $D_j(\vec{p},\svec{1}{j-1})$ holds, then $(\vec{p}, \svec{1}{i-1}) \in [0,1]^{k+i-1}$ and
		\begin{align*}
		d_i(\vec{p}, \svec{1}{i-1}) \leq \proj{i}{\fixpoint{\xvec{i}{n}}{\slicevec{t}{i}{n}(\vec{p}, \svec{1}{i-1},\xvec{i}{n})}}
		\end{align*}
	\item for $i = m+1, m+2, \ldots, n$ if $(\vec{p},\svec{1}{i-1}) \in \RR^{k+i-1}$ and for all $j = 1, 2, \ldots, i$, $D_j(\vec{p},\svec{1}{j-1})$ holds, then $(\vec{p}, \svec{1}{i-1}) \in [0,1]^{k+i-1}$ and
	\begin{align*}
		d_i(\vec{p}, \svec{1}{i-1}) =  \proj{i}{\fixpoint{\xvec{i}{n}}{\slicevec{t}{i}{n}(\vec{p}, \svec{1}{i-1},\xvec{i}{n})}}
	\end{align*}
	% \item For $i < n$ if $D_i(\vec{p},\svec{1}{i-1})$ holds, then $D_{i+1}(\vec{p},\svec{1}{i-1},d_i(\vec{p},\svec{1}{i-1}))$ holds.
	\end{enumerate}
	\State By using the local algorithm, calculate $t_m(\vec{y}, \vec{x})$
	at $(\vec{r}, \slicevec{\cyrde}{1}{n}(\vec{r}))$ as
	$C'(\vec{y}, \xvec{1}{n}) \vdash e'(\vec{y}, \xvec{1}{n})$.
	\State Let $e(\vec{y}, \xvec{1}{m}) := e'(\vec{y},\xvec{1}{m}, \slicevec{\cyrde}{m+1}{n}(\vec{y}, \xvec{1}{m}))$, where $e(\vec{y}, \xvec{1}{m})$ has the form
	\begin{equation}
		e(\vec{y}, \xvec{1}{m}) = \vec{q_y} \cdot \vec{y} + q_1 x_1 + q_2 x_2 + \ldots + q_m x_m
	\label{def:qs}
	\end{equation}
	and
	$q_1, q_2, \ldots q_m$ are rational numbers and $\vec{q_y}$ is a $k$-dimensional vector of rational numbers.

	\State Let
	\begin{equation}
	E(\vec{y}, \xvec{1}{m}) := C( \vec{y}, \xvec{1}{m}, \slicevec{\cyrde}{m+1}{n}(\vec{y}, \xvec{1}{m}))  \cup \bigcup_{i = m+1}^n D_i(\vec{y}, \xvec{1}{m},\slicevec{\cyrde}{m+1}{i}(\vec{y}, \xvec{1}{m}))
	\label{eq:def_E}
	\end{equation}
	\State Arrange the inequalities in $E(\vec{y}, \xvec{1}{m})$  to have the following structure:
	\begin{align}
	\begin{split}
	E'(\vec{y}, \xvec{1}{m-1}) \cup
	 \{x_m > a_i(\vec{y},\xvec{1}{m-1})\}_{1 \leq i \leq u'}\, \cup \\
	\{x_m \geq a_i(\vec{y},\xvec{1}{m-1})\}_{u' \leq i \leq u} \cup
	 \{x_m < b_i(\vec{y},\xvec{1}{m-1})\}_{1 \leq i \leq v'}\, \cup \\
	 \{x_m \leq b_i(\vec{y},\xvec{1}{m-1})\}_{v' \leq i \leq v}
	 \end{split}
	 \label{eq:rearranged}
	\end{align}
	\State We now make a case distinction based on the value of $q_m$ from equation \ref{def:qs}.
	\If{$q_m \neq 1$ }
		\State Define the linear expression
		$$ f(\vec{y}, \xvec{1}{m-1}) := \frac{1}{1-q_m}(\vec{q_y} \cdot \vec{y} + q_1 x_1 + \ldots + q_{m-1} x_{m-1} )$$ \todo{Rename $f$ to $d_m'$? Have $D_m'$ and rename $D_m$ to $D_m'$?}
		\If{$E(\vec{r},\slicevec{\cyrde}{1}{m-1}(\vec{r}), f(\vec{r}, \slicevec{\cyrde}{1}{m-1}(\vec{r})))$ holds
		}
		\State \begin{align}
		\begin{split}
		D_m(\vec{y},\xvec{1}{m-1}) :=  D_m(\vec{y},\xvec{1}{m-1}) \, \cup
		E'(\vec{y},\xvec{1}{m-1}) \, \cup \\
		\{d_m(\vec{y},\xvec{1}{m-1}) \leq f(\vec{y},\xvec{1}{m-1}) \} \, \cup \\
		\{d_m(\vec{y},\xvec{1}{m-1}) > a_i(\vec{y},\xvec{1}{m-1})\}_{1 \leq i \leq u'} \, \cup \\
		\{d_m(\vec{y},\xvec{1}{m-1}) \geq a_i(\vec{y},\xvec{1}{m-1})\}_{u' \leq i \leq u}  \, \cup \\
		\{f(\vec{y},\xvec{1}{m-1}) < b_i(\vec{y},\xvec{1}{m-1})\}_{1 \leq i \leq v'}  \, \cup \\
		\{f(\vec{y},\xvec{1}{m-1}) \leq b_i(\vec{y},\xvec{1}{m-1})\}_{v' \leq i \leq v}
		\label{eq:update_qm_neq_1}
		\end{split}
		\end{align}
		\State $$d_m(\vec{y},\xvec{1}{m-1}) :=  f(\vec{y},\xvec{1}{m-1})$$
		\State $m := m-1$
			\If{$m = 0$}
				exit the loop \textbf{else}  go back to the loop
			\EndIf

		\Else
		\State Define $$ N(\vec{y}, \xvec{1}{m-1}) $$ to be the negation of the inequality
		$$ e_1(\vec{y}, \xvec{1}{m-1}, f(\vec{y}, \xvec{1}{m-1})) \triangleleft e_2(\vec{y}, \xvec{1}{m-1}, f(\vec{y},\xvec{1}{m-1})),$$
		where $\triangleleft$ is used for either $<$ or $\leq$ and $e_1(\vec{y}, \xvec{1}{m}) \triangleleft e_2(\vec{y}, \xvec{1}{m})$ is the chosen inequality in $E(\vec{y}, \xvec{1}{m})$, that does not hold at $$(\vec{r}, \slicevec{\cyrde}{1}{m-1}(\vec{r}), f(\vec{r}, \slicevec{\cyrde}{1}{m-1}(\vec{r})))$$
		and go to \textbf{find next approximation} below.
		\EndIf
	\Else (case, that $q_m = 1$)
		\If{\todo{$e(\vec{r}, \slicevec{\cyrde}{1}{m-1}(\vec{r}))  = 0$ is that even right? Should it be $e(\vec{r}, \slicevec{\cyrde}{1}{m-1}(\vec{r}),0)$}}
			\State \begin{align}
				\begin{split}
				D_m(\vec{y},\xvec{1}{m-1}) : = D_m(\vec{y},\xvec{1}{m-1})  \, \cup \\
				E(\vec{y},\xvec{1}{m-1},d_m(\vec{y},\xvec{1}{m-1})) \, \cup \\
				\{ \todo{e(\vec{y},\xvec{1}{m-1},0)  = 0}\}
				\label{eq:update_qm_eq_1}
				\end{split}
			\end{align}

			\State leave $d_m(\vec{y},\xvec{1}{m-1})$ unchanged
			\State $m := m-1$
				\If{$m = 0$}
			exit the loop \textbf{else}  go back to the loop
			\EndIf

		\Else
			\State Define $N(\vec{y},\xvec{1}{m-1})$ to be the one of the inequalities
			\begin{align*}
				\vec{q_y} \cdot \vec{y} + q_1 x_1 + \ldots q_{m-1} x_{m-1} < 0, \\ \vec{q_y} \cdot \vec{y} + q_1 x_1 + \ldots q_{m-1} x_{m-1} > 0
			\end{align*}
			that holds at $(\vec{r}, \slicevec{\cyrde}{1}{m-1}(\vec{r}))$ and proceed to \textbf{find next approximation} below.
		\EndIf
	\EndIf

	\State \textbf{Find next approximation:}
	\State Let $\slicevec{\rebase{d}}{m}{n}(\vec{y}, \xvec{1}{m-1}) := \slicevec{\cyrde}{m}{n}(\vec{y}, \xvec{1}{m-1})$.
	\State \todo{Explain, that $\rebase{d}$ is for renaming for new iteration}
	\State Consider the inequalities in $E(\vec{y}, \xvec{1}{m-1})$ arranged as in (\ref{eq:rearranged}) and choose a $j$ such that $b_j(\vec{r},\slicevec{\cyrde}{1}{m-1}(\vec{r})) \leq b_i(\vec{r},\slicevec{\cyrde}{1}{m-1}(\vec{r}))$ for all $i$. We shall refer to $b_j(\vec{y}, \xvec{1}{m-1})$ as a \emph{supremum term}. If there is a candidate for a supremum term, which arises from strict inequality, choose $b_j(\vec{y}, \xvec{1}{m-1})$ to be one such.
	\State Let $\rebase{D}_m(\vec{y}, \xvec{1}{m-1})$ and $\rebase{d}_m(\vec{y}, \xvec{1}{m-1})$ be the current values of $D_m(\vec{y}, \xvec{1}{m-1})$ and $d_m(\vec{y}, \xvec{1}{m-1})$ (we will now update them).
	\If {the supremum term $b_j(\vec{y}, \xvec{1}{m-1})$ arises from a strict inequality of the form $x_m < b_j(\vec{y}, \xvec{1}{m-1})$ }
		\State Update the current values to be
		\begin{equation}
			\begin{split}
			D_m(\vec{y}, \xvec{1}{m-1}) :=D_m(\vec{y}, \xvec{1}{m-1}) \cup
			E'(\vec{y}, \xvec{1}{m-1})\cup
			\\ \{N(\vec{y}, \xvec{1}{m-1})\}\cup
			\{d_m(\vec{y}, \xvec{1}{m-1})<b_j(\vec{y}, \xvec{1}{m-1})\}\cup\\
			\{b_j(\vec{y}, \xvec{1}{m-1})\leq b_i(\vec{y}, \xvec{1}{m-1})\}_{1 \leq i \leq v} \cup\\
			\{d_m(\vec{y}, \xvec{1}{m-1})> a_i(\vec{y}, \xvec{1}{m-1})\}_{1 \leq i \leq u'}  \cup\\
			 \{d_m(\vec{y}, \xvec{1}{m-1}) \geq a_i(\vec{y}, \xvec{1}{m-1})\}_{u' \leq i \leq u}
			\end{split}
			\label{eq:fnaDm1}
		\end{equation}
		\State \begin{equation}
			d_m(\vec{y}, \xvec{1}{m-1}) := e(\vec{y}, \xvec{1}{m-1},b_j(\vec{y}, \xvec{1}{m-1}))
			\label{eq:fna-dm}
		\end{equation}
	\Else
		\State Update the current values to be
		\begin{equation}
		\begin{split}
		D_m(\vec{y}, \xvec{1}{m-1}) :=D_m(\vec{y}, \xvec{1}{m-1}) \cup
		E'(\vec{y}, \xvec{1}{m-1})\cup\\
		\{N(\vec{y}, \xvec{1}{m-1})\}\cup
		\{d_m(\vec{y}, \xvec{1}{m-1})\leq b_j(\vec{y}, \xvec{1}{m-1})\}\cup\\
		\{b_j(\vec{y}, \xvec{1}{m-1})\leq b_i(\vec{y}, \xvec{1}{m-1})\}_{v' \leq i \leq v} \cup\\
		\{b_j(\vec{y}, \xvec{1}{m-1})< b_i(\vec{y}, \xvec{1}{m-1})\}_{1 \leq i \leq v'} \cup\\
		 \{d_m(\vec{y}, \xvec{1}{m-1})> a_i(\vec{y}, \xvec{1}{m-1})\}_{1 \leq i \leq u'}  \cup\\
		\{d_m(\vec{y}, \xvec{1}{m-1}) \geq a_i(\vec{y}, \xvec{1}{m-1})\}_{u' \leq i \leq u}
		\end{split}
		\label{eq:fnaDm2}
		\end{equation}
		\State $$ d_m(\vec{y}, \xvec{1}{m-1}) := e(\vec{y}, \xvec{1}{m-1},b_j(\vec{y}, \xvec{1}{m-1}))$$
	\EndIf
	\State For $i = m+1, m+2, \ldots, n$ let
	\todo{
		\begin{equation}
		D_i(\vec{y}, \xvec{1}{i-1}) := \{ \rebase{d}_{i-1}(\vec{y},\xvec{1}{m-1}) \leq x_{i-1}, x_{i-1} \leq 1\}
		\label{eq:fnaDm-n}
		\end{equation}} and $$d_i(\vec{y}, \xvec{1}{i-1}) := \rebase{d}_i(\vec{y}, \xvec{1}{m-1}).$$
		% \State Update the initial value of $D_n(\vec{y}, \xvec{1}{n -1})$  to be
		% \begin{equation*}
		% 	D_\n(\vec{y}, \xvec{1}{\n -1}) :=  \{x_i \geq \cyrde_i'(\vec{y})\}_{i = 1,2, \ldots, \ell-1} \cup D_\ell(\vec{y}, \slicevec{\cyrde'}{1}{\ell - 1}(\vec{y}))
		% \end{equation*}
		\State set $m := n$
\State \textbf{end loop}
\State \textbf{return}
\[
\begin{bmatrix}
	D_1(\vec{y}) \\
	D_2(\vec{y}, d_1(\vec{y})) \\
	\vdots \\
	D_i(\vec{y}, \slicevec{\cyrde}{1}{i-1}(\vec{y})) \\
	\vdots \\
	D_n(\vec{y}, \slicevec{\cyrde}{1}{n-1}(\vec{y}))
\end{bmatrix}
\vdash
\slicevec{\cyrde}{1}{n}(\vec{y})
\]


\end{algorithmic}