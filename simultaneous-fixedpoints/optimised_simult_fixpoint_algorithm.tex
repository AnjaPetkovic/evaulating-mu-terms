              
\documentclass[12pt]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{url}
\usepackage[reqno]{amsmath}    % basic ams math environments and symbols
\usepackage{amssymb,amsthm}    % ams symbols and theorems
\usepackage{mathtools}         % extends ams with arrows and stuff
\usepackage{url}               % \url and \href for links
\usepackage{graphicx}          % images
\usepackage{enumerate}

\usepackage{algpseudocode}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\title{Iteration algorithm for calculating simultaneous fixed point}
\author{Anja Petković }
%\date{April 2018}

\begin{document}
	
	\maketitle
	
	Here we give an algorithm for finidng a simulteneous $n$-dimensional fixed point of a monotone piecewise linear function. 
	
	\section{Statement of the problem and prerequisites}
	We are working in the following setting: we are given a function $t \colon [0,1]^n \times [0,1]^k \to [0,1]^n$, which is monotone (as a map between lattices) and piecewise linear. We use notation $(\vec{x}, \vec{y})$ for elements of $[0,1]^n \times [0,1]^k$ and we think of the vector $\vec{y}$ as the vector of parameters. We name the components of $\vec{x}$ as $(x_1, x_2, \ldots, x_n)$. The function is given by a local algorithm, which given an input vector $(\vec{s}, \vec{r}) \in$ yields $n$ conditioned linear expressions 
	\begin{align*}
		C_1(x_1, x_2, \ldots, x_n, \vec{y}) & \vdash  e_1(x_1, x_2, \ldots, x_n, \vec{y}) \\
		C_2(x_1, x_2, \ldots, x_n, \vec{y}) & \vdash  e_2(x_1, x_2, \ldots, x_n, \vec{y}) \\
		\vdots & \\
		C_n(x_1, x_2, \ldots, x_n, \vec{y}) & \vdash  e_n(x_1, x_2, \ldots, x_n, \vec{y}), \\
	\end{align*}
	such that for all $i \in \{1,2,\ldots.n\}$,  $C_i(\vec{s},\vec{r})$ holds and 
	$$t(\vec{s},\vec{r}) = \left ( e_1(\vec{s},\vec{r}), e_2(\vec{s},\vec{r}), \ldots, e_n(\vec{s},\vec{r})\right).$$
	We are calculating the simultaneous least fixed point of the first $n$ components, meaning we are looking for
	\begin{equation}
		\mu 
		\begin{bmatrix}
		x_1 \\
		x_2 \\
		\vdots \\
		x_n
		\end{bmatrix}. t(x_1, x_2, \ldots, x_n, \vec{y}).
	\end{equation}
	
	We use the prodecure of finding a fixed point in one variable for a monotone piecewise linear function $t' \colon [0,1]\times [0,1]^{k} \to [0,1]$, described in the article \cite{Mio-Simpson} and call it \emph{singleFP} (as in single fixed point). The procedure is used as one iteration of the loop, and we get either the fixed point and the conditioned linear expression at the input vector of parameters $\vec{r} \in [0,1]^k$, or the next approximation for it. When we iteratively use this procedure until we reach a final result, we wil use notation \emph{singleFPloop}. For the purposes of this generalisaton we need the following properties of the \emph{singleFP} procedure (that are described and proven in the article):
	\begin{enumerate}[(R1)]
		\item We input a vector $\vec{r} \in [0,1]^k$ (values for parameters), a set of linear inequalities $D_0'(\vec{y})$ and an approximation to the fixed point $d_0'(\vec{y})$ with the following two properties:
		\begin{enumerate}[($\mathcal{I}$1')]
			\item $D_0'(\vec{r})$ is true
			\item for all $\vec{s} \in [0,1]^k$, if $D_0'(\vec{s})$ then $d_0'(\vec{s}) \leq (\mu x. t'(x, \vec{y}))(\vec{s})$.
		\end{enumerate}
		\item The procedure either outputs the least fixed point in the form of a conditioned linear expression $C(\vec{y}) \vdash e( \vec{y})$, meaning 
		\begin{enumerate}[(P1')]
			\item $C(\vec{r})$ is true
			\item for all $\vec{s} \in [0,1]^k$, if $C(\vec{s})$ then $\vec{s} \in [0,1]^k$ and $e(\vec{s}) = (\mu x. t'(x, \vec{y}))(\vec{s})$,
		\end{enumerate}
		or it gives the next approximations $D_1'(\vec{y})$ and $d_1'(\vec{y})$, that satisfy properties $(\mathcal{I}1)$ and $(\mathcal{I}2)$. Moreover $D_0'(\vec{y})$ is a subset of $D_1'(\vec{y})$ and $C(\vec{y})$.
		\item Let $C(x,\vec{y}) \vdash e(x, \vec{y})$ be the conditioned linear expression given by the local algorithm for $t'$ at $(d_0'(\vec{r}), \vec{r})$ (this is the first step of the procedure \emph{singleFP}). If the procedure gives the next approximation, it holds that $d_1'(\vec{s}) > d_0'(\vec{s})$ for $\vec{s}$, that satisfies $D_1'(\vec{s})$ and $C(d_1'(\vec{r}), \vec{r})$ is false. 
	\end{enumerate}

We also need to make use of the Bekić principle (see \cite{rudiments}), stating the following (for the two variables now):

\begin{theorem}[Bekić principle]
	For two dimensions, a simulatneous fixed point can be calculated as:
	$$
	\mu 
	\begin{bmatrix}
	x_1 \\
	x_2
	\end{bmatrix}. 
	\begin{bmatrix}
	f_1(x_1, x_2, \vec{y})\\
	f_2(x_1,x_2,\vec{y})
	\end{bmatrix} = 
	\begin{bmatrix}
	\mu x_1. f_1(x_1, \mu x_2. f_2(x_1, x_2, \vec{y}), \vec{y}) \\
	\mu x_2. f_2(\mu x_1. f_1(x_1, \mu x_2. f_2(x_1, x_2, \vec{y}), \vec{y}),x_2, \vec{y})
	\end{bmatrix}.
	$$
\end{theorem}

This generalises to for $n$ variables using the Gaussian principle (see \cite[pg. 31: Gaussian elimination principle]{rudiments}) in the following way:

\begin{theorem}[Generalised Bekić principle]
	The simultaneous fixed point 
	$$
	\begin{bmatrix}
	a_1(\vec{y}) \\
	a_2(\vec{y}) \\
	\vdots \\
	a_n(\vec{y})
	\end{bmatrix} = 
	\mu 
	\begin{bmatrix}
	x_1 \\
	x_2 \\
	\vdots \\
	x_n
	\end{bmatrix}. 
	\begin{bmatrix}
	t_1(x_1, x_2, \ldots, x_n, \vec{y}) \\
	t_2(x_1, x_2, \ldots, x_n, \vec{y}) \\
	\vdots \\
	t_n(x_1, x_2, \ldots, x_n, \vec{y})
	\end{bmatrix} 
	$$ can be calculated in the following way:
	\begin{enumerate}
		\item Calculate $g_1(, x_2, \ldots, x_n, \vec{y}) = \mu x_1. t_1(x_1, x_2, \ldots, x_n, \vec{y})$.
		\item Rcursively calculate 
		$$
		\begin{bmatrix}
		a_2(\vec{y}) \\
		\vdots \\
		a_n(\vec{y})
		\end{bmatrix} = 
		\mu 
		\begin{bmatrix}
		x_2 \\
		\vdots \\
		x_n
		\end{bmatrix}. 
		\begin{bmatrix}
		t_2(g_1(x_2, \ldots, x_n, \vec{y}) , x_2, \ldots, x_n, \vec{y}) \\
		t_3(g_1(x_2, \ldots, x_n, \vec{y}) , x_2, \ldots, x_n, \vec{y}) \\
		\vdots \\
		t_n(g_1(x_2, \ldots, x_n, \vec{y}) , x_2, \ldots, x_n, \vec{y})
		\end{bmatrix} 
		$$
		\item $a_1(\vec{y}) = g_1(a_2(\vec{y}), \ldots, a_n(\vec{y}), \vec{y}) $
	\end{enumerate}
\label{thm:generalised_bekic_principle}
\end{theorem}

	\section{Generalised algorithm for the 2-dimensional simultaneous fixed point}

We have an input $\vec{r} \in [0,1]^k$ and the monotone piecewise linear function $t \colon [0,1]^2 \times [0,1]^k \to [0,1]^2$ with components $t_1(\vec{x},\vec{y})$, $t_2(\vec{x},\vec{y})$.
\begin{algorithmic}
	
	\State $D_1^0(\vec{y}) := \emptyset$
	\State $d^0_1(\vec{y}) = 0$ 
	\State Perform \emph{singleFPloop} with 
	\begin{align*}
	\vec{r}' &= d^0_1(\vec{y}), \vec{r} \\
	t'(x, x_1, \vec{y}) &= t_2(x_1, x, \vec{y}),\\
	D_0'(x_1,\vec{y}) &= \emptyset \\
	d_0'(x_1,\vec{y}) &= 0				
	\end{align*}\Comment paramterers are $x_1$ and $\vec{y}$\\
	\State and save the result in $C_2^0(x_1, \vec{y}) \vdash e_2^0(x_1, \vec{y})$
	\While{True}
	\State Perform one iteration of \emph{singleFP} with 
	\begin{align*}
	\vec{r}' &= \vec{r} \\
	t'(x, \vec{y}) &= t_1(x, e_2^0(x,\vec{y}), \vec{y}),\\
	D_0'(\vec{y}) &=  D_1^0(\vec{y}) \\
	d_0'(\vec{y}) &= d^0_1(\vec{y})				
	\end{align*}\Comment paramterers are $\vec{y}$\\
	\State with adding constraints in $C_2^0(x_1, \vec{y})$ to the result of calling the local algorithm, and save the result in $D_1^1(\vec{y}) \vdash d_1^1(\vec{y})$.
	\If{the result is final}
	\State break the while loop
	\EndIf
	\If{$C_2^0(d_1^1(\vec{r}), \vec{r})$ holds} \Comment $\mu x_2. t_2(x_1, x_2, \vec{y})$ has same piece as before
	\State $C_2^1(x_1, \vec{y}) := C_2^0(x_1, \vec{y})$
	\State $ e_2^1(x_1, \vec{y}) := e_2^0(x_1, \vec{y})$
	\Else
	\State Rearrange the inequalities in $C_2^0(x_1,\vec{y})$, so they have the following structure:
	\begin{equation}
		C_2'(\vec{y}) \cup \{x_1 > a_i\}_{1 \leq i \leq l'} \cup \{x_1 \geq a_i\}_{l' \leq i \leq l} \cup \{x_1 < b_i\}_{1 \leq i \leq m'} \cup \{x_1 \leq b_i\}_{m' \leq i \leq m},
	\end{equation}
	such that the only variables in $C_2'$, $a_i$ and $b_i$ are $\vec{y}$. Choose $j$ with $1 \leq j \leq m$ such that $b_j(\vec{r}) \leq b_i(\vec{r})$ for all $1 \leq i\leq m$ ($b_j$ is the supremum term).
	\State Perform \emph{SingleFPloop} with 
	\begin{align*}
	\vec{r}' &= d^1_1(\vec{y}), \vec{r} \\
	t'(x, x_1, \vec{y}) &= t_2(x_1, x, \vec{y}),\\
	D_0'(x_1,\vec{y}) &= \{b_j(\vec{y}) \leq x_1\} \cup \{b_j(\vec{y}) \leq b_i(\vec{y})\mid 1 \leq i \leq m\} \\
	d_0'(x_1,\vec{y}) &= e_2^0(b_j(\vec{y}), \vec{y})		
	\end{align*}\Comment paramterers are $x_1$ and $\vec{y}$\\
	\State and save the result in $C_2^1(x_1, \vec{y}) \vdash e_2^1(x_1, \vec{y})$
	\EndIf
	\State Rewrite
	\begin{align*}
	D_1^0(\vec{y}) &= D_1^1(\vec{y}) \\
	d_1^0(\vec{y}) &= d_1^1(\vec{y})\\
	C_2^0(x_1, \vec{y}) &= C_2^1(x_1, \vec{y}) \\
    e_2^0(x_1, \vec{y})&=  e_2^1(x_1, \vec{y})		
	\end{align*}
	\EndWhile
	\State The first comoponent of the fixed point is  $D_1^1(\vec{y}) \vdash d_1^1(\vec{y})$.
	\State Perform \emph{SingleFPloop} with 
	\begin{align*}
	\vec{r}' &= \vec{r} \\
	t'(x, \vec{y}) &= t_2(d_1^1(\vec{y}), x, \vec{y}),\\
	D_0'(\vec{y}) &= \emptyset \\
	d_0'(\vec{y}) &= 0		
	\end{align*}\Comment paramterers are $\vec{y}$\\
	\State and save the result in $D_2^2(\vec{y}) \vdash d_2^2(\vec{y})$
	\State \Return 
	\begin{equation}
		\begin{bmatrix}
		D_1^1(\vec{y}) \\
		D_2^2(\vec{y})
		\end{bmatrix} \vdash
		\begin{bmatrix}
		d_1^1(\vec{y}) \\
		d_2^2(\vec{y})
		\end{bmatrix}		
	\end{equation}	
\end{algorithmic}

\section{Generalised algorithm for $n$-dimensional fixed point}

We have an input $\vec{r} \in [0,1]^k$ and the monotone piecewise linear function $t \colon [0,1]^n \times [0,1]^k \to [0,1]^n$ with components $t_1(\vec{x},\vec{y})$, $t_2(\vec{x},\vec{y})$, \ldots $t_n(\vec{x},\vec{y})$.
\begin{algorithmic}
	\State $\vec{D}^0(\vec{y}) := 
	\begin{bmatrix}
	\emptyset \\
	\emptyset \\
	\vdots\\
	\emptyset
	\end{bmatrix}$ ($n$-dimensional empty set)  
	\State $\vec{d^0}(\vec{y}) : = 
	\begin{bmatrix}
	0 \\
	0 \\
	\vdots\\
	0
	\end{bmatrix}$ ($n$-dimensional) \Comment notation for components: $d^0_1(\vec{y}), d^0_2(\vec{y}), \ldots$
	\State
	$ \vec{g} :=
	\begin{bmatrix}
	g_1(x_2, x_3, \ldots, x_n, \vec{y}) \\
	g_2(x_3, x_4, \ldots, x_n, \vec{y}) \\
	\vdots\\
	g_n(\vec{y}) 
	\end{bmatrix} := 
	\begin{bmatrix}
	x_1 \\
	x_2 \\
	\vdots\\
	x_n
	\end{bmatrix}
	$
	\State $i := 1$
	\While{$i \neq 0$}
		\State sth
	\EndWhile
	
\end{algorithmic}

\section{Correctness proof}

\section{Termination proof}



\begin{thebibliography}{99}
	\bibitem{Mio-Simpson} Mio, Simpson: \emph{Lukasiewicz $\mu$-calculus}
	\bibitem{rudiments} \emph{Rudiments of $\mu$-calclus}
\end{thebibliography}

\end{document}