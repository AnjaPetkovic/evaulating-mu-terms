from fractions import Fraction

class muTerm:

	possible_operators = ['var','0','1','r','m','M','+','.','mu','nu']

	def __init__(self,operator,subterm1,subterm2 = None, value = None, name = None):
		""" Constructor of a muTerm.

		"""
		if operator not in self.possible_operators:
			raise ValueError("Term operator not valid")
		self.operator = operator
		if operator == 'var':
			if name == None:
				raise ValueError("Variables require names.")
			if type(name) != str and type(name) != int:
				raise ValueError("Variable names are strings or integers.")
			else:
				self.name = name
				self.subterm1 = None
				self.subterm2 = None
				self.value = None
				self.var_set = {name}
				self.var_counter = len(self.var_set)
				self.size = 0
		elif operator == '0':
			self.value = 0
			self.subterm1 = None
			self.subterm2 = None
			self.name = None
			self.var_set = set()
			self.var_counter = len(self.var_set)
			self.size = 0 
		elif operator == '1':
			self.value = 1
			self.subterm1 = None
			self.subterm2 = None
			self.name = None
			self.var_set = set()
			self.var_counter = len(self.var_set)
			self.size = 0
		elif operator == 'r':
			if value == None or type(value) not in [int,float,Fraction]:
				raise ValueError("operator r requires assigned value")
			if subterm1 == None:
				raise ValueError("operator r requires nontrivial subterm")
			else:
				self.value = value
				self.subterm1 = subterm1
				self.subterm2 = None
				self.name = None
				self.var_set = subterm1.var_set
				self.var_counter = len(self.var_set)
				self.size = subterm1.size + 1
		elif operator == 'm':
			if subterm1 == None or subterm2 == None:
				raise ValueError("Operator m requires nontrivial subterms")
			else:
				self.subterm1 = subterm1
				self.subterm2 = subterm2
				self.value = None
				self.name = None
				self.var_set = subterm1.var_set | subterm2.var_set
				self.var_counter = len(self.var_set)
				self.size = subterm1.size + subterm2.size
		elif operator == 'M':
			if subterm1 == None or subterm2 == None:
				raise ValueError("Operator M requires nontrivial subterms")
			else:
				self.subterm1 = subterm1
				self.subterm2 = subterm2
				self.name = None
				self.value = None
				self.var_set = subterm1.var_set | subterm2.var_set
				self.var_counter = len(self.var_set)
				self.size = subterm1.size + subterm2.size
		elif operator == '+':
			if subterm1 == None or subterm2 == None:
				raise ValueError("Operator + requires nontrivial subterms")
			else:
				self.subterm1 = subterm1
				self.subterm2 = subterm2
				self.name = None
				self.value = None
				self.var_set = subterm1.var_set | subterm2.var_set
				self.var_counter = len(self.var_set)
				self.size = subterm1.size + subterm2.size
		elif operator == '.':
			if subterm1 == None or subterm2 == None:
				raise ValueError("Operator . requires nontrivial subterms")
			else:
				self.subterm1 = subterm1
				self.subterm2 = subterm2
				self.name = None
				self.value = None
				self.var_set = subterm1.var_set | subterm2.var_set
				self.var_counter = len(self.var_set)
				self.size = subterm1.size + subterm2.size
		elif operator == 'mu':
			if subterm1 == None:
				raise ValueError("Operator mu requires nontrivial subterm")
			if name == None:
				raise ValueError("Operator mu requires bounding variable name")
			else:
				self.subterm1 = subterm1
				self.subterm2 = None
				self.name = name
				self.value = None
				self.var_set = subterm1.var_set - {name}
				self.var_counter = len(self.var_set)
				self.size = subterm1.size
		elif operator == 'nu':
			if subterm1 == None:
				raise ValueError("Operator nu requires nontrivial subterm")
			if name == None:
				raise ValueError("Operator nu requires bounding variable name")
			else:
				self.subterm1 = subterm1
				self.subterm2 = None
				self.name = name
				self.value = None
				self.var_set = subterm1.var_set - {name}
				self.var_counter = len(self.var_set)
				self.size = subterm1.size


	def __repr__(self):
		return ("muTerm("+ repr(self.operator) + "," + repr(self.subterm1) + "," + repr(self.subterm2) + "," 
			+ repr(self.value) + "," + repr(self.name) + ")")


	def __str__(self):
		""" String representation of terms using unicode signs"""
		if self == None:
			return ''
		s1 = str(self.subterm1)
		s2 = str(self.subterm2)
		if self.operator == '0':
			return '0'
		if self.operator == '1':
			return '1'
		if self.operator == 'r':
			return str(self.value) + "(" + s1 + ")"
		if self.operator == 'var':
			if type(self.name) == int:
				return 'var(' + str(self.name) + ')'
			return self.name 
		if self.operator == 'm':
			return '(' + s1 + ')' + '\u220f' + '(' + s2 + ')'
		if self.operator == 'M':
			return '(' + s1 + ')' + '\u2210' + '(' + s2 + ')'
		if self.operator == '+':
			return '(' + s1 + ')' + '\u2295' + '(' + s2 + ')'
		if self.operator == '.':
			return '(' + s1 + ')' + '\u2299' + '(' + s2 + ')'
		if self.operator == 'mu':
			if type(self.name) == int:
				varName =  'var(' + str(self.name) + ')'
			else:
				varName = self.name
			return '\u03bc ' + varName + '.' + '(' + s1 + ')'
		if self.operator == 'nu':
			if type(self.name) == int:
				varName =  'var(' + str(self.name) + ')'
			else:
				varName = self.name
			return '\u03bd ' + varName + '.' + '(' + s1 + ')'

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.__dict__ == other.__dict__
		else:
			return False


	def __ne__(self, other):
		return not self.__eq__(other)

	def repair_var_set(self,dictionary):
		var_set1 = set()
		for x in self.var_set:
			if x not in dictionary.keys():
				raise ValueError("Renaming variables can only be applied to closed terms")
			var_set1.add(dictionary[x])
		self.var_set = var_set1



	def __rename_variables_recursive(self,counter,dictionary):
		"""Private method for recursively renaming variables"""
		if self.operator == 'var':
			if self.name not in dictionary.keys():
				raise ValueError("Renaming variables can only be applied to closed terms")
			self.name = dictionary[self.name]
			self.repair_var_set(dictionary)
			self.var_counter = counter
		elif self.operator == "mu" or self.operator == "nu":
			counter += 1
			if self.name in dictionary.keys():
				raise ValueError("Variable " + str(self.name) + " is already bounded.")
			dictionary[self.name] = counter
			self.name = counter
			self.repair_var_set(dictionary)
			self.var_counter = counter -1
			self.subterm1.__rename_variables_recursive(counter,dictionary)
		elif self.operator in ['m','M','+','.']:
			self.repair_var_set(dictionary)
			self.var_counter = counter
			self.subterm1.__rename_variables_recursive(counter,dictionary)
			self.subterm2.__rename_variables_recursive(counter,dictionary)
		elif self.operator == "r":
			self.repair_var_set(dictionary)
			self.var_counter = counter
			self.subterm1.__rename_variables_recursive(counter,dictionary)
		else:
			self.repair_var_set(dictionary)
			self.var_counter = counter


	def rename_variables(self):
		"""Method for renaming variables for closed terms, so the names are in cannonical order:
		   the outer variable (bound first form outside) is named var(1), the second is var(2) etc.
		   Each subterm gets var_counter value defining how many free variables may appear so far """
		if self.var_set != set():
			raise ValueError("Renaming variables can only be applied to closed terms")
		self.__rename_variables_recursive(0,{})

	
def eliminate_consequent_mu(term, dictionary,previous = None):
	"""
	Function which eliminates consequent mu's and nu's in the following manner:
	mu x_1. mu x_2. mu x_3. t(x_1, x_2, x_3) = mu x_1. t(x_1, x_1, x_1)
	for proof see Arnold, Niwinski: Rudiments of mu-calculus
	Call method rename_variables afterwords for correct result.
	"""
	if term.operator == 'mu':
		if previous == None or 'nu' in previous:
			previous = ('mu',term.name)
			if term.name in dictionary.keys():
				raise ValueError("Renaming variables can only be applied to closed terms")	
			dictionary[term.name] = term.name
			sub1 = eliminate_consequent_mu(term.subterm1,dictionary,previous)
			return muTerm(term.operator,sub1,None, None, term.name)
		elif 'mu' in previous:
			new_name = previous[1]
			if term.name in dictionary.keys():
				raise ValueError("Renaming variables can only be applied to closed terms")
			dictionary[term.name] = new_name
			return eliminate_consequent_mu(term.subterm1,dictionary,previous)
		else:
			raise ValueError("Term variables are not named properly. Try renaming variables first.")
	if term.operator == 'nu':
		if previous == None or 'mu' in previous:
			previous = ('nu',term.name)
			if term.name in dictionary.keys():
				raise ValueError("Renaming variables can only be applied to closed terms")	
			dictionary[term.name] = term.name
			sub1 = eliminate_consequent_mu(term.subterm1,dictionary,previous)
			return muTerm(term.operator,sub1,None, None, term.name)
		elif 'nu' in previous:
			new_name = previous[1]
			if term.name in dictionary.keys():
				raise ValueError("Renaming variables can only be applied to closed terms")
			dictionary[term.name] = new_name
			return eliminate_consequent_mu(term.subterm1,dictionary,previous)
		else:
			raise ValueError("Term variables are not named properly. Try renaming variables first.")
	elif term.operator in ['m','M','+','.','r']:
		previous = None
		sub1 = eliminate_consequent_mu(term.subterm1,dictionary,previous)
		sub2 = None
		if term.operator != 'r':
			sub2 = eliminate_consequent_mu(term.subterm2,dictionary,previous)
		return muTerm(term.operator,sub1,sub2, term.value, term.name)
	elif term.operator == 'var':
		return muTerm('var',None,None,None,dictionary[term.name])
	elif term.operator in ['0','1']:
		return muTerm(term.operator,None,None,term.value,None)
	








		

testTerm = muTerm('nu', muTerm('.',muTerm('mu',muTerm('+',muTerm('var',None,None,None,'x'),muTerm('1',None)),None,None,'x'),
	muTerm('m',muTerm('r',muTerm('1',None),None,0.5),muTerm('M',muTerm('var',None,None,None,'y'),muTerm('0',None)))),None,None,'y')

t = muTerm('nu',muTerm('nu',muTerm('nu',muTerm('nu',muTerm('+',muTerm('m',muTerm('r',muTerm('m',muTerm('0',None,None,0,None),muTerm('.',muTerm('+',muTerm('0',None,None,0,None),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(2, 3),None),None,None),muTerm('r',muTerm('var',None,None,None,2),None,Fraction(2, 3),None),None,None),None,None),None,Fraction(1, 2),None),muTerm('m',muTerm('m',muTerm('+',muTerm('0',None,None,0,None),muTerm('.',muTerm('r',muTerm('m',muTerm('0',None,None,0,None),muTerm('.',muTerm('+',muTerm('0',None,None,0,None),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(2, 3),None),None,None),muTerm('r',muTerm('var',None,None,None,2),None,Fraction(2, 3),None),None,None),None,None),None,Fraction(1, 2),None),muTerm('r',muTerm('var',None,None,None,4),None,Fraction(1, 2),None),None,None),None,None),muTerm('r',muTerm('m',muTerm('0',None,None,0,None),muTerm('.',muTerm('+',muTerm('0',None,None,0,None),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(2, 3),None),None,None),muTerm('r',muTerm('var',None,None,None,2),None,Fraction(2, 3),None),None,None),None,None),None,Fraction(1, 2),None),None,None),muTerm('.',muTerm('r',muTerm('m',muTerm('0',None,None,0,None),muTerm('.',muTerm('+',muTerm('0',None,None,0,None),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(2, 3),None),None,None),muTerm('r',muTerm('var',None,None,None,2),None,Fraction(2, 3),None),None,None),None,None),None,Fraction(1, 2),None),muTerm('m',muTerm('r',muTerm('var',None,None,None,4),None,Fraction(1, 2),None),muTerm('m',muTerm('+',muTerm('0',None,None,0,None),muTerm('.',muTerm('r',muTerm('m',muTerm('0',None,None,0,None),muTerm('.',muTerm('+',muTerm('0',None,None,0,None),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(2, 3),None),None,None),muTerm('r',muTerm('var',None,None,None,2),None,Fraction(2, 3),None),None,None),None,None),None,Fraction(1, 2),None),muTerm('r',muTerm('var',None,None,None,4),None,Fraction(1, 2),None),None,None),None,None),muTerm('r',muTerm('m',muTerm('0',None,None,0,None),muTerm('.',muTerm('+',muTerm('0',None,None,0,None),muTerm('r',muTerm('1',None,None,1,None),None,Fraction(2, 3),None),None,None),muTerm('r',muTerm('var',None,None,None,2),None,Fraction(2, 3),None),None,None),None,None),None,Fraction(1, 2),None),None,None),None,None),None,None),None,None),None,None),muTerm('r',muTerm('var',None,None,None,2),None,Fraction(1, 2),None),None,None),None,None,4),None,None,3),None,None,2),None,None,1)
t1 = eliminate_consequent_mu(t,{})
t1.rename_variables()
